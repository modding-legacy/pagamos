package com.legacy.pagamos;

import com.legacy.pagamos.client.PagamosClient;
import com.legacy.pagamos.registry.PagamosEntityTypes;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.common.NeoForge;

@Mod(Pagamos.MODID)
public class Pagamos
{
	public static final String NAME = "Pagamos";
	public static final String MODID = "pagamos";

	public Pagamos(IEventBus modBus, ModContainer container)
	{
		IEventBus forgeBus = NeoForge.EVENT_BUS;

		modBus.addListener(Pagamos::commonInit);

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			PagamosClient.init(modBus);
			forgeBus.register(PagamosClient.class);
		}

		modBus.addListener(PagamosEntityTypes::onAttributesRegistered);
		modBus.addListener(EventPriority.LOWEST, PagamosEntityTypes::registerPlacements);
		modBus.addListener(PagamosEntityTypes::registerPlacementOverrides);

		forgeBus.register(PagamosEvents.class);
		modBus.register(PagamosEvents.ModEvents.class);
	}

	private static void commonInit(final FMLCommonSetupEvent event)
	{
	}

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}
}
