package com.legacy.pagamos;

import com.legacy.pagamos.registry.PagamosParticles;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;

public class PagamosEvents
{
	@SubscribeEvent
	public static void onPlayerRightClickBlock(PlayerInteractEvent.RightClickBlock event)
	{
		if (!(event.getEntity() instanceof ServerPlayer) || !(event.getLevel() instanceof ServerLevel))
			return;

		/*// Debug to see if any blocks aren't tagged with what they can be mined with
		List<String> blocksWithoutTool = new ArrayList<>();
		BuiltInRegistries.BLOCK.stream().map(b -> b.builtInRegistryHolder()).filter(h -> h.key().location().getNamespace().equals(Pagamos.MODID)).forEach(h ->
		{
			if (!h.is(BlockTags.MINEABLE_WITH_PICKAXE) && !h.is(BlockTags.MINEABLE_WITH_SHOVEL) && !h.is(BlockTags.MINEABLE_WITH_AXE) && !h.is(BlockTags.MINEABLE_WITH_HOE) && !h.is(BlockTags.SWORD_EFFICIENT))
				blocksWithoutTool.add(h.unwrapKey().map(k -> k.location().toString()).orElse("null"));
		});
		if (!blocksWithoutTool.isEmpty())
			System.out.println("Blocks without a tool: \n" + String.join("\n", blocksWithoutTool));*/

	}

	public static class ModEvents
	{
		@SubscribeEvent
		public static void registerParticleFactories(RegisterParticleProvidersEvent event)
		{
			PagamosParticles.Factory.init(event);
		}
	}
}
