package com.legacy.pagamos.util;

import java.util.HashSet;
import java.util.Set;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Vec3i;
import net.minecraft.util.RandomSource;
import net.minecraft.world.phys.Vec3;

public class MathUtil
{
	public static double calcDistanceDouble(Vec3i firstPos, Vec3i secondPos)
	{
		return calcDistance(Vec3.atCenterOf(firstPos), Vec3.atCenterOf(secondPos));
	}
	
	public static int calcDistance(Vec3i firstPos, Vec3i secondPos)
	{
		return (int) calcDistanceDouble(firstPos, secondPos);
	}

	public static double calcDistance(Vec3 firstPos, Vec3 secondPos)
	{
		double x = firstPos.x() - secondPos.x();
		double y = firstPos.y() - secondPos.y();
		double z = firstPos.z() - secondPos.z();
		double dist = Math.sqrt((x * x) + (y * y) + (z * z));

		return dist;
	}

	public static final float plusOrMinus(RandomSource rand, float base, float diff)
	{
		return base + plusOrMinus(rand, diff);
	}

	public static final float plusOrMinus(RandomSource rand, float diff)
	{
		return ((rand.nextFloat() - rand.nextFloat()) * diff);
	}

	public static int plusOrMinus(RandomSource rand, int original, int diff)
	{
		return rand.nextInt(diff * 2 + 1) - diff + original;
	}

	public static int plusOrMinus(RandomSource rand, int diff)
	{
		return plusOrMinus(rand, 0, diff);
	}
	
	public static Set<BlockPos> getLinePositions(BlockPos cornerA, BlockPos cornerB)
	{
		Set<BlockPos> poses = new HashSet<>();
		double magnitude = Math.sqrt(cornerA.distSqr(cornerB));
		if (magnitude == 0)
		{
			poses.add(cornerA);
			return poses;
		}
		int x = cornerA.getX();
		int y = cornerA.getY();
		int z = cornerA.getZ();
		double dX = (x - cornerB.getX()) / magnitude;
		double dY = (y - cornerB.getY()) / magnitude;
		double dZ = (z - cornerB.getZ()) / magnitude;
		BlockPos pos = cornerA;
		int i = 0;
		while (!pos.equals(cornerB) && i < magnitude + 1)
		{
			pos = cornerA.offset((int) -Math.round(dX * i), (int) -Math.round(dY * i), (int) -Math.round(dZ * i));
			i += 1;
			poses.add(pos);
		}
		return poses;
	}
}
