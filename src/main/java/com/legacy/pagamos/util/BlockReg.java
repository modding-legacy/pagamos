package com.legacy.pagamos.util;

import java.util.function.Supplier;

import com.legacy.pagamos.block.PagamosFlowerPotBlock;
import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.core.Holder;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FenceBlock;
import net.minecraft.world.level.block.FenceGateBlock;
import net.minecraft.world.level.block.FlowerBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.PressurePlateBlock;
import net.minecraft.world.level.block.RotatedPillarBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.TrapDoorBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.block.state.properties.WoodType;

public interface BlockReg
{
	Supplier<BlockBehaviour.Properties> LEAVES = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_LEAVES);
	Supplier<BlockBehaviour.Properties> LOG = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_LOG);
	Supplier<BlockBehaviour.Properties> SAPLING = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_SAPLING);
	Supplier<BlockBehaviour.Properties> PLANKS = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_PLANKS);
	Supplier<BlockBehaviour.Properties> SLAB = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_SLAB);
	Supplier<BlockBehaviour.Properties> STAIRS = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_STAIRS);
	Supplier<BlockBehaviour.Properties> FENCE = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_FENCE);
	Supplier<BlockBehaviour.Properties> GATE = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_FENCE_GATE);
	Supplier<BlockBehaviour.Properties> PRESSURE_PLATE = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_PRESSURE_PLATE);
	Supplier<BlockBehaviour.Properties> BUTTON = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_BUTTON);
	Supplier<BlockBehaviour.Properties> DOOR = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_DOOR);
	Supplier<BlockBehaviour.Properties> TRAPDOOR = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_TRAPDOOR);
	Supplier<BlockBehaviour.Properties> GLASS = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.GLASS);
	Supplier<BlockBehaviour.Properties> SIGN = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_SIGN);
	Supplier<BlockBehaviour.Properties> HANGING_SIGN = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.OAK_HANGING_SIGN);
	Supplier<BlockBehaviour.Properties> FLOWER = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.POPPY);

	Supplier<BlockBehaviour.Properties> STONE_BUTTON = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.STONE_BUTTON);
	Supplier<BlockBehaviour.Properties> STONE_PRESSURE_PLATE = () -> BlockBehaviour.Properties.ofFullCopy(Blocks.STONE_PRESSURE_PLATE);

	public static Block log(String key)
	{
		return PagamosBlocks.register(key, RotatedPillarBlock::new, LOG);
	}

	public static Block planks(String key)
	{
		return PagamosBlocks.register(key, Block::new, PLANKS);
	}

	public static Block leaves(String key)
	{
		return PagamosBlocks.register(key, LeavesBlock::new, LEAVES);
	}

	/*public static Block sign(String key, WoodType type)
	{
		return PagamosBlocks.registerBlock(key, p -> new NCStandingSignBlock(p, type), SIGN);
	}
	
	public static Block wallSign(String key, WoodType type)
	{
		return PagamosBlocks.registerBlock(key, p -> new NCWallSignBlock(p, type), SIGN);
	}
	
	public static Block hangingSign(String key, WoodType type)
	{
		return PagamosBlocks.registerBlock(key, p -> new NCCeilingHangingSignBlock(p, type), SIGN);
	}
	
	public static Block wallHangingSign(String key, WoodType type)
	{
		return PagamosBlocks.registerBlock(key, p -> new NCWallHangingSignBlock(p, type), SIGN);
	}*/

	public static Block woodenButton(String key, BlockSetType type)
	{
		return PagamosBlocks.register(key, p -> new ButtonBlock(type, 30, p), BUTTON);
	}

	public static Block stoneButton(String key, BlockSetType type)
	{
		return PagamosBlocks.register(key, p -> new ButtonBlock(type, 20, p), STONE_BUTTON);
	}

	public static Block woodenPressurePlate(String key, BlockSetType type)
	{
		return PagamosBlocks.register(key, p -> new PressurePlateBlock(type, p), BUTTON);
	}

	public static Block stonePressurePlate(String key, BlockSetType type)
	{
		return PagamosBlocks.register(key, p -> new PressurePlateBlock(type, p), STONE_PRESSURE_PLATE);
	}

	public static Block slab(String key, Block parent)
	{
		return PagamosBlocks.register(key, SlabBlock::new, () -> BlockBehaviour.Properties.ofFullCopy(parent));
	}

	public static Block stairs(String key, Block parent)
	{
		return PagamosBlocks.register(key, p -> new StairBlock(parent.defaultBlockState(), p), () -> BlockBehaviour.Properties.ofFullCopy(parent));
	}

	public static Block fence(String key)
	{
		return PagamosBlocks.register(key, FenceBlock::new, FENCE);
	}

	public static Block gate(String key, WoodType type)
	{
		return PagamosBlocks.register(key, p -> new FenceGateBlock(type, p), GATE);
	}

	public static Block door(String key, BlockSetType type)
	{
		return PagamosBlocks.register(key, p -> new DoorBlock(type, p), DOOR);
	}

	public static Block trapdoor(String key, BlockSetType type)
	{
		return PagamosBlocks.register(key, p -> new TrapDoorBlock(type, p), TRAPDOOR);
	}

	public static Block wall(String key, Block parent)
	{
		return PagamosBlocks.register(key, WallBlock::new, () -> BlockBehaviour.Properties.ofFullCopy(parent));
	}

	public static Block flowerPot(String key, Supplier<Block> flower)
	{
		return PagamosBlocks.registerBlock(key, p -> new PagamosFlowerPotBlock(flower, p), () -> BlockBehaviour.Properties.of().strength(0.0F));
	}

	public static Block flower(String key, Holder<MobEffect> effect, float seconds)
	{
		return PagamosBlocks.register(key, p -> new FlowerBlock(effect, seconds, p), FLOWER);
	}
}
