package com.legacy.pagamos.block_entity;

import com.legacy.pagamos.registry.PagamosItems;

import net.minecraft.core.BlockPos;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.player.Inventory;
import net.minecraft.world.inventory.AbstractContainerMenu;
import net.minecraft.world.inventory.FurnaceMenu;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.RecipeType;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class FreezerBlockEntity extends AbstractFurnaceBlockEntity
{
	public FreezerBlockEntity(BlockPos pos, BlockState state)
	{
		super(PagamosBlockEntityTypes.FREEZER.get(), pos, state, RecipeType.SMELTING);
	}

	@Override
	protected Component getDefaultName()
	{
		return this.getBlockState().getBlock().getName();
	}

	@Override
	protected AbstractContainerMenu createMenu(int id, Inventory player)
	{
		return new FurnaceMenu(id, player, this, this.dataAccess);
	}

	@Override
	public boolean canPlaceItem(int index, ItemStack stack)
	{
		if (index == 2)
		{
			return false;
		}
		else if (index != 1)
		{
			return true;
		}
		else
		{
			ItemStack itemstack = this.items.get(1);
			return isFuel(stack);
		}
	}

	/*@Override
	protected int getBurnDuration(ItemStack stack)
	{
		return getItemChillTime(stack); //200
	}*/

	public static boolean isFuel(ItemStack stack)
	{
		return getItemChillTime(stack) > 0;
	}

	public static int getItemChillTime(ItemStack stack)
	{
		if (stack.isEmpty())
		{
			return 0;
		}

		Item i = stack.getItem();

		/*if (Block.byItem(i).defaultBlockState().getMaterial() == Material.TOP_SNOW)
		{
			return 300;
		}*/

		if (i == Items.SNOWBALL)
		{
			return 100;
		}

		if (i == Blocks.SNOW.asItem())
		{
			return 1600;
		}

		else
		{
			return i == PagamosItems.ice_shard ? 20000 : 0;
		}
	}
}