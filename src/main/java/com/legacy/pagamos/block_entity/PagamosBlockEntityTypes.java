package com.legacy.pagamos.block_entity;

import java.util.Set;
import java.util.function.Supplier;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PagamosBlockEntityTypes
{
	public static final Supplier<BlockEntityType<? extends FreezerBlockEntity>> FREEZER = Lazy.of(() -> create(FreezerBlockEntity::new, PagamosBlocks.freezer));

	public static void init(RegisterEvent event)
	{
		register(event, "freezer", FREEZER);
	}

	@SuppressWarnings("unchecked")
	private static <T extends BlockEntity> void register(RegisterEvent event, String name, Object blockEntity)
	{
		event.register(Registries.BLOCK_ENTITY_TYPE, Pagamos.locate(name), (Supplier<BlockEntityType<?>>) blockEntity);
	}

	private static <T extends BlockEntity> BlockEntityType<T> create(BlockEntityType.BlockEntitySupplier<? extends T> blockEntity, Block... validBlocks)
	{
		return new BlockEntityType<>(blockEntity, Set.of(validBlocks));
	}
}
