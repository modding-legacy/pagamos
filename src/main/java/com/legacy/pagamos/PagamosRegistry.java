package com.legacy.pagamos;

import com.legacy.pagamos.block_entity.PagamosBlockEntityTypes;
import com.legacy.pagamos.registry.PagamosBiomes;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosEntityTypes;
import com.legacy.pagamos.registry.PagamosFeatures;
import com.legacy.pagamos.registry.PagamosItems;
import com.legacy.pagamos.registry.PagamosParticles;
import com.legacy.pagamos.registry.PagamosPoiTypes;
import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.core.registries.Registries;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = Pagamos.MODID, bus = Bus.MOD)
public class PagamosRegistry
{
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
			PagamosEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.ITEM))
			PagamosItems.init(event);
		else if (event.getRegistryKey().equals(Registries.BLOCK))
			PagamosBlocks.init(event);
		else if (event.getRegistryKey().equals(Registries.BLOCK_ENTITY_TYPE))
			PagamosBlockEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.POINT_OF_INTEREST_TYPE))
			PagamosPoiTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			PagamosSounds.init();
		else if (event.getRegistryKey().equals(Registries.FEATURE))
			PagamosFeatures.init(event);
		else if (event.getRegistryKey().equals(Registries.BIOME))
			PagamosBiomes.init(event);
		else if (event.getRegistryKey().equals(Registries.PARTICLE_TYPE))
			PagamosParticles.init(event);
	}
}