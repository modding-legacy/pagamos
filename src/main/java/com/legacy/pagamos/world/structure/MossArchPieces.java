package com.legacy.pagamos.world.structure;

import java.util.ArrayList;
import java.util.List;

import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosStructures;
import com.legacy.pagamos.util.MathUtil;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.ChunkPos;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.structure.BoundingBox;
import net.minecraft.world.level.levelgen.structure.StructurePiece;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePieceSerializationContext;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;
import net.minecraft.world.level.levelgen.structure.templatesystem.StructureTemplateManager;

public class MossArchPieces
{
	public static void assemble(StructureTemplateManager templateManager, BlockPos pos, Rotation rotation, StructurePiecesBuilder builder, RandomSource random, RandomState randomState)
	{
		builder.addPiece(new MossArchPieces.Piece(pos, random.nextFloat() * 360F, 45 + random.nextFloat() * 30F, 30 + random.nextInt(15)));
	}

	public static class Piece extends StructurePiece
	{
		private static final String ROT_ANGLE = "ArchAngle", TILT_ANGLE = "ArchTilt", DISTANCE = "ArchDistance";
		private final float rotAngle, tiltAngle;
		private final int distance;

		protected Piece(BlockPos pos, float rotAngle, float tiltAngle, int distance)
		{
			super(PagamosStructures.MOSS_ARCH.getPieceType("main").get(), 3, BoundingBox.fromCorners(pos.offset(-distance, 0, -distance), pos.offset(distance, 1, distance)));

			this.rotAngle = rotAngle;
			this.tiltAngle = tiltAngle;
			this.distance = distance;
		}

		public Piece(StructurePieceSerializationContext context, CompoundTag nbt)
		{
			super(PagamosStructures.MOSS_ARCH.getPieceType("main").get(), nbt);
			this.rotAngle = nbt.getFloat(ROT_ANGLE);
			this.tiltAngle = nbt.getFloat(TILT_ANGLE);
			this.distance = nbt.getInt(DISTANCE);
		}

		@Override
		public void postProcess(WorldGenLevel level, StructureManager structureManager, ChunkGenerator chunkGen, RandomSource rand, BoundingBox bounds, ChunkPos chunkPos, BlockPos pos)
		{
			BlockPos center = this.boundingBox.getCenter().atY(this.getBoundingBox().maxY());

			float rot = this.rotAngle, endRot = this.rotAngle + this.tiltAngle;
			float xRot = -Mth.sin(rot * Mth.DEG_TO_RAD), zRot = Mth.cos(rot * Mth.DEG_TO_RAD);
			float xRotEnd = -Mth.sin(endRot * Mth.DEG_TO_RAD), zRotEnd = Mth.cos(endRot * Mth.DEG_TO_RAD);

			int distance = this.distance;

			List<BlockPos> positions = new ArrayList<>();

			int centerHeight = 80;

			BlockPos startPos = center.offset(BlockPos.containing((int) (xRot * distance), 0, (int) (zRot * distance))),
					nearShortOffset = center.offset(BlockPos.containing((int) (xRot * (distance / 2)), 0, (int) (zRot * (distance / 2)))),
					farShortOffset = center.offset(BlockPos.containing((int) (xRotEnd * (distance / 3)), 0, (int) (zRotEnd * (distance / 3))).multiply(-1)),
					endPos = center.offset(BlockPos.containing((int) (xRotEnd * (distance / 2)), 0, (int) (zRotEnd * (distance / 2))).multiply(-1));

			for (BlockPos checkPos : List.of(startPos, nearShortOffset, center, farShortOffset, endPos))
			{
				boolean isCenter = checkPos.equals(center), isEnd = checkPos.equals(endPos);
				boolean isShort = checkPos.equals(nearShortOffset), isEndShort = checkPos.equals(farShortOffset);

				int shortDiff = (centerHeight / 6);
				positions.add(checkPos.atY(isCenter ? centerHeight : isShort ? centerHeight - shortDiff : isEndShort ? centerHeight + shortDiff : isEnd ? 115 : 20));
			}

			try
			{
				int size = positions.size();
				if (size >= 5)
				{
					for (int i = 0; i < size; ++i)
					{
						BlockPos thisPos = positions.get(i);
						BlockPos nextPos = (i + 1 < positions.size()) ? positions.get(i + 1) : null;

						if (nextPos != null)
						{
							for (BlockPos linePos : MathUtil.getLinePositions(thisPos, nextPos))
							{
								int w = closeToEdge(linePos, positions) ? 5 : 3;
								for (int x = -w; x <= w; x++)
									for (int z = -w; z <= w; z++)
										for (int y = -w; y <= w; y++)
										{
											BlockPos p = linePos.offset(x, y, z);

											double distanceToStart = MathUtil.calcDistanceDouble(p, positions.getFirst()),
													distanceToEnd = MathUtil.calcDistanceDouble(p, positions.getLast());

											int amount = 0;

											if (distanceToEnd <= 10)
												amount += 10 - Math.abs(-distanceToEnd);
											else if (distanceToStart <= 10)
												amount += 10 - Math.abs(-distanceToStart);

											double wavyInput = 0.5F * MathUtil.calcDistance(p, center);
											if (MathUtil.calcDistance(linePos, p) <= 2 + ((int) (Math.sin(wavyInput) + Math.cos(wavyInput * 2))) + (int) (amount * 0.8F) && bounds.isInside(p) && (level.getBlockState(p).isEmpty() || level.getBlockState(p).is(BlockTags.REPLACEABLE_BY_TREES) || level.getBlockState(p).is(PagamosBlocks.crystal_ice)))
												level.setBlock(p, PagamosBlocks.salmon_moss.defaultBlockState(), 2);
										}

							}
						}
					}

				}

			}
			catch (Throwable t)
			{
				t.printStackTrace();
			}
		}

		private static boolean closeToEdge(BlockPos checkPos, List<BlockPos> positions)
		{
			return MathUtil.calcDistance(checkPos, positions.getFirst()) <= 10 || MathUtil.calcDistance(checkPos, positions.getLast()) <= 10;
		}
		@Override
		protected void addAdditionalSaveData(StructurePieceSerializationContext context, CompoundTag tag)
		{
			tag.putFloat(ROT_ANGLE, this.rotAngle);
			tag.putFloat(TILT_ANGLE, this.tiltAngle);
			tag.putInt(DISTANCE, this.distance);
		}
	}
}
