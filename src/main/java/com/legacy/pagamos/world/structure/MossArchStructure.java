package com.legacy.pagamos.world.structure;

import java.util.Optional;

import com.legacy.pagamos.registry.PagamosStructures;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.levelgen.structure.Structure;
import net.minecraft.world.level.levelgen.structure.StructureType;
import net.minecraft.world.level.levelgen.structure.pieces.StructurePiecesBuilder;

public class MossArchStructure extends Structure
{
	public static final MapCodec<MossArchStructure> CODEC = Structure.simpleCodec(MossArchStructure::new);

	public MossArchStructure(Structure.StructureSettings settings)
	{
		super(settings);
	}

	private void generatePieces(StructurePiecesBuilder builder, BlockPos genPos, GenerationContext context)
	{
		MossArchPieces.assemble(context.structureTemplateManager(), genPos, Rotation.NONE, builder, context.random(), context.randomState());
	}

	@Override
	public Optional<GenerationStub> findGenerationPoint(GenerationContext context)
	{
		BlockPos genPos = context.chunkPos().getMiddleBlockPosition(90);
		return Optional.of(new Structure.GenerationStub(genPos, (builder) -> this.generatePieces(builder, genPos, context)));
	}

	@Override
	public StructureType<?> type()
	{
		return PagamosStructures.MOSS_ARCH.getType();
	}
}