package com.legacy.pagamos.world.features;

import com.legacy.pagamos.data.PagamosTags;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.util.MathUtil;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.PipeBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class HugeMaroonMushroomFeature extends Feature<NoneFeatureConfiguration>
{
	public HugeMaroonMushroomFeature(Codec<NoneFeatureConfiguration> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = level.getRandom();

		BlockPos ground = context.origin().below();

		if (!isValid(level.getBlockState(ground)) || !level.getBlockState(ground).isSolidRender())
			return false;

		int height = 9 + rand.nextInt(4);

		int collisionCheckRange = 5;
		for (int y = 0; y < height + 3; ++y)
		{
			if (!level.getBlockState(pos.offset(0, y, 0)).isAir())
				return false;

			for (int x = -collisionCheckRange; x < collisionCheckRange; ++x)
				for (int z = -collisionCheckRange; z < collisionCheckRange; ++z)
					if (level.getBlockState(pos.offset(x, y, z)).getBlock() instanceof HugeMushroomBlock)
						return false;
		}

		BlockState base = PagamosBlocks.maroon_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false);

		var prop = PipeBlock.PROPERTY_BY_DIRECTION;

		// top unused
		/*for (int x = -one; x <= one; x++)
			for (int z = -one; z <= one; z++)
				this.setBlock(level, pos.offset(x, height + 1, z), base);*/

		// top row
		int w = 2;
		for (int x = -w; x <= w; x++)
			for (int z = -w; z <= w; z++)
				for (int y = 0; y <= 1; y++)
				{
					var p = pos.offset(x, height - y, z);
					if (y == 0 && !(Mth.abs(x) == Mth.abs(z) && Mth.abs(z) == w) || y != 0 && MathUtil.calcDistanceDouble(pos.atY(p.getY()), p) > 2)
						this.setBlock(level, p, base);
				}

		Direction forcedLightDir = Direction.Plane.HORIZONTAL.getRandomDirection(rand);
		for (var dir : Direction.Plane.HORIZONTAL)
		{
			// top row single block edges
			var st = base.setValue(prop.get(dir.getOpposite()), false);

			if (rand.nextFloat() < 0.1F || dir == forcedLightDir)
				this.setBlock(level, pos.relative(dir, 1 + rand.nextInt(2)).above(height - 1), Blocks.SHROOMLIGHT.defaultBlockState());

			this.setBlock(level, pos.relative(dir, 3).above(height - 1), st);

			int h2 = height - 1;
			BlockPos h2Pos = pos.above(h2).relative(dir, 2);
			this.setBlock(level, h2Pos.relative(dir.getClockWise(), 1), st.setValue(prop.get(dir.getCounterClockWise()), false));
			this.setBlock(level, h2Pos.relative(dir.getCounterClockWise(), 1), st.setValue(prop.get(dir.getClockWise()), false));

			// row 3 and 4
			for (int y = 0; y <= 1; y++)
			{
				this.setBlock(level, pos.above(height - (2 + y)).relative(dir, 2).relative(dir.getClockWise(), 2), st.setValue(prop.get(dir.getCounterClockWise()), false));

				w = 1;
				for (int offs = -w; offs <= w; offs++)
					this.setBlock(level, pos.above(height - (2 + y)).relative(dir, 3).relative(dir.getClockWise(), offs), st);
			}

			// bottom row (5)
			this.setBlock(level, pos.above(height - 4).relative(dir, 3).relative(dir.getClockWise(), 2), st.setValue(prop.get(dir.getCounterClockWise()), false));
			this.setBlock(level, pos.above(height - 4).relative(dir, 3).relative(dir.getCounterClockWise(), 2), st.setValue(prop.get(dir.getClockWise()), false));

			for (int offs = -w; offs <= w; offs++)
				this.setBlock(level, pos.above(height - 4).relative(dir, 4).relative(dir.getClockWise(), offs), st);
		}

		for (int y = 0; y < height; ++y)
			this.setBlock(level, pos.offset(0, y, 0), Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.DOWN, false).setValue(HugeMushroomBlock.UP, false));

		return true;
	}

	private static final boolean isValid(BlockState state)
	{
		return state.is(PagamosTags.Blocks.CAN_SUSTAIN_TITIAN_PLANT);
	}
}