package com.legacy.pagamos.world.features;

import java.util.function.Function;

import com.legacy.pagamos.data.PagamosTags;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.mojang.serialization.Codec;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class SmallCrystalClumpFeature extends Feature<NoneFeatureConfiguration>
{
	public SmallCrystalClumpFeature(Codec<NoneFeatureConfiguration> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = level.getRandom();

		return generateCrystals(level, rand, pos, rand.nextBoolean(), 10, 0.1F, state -> state.is(PagamosTags.Blocks.CRYSTAL_CAN_GENERATE_ON));
	}

	public static boolean generateCrystals(WorldGenLevel level, RandomSource rand, BlockPos pos, boolean isPink, int radius, float chance)
	{
		return generateCrystals(level, rand, pos, isPink, radius, chance, (state) -> true);
	}

	public static boolean generateCrystals(WorldGenLevel level, RandomSource rand, BlockPos pos, boolean isPink, int radius, float chance, Function<BlockState, Boolean> placeFunc)
	{
		BlockState state = isPink ? PagamosBlocks.mauve_crystal.defaultBlockState() : PagamosBlocks.azure_crystal.defaultBlockState();

		boolean placedAny = false;
		for (int x = -radius; x <= radius; x++)
		{
			for (int z = -radius; z <= radius; z++)
			{
				for (int y = -radius; y <= radius; y++)
				{
					if (rand.nextFloat() < chance)
					{
						Direction facing = Util.getRandom(Direction.values(), rand);
						BlockState crystal = state.setValue(BlockStateProperties.FACING, facing);
						BlockPos placePos = pos.offset(x, y, z);

						if (level.isEmptyBlock(placePos) && crystal.canSurvive(level, placePos) && placeFunc.apply(level.getBlockState(placePos.relative(facing.getOpposite()))))
						{
							level.setBlock(placePos, crystal, 2);
							placedAny = true;
						}
					}
				}

			}
		}

		return placedAny;
	}
}