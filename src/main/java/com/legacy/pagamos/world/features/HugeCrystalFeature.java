package com.legacy.pagamos.world.features;

import com.legacy.pagamos.data.PagamosTags;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.util.MathUtil;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class HugeCrystalFeature extends Feature<NoneFeatureConfiguration>
{
	public HugeCrystalFeature(Codec<NoneFeatureConfiguration> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = level.getRandom();

		boolean isPink = rand.nextBoolean();
		/*if (!isValid(level.getBlockState(pos)))
			return false;*/

		boolean valid = false;
		for (int i = 0; i < 2; ++i)
		{
			BlockPos newPos = pos.below(i);
			if (isValid(level.getBlockState(newPos)))
			{
				pos = newPos;
				valid = true;
				break;
			}
		}

		if (!valid)
			return false;

		for (Direction dir : Direction.values())
		{
			if (/*dir == Direction.EAST && */level.isEmptyBlock(pos.relative(dir)))
			{
				int yOffset = 12 + rand.nextInt(5);

				int xOffset = 4 + rand.nextInt(3);
				int zOffset = 4 + rand.nextInt(3);

				if (rand.nextBoolean())
					xOffset = -xOffset;

				if (rand.nextBoolean())
					zOffset = -zOffset;

				boolean upDown = dir.getAxis() == Axis.Y;
				BlockPos target = pos.relative(dir, yOffset);

				if (upDown)
					target = target.relative(Direction.EAST, xOffset).relative(Direction.NORTH, zOffset);
				else // ns = z, ew = x
					target = target.relative(Direction.UP, xOffset).relative(dir.getAxis() == Axis.X ? Direction.NORTH : Direction.EAST, zOffset);

				BlockPos targetPos = target;

				boolean hasConflictAtTip = !level.isEmptyBlock(target) && !level.getBlockState(target).is(PagamosTags.Blocks.CRYSTAL_CAN_REPLACE);

				Axis axis = dir.getAxis();

				if (axis != Axis.Y && hasConflictAtTip)
					return false;

				int baseOffset = 2 + rand.nextInt(2);

				for (int x = -baseOffset; x <= baseOffset; x++)
				{
					boolean xof = Mth.abs(x) >= baseOffset;
					for (int z = -baseOffset; z <= baseOffset; z++)
					{
						boolean zof = Mth.abs(z) >= baseOffset;

						BlockPos base = pos.offset(axis != Axis.X ? x : 0, axis != Axis.Y ? (axis != Axis.X ? z : x) : 0, axis != Axis.Z ? z : 0);

						int cornerCutDistance = 10;

						for (BlockPos linePos : MathUtil.getLinePositions(base, targetPos))
						{
							int dist = (int) MathUtil.calcDistance(linePos, targetPos);

							boolean cutCorner = dist >= cornerCutDistance && xof && zof;
							if (!cutCorner)
								this.setBlock(level, linePos, isPink, false);
						}
					}
				}

				SmallCrystalClumpFeature.generateCrystals(level, rand, pos.relative(dir, yOffset / 2), isPink, 7, 0.2F);

				/*int maxOffsets = 40;
				for (int lineOffset = 0; lineOffset < maxOffsets; ++lineOffset)
				{
					boolean big = lineOffset > 10;
				
					for (BlockPos linePos : getLinePositions(basePos, targetPos))
						this.setBlock(level, linePos, false);
				
					int baseOffset = big ? 7 : 3;
					basePos = pos.offset(dir.getAxis() != Axis.X ? rand.nextInt(baseOffset) - (baseOffset / 2) : 0, dir.getAxis() != Axis.Y ? rand.nextInt(baseOffset) - (baseOffset / 2) : 0, dir.getAxis() != Axis.Z ? rand.nextInt(baseOffset) - (baseOffset / 2) : 0);
				
					// targetPos = target.offset(rand.nextInt(2), rand.nextInt(2), rand.nextInt(2));
				}*/

				return true;
			}
		}

		return false;
	}

	private static final boolean isValid(BlockState state)
	{
		return state.is(PagamosTags.Blocks.CRYSTAL_CAN_GENERATE_ON);
	}

	private void setBlock(LevelAccessor level, BlockPos pos, boolean type, boolean debug)
	{
		if (level.isEmptyBlock(pos) || level.getBlockState(pos).is(PagamosTags.Blocks.CRYSTAL_CAN_REPLACE))
			level.setBlock(pos, type ? PagamosBlocks.mauve_crystal_block.defaultBlockState() : PagamosBlocks.azure_crystal_block.defaultBlockState(), 2);
	}
}