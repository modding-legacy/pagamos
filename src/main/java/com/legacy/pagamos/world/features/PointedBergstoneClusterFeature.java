package com.legacy.pagamos.world.features;

import java.util.function.BiFunction;
import java.util.function.Consumer;

import com.legacy.pagamos.block.PointedBergstoneBlock;
import com.legacy.pagamos.data.PagamosTags;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.util.MathUtil;
import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.DripstoneThickness;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;

public class PointedBergstoneClusterFeature extends Feature<NoneFeatureConfiguration>
{
	public PointedBergstoneClusterFeature(Codec<NoneFeatureConfiguration> codec)
	{
		super(codec);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos pos = context.origin();
		RandomSource rand = level.getRandom();

		int horizRadius = 6, vertRadius = 3;
		for (int x = -horizRadius; x <= horizRadius; x++)
		{
			for (int z = -horizRadius; z <= horizRadius; z++)
			{
				// Chance to even try in this column
				if (rand.nextFloat() < 0.05F)
				{
					yCheck: for (int y = -vertRadius; y <= vertRadius; y++)
					{
						BlockPos placePos = pos.offset(x, y, z);

						// Creates round patches
						if (MathUtil.calcDistance(pos, placePos) <= horizRadius)
						{
							for (Direction direction : Direction.Plane.VERTICAL)
							{
								// If we succeed, then break out of this column
								if (this.createDripstone(level, rand, placePos, direction))
									break yCheck;
							}
						}
					}
				}
			}
		}

		/*int range = 10;
		for (int i = 0; i < 10; i++)
			for (Direction direction : Direction.Plane.VERTICAL)
				this.createDripstone(level, rand, pos.offset(MathUtil.plusOrMinus(rand, range), MathUtil.plusOrMinus(rand, range), MathUtil.plusOrMinus(rand, range)), direction);*/

		return true;
	}

	private boolean createDripstone(WorldGenLevel level, RandomSource rand, BlockPos pos, Direction growthDir)
	{
		BlockState placeOnto = level.getBlockState(pos.relative(growthDir.getOpposite()));
		if (this.canReplace(level.getBlockState(pos)) && placeOnto.is(PagamosTags.Blocks.DRIPSTONES_CAN_GENERATE_ON))
		{
			int height = rand.nextInt(10);

			for (int y = 0; y <= height; y++)
			{
				if (!this.canReplace(level.getBlockState(pos.relative(growthDir, y))))
				{
					height = y - 1;
					break;
				}
			}

			if (height < 0)
				return false;

			BlockPos.MutableBlockPos placePos = pos.mutable();
			buildBaseToTipColumn(growthDir, height, false, finalState ->
			{
				if (this.canReplace(level.getBlockState(placePos)))
				{
					level.setBlock(placePos, finalState.setValue(PointedBergstoneBlock.SNOWY, level.getBlockState(placePos.relative(growthDir.getOpposite())).is(BlockTags.SNOW)), 3);
					placePos.move(growthDir);
				}
			});

			return true;
		}

		return false;
	}

	protected static void buildBaseToTipColumn(Direction direction, int height, boolean mergeTip, Consumer<BlockState> blockSetter)
	{
		BiFunction<Direction, DripstoneThickness, BlockState> finalState = (dir, thickness) -> PagamosBlocks.pointed_bergstone.defaultBlockState().setValue(PointedBergstoneBlock.TIP_DIRECTION, dir).setValue(PointedBergstoneBlock.THICKNESS, thickness);

		if (height >= 3)
		{
			blockSetter.accept(finalState.apply(direction, DripstoneThickness.BASE));

			for (int i = 0; i < height - 3; i++)
			{
				blockSetter.accept(finalState.apply(direction, DripstoneThickness.MIDDLE));
			}
		}

		if (height >= 2)
		{
			blockSetter.accept(finalState.apply(direction, DripstoneThickness.FRUSTUM));
		}

		if (height >= 1)
		{
			blockSetter.accept(finalState.apply(direction, mergeTip ? DripstoneThickness.TIP_MERGE : DripstoneThickness.TIP));
		}
	}

	private boolean canReplace(BlockState state)
	{
		return state.isEmpty() || state.is(Blocks.WATER) || state.is(BlockTags.REPLACEABLE);
	}
}