package com.legacy.pagamos.world;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.legacy.pagamos.registry.PagamosBiomes;
import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.Noises;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.SurfaceRules.RuleSource;
import net.minecraft.world.level.levelgen.VerticalAnchor;

public class PagamosSurfaceRuleData
{
	private static final SurfaceRules.RuleSource BEDROCK = stateRule(Blocks.BEDROCK);
	private static final SurfaceRules.RuleSource BERGSTONE = stateRule(PagamosBlocks.bergstone);
	private static final SurfaceRules.RuleSource COBBLED_BERGSTONE = stateRule(PagamosBlocks.cobbled_bergstone);
	private static final SurfaceRules.RuleSource PACKED_SNOW = stateRule(PagamosBlocks.packed_snow);

	private static final SurfaceRules.RuleSource CRYSTAL_ICE = stateRule(PagamosBlocks.crystal_ice);

	private static final SurfaceRules.RuleSource TITIAN_GROWTH = stateRule(PagamosBlocks.titian_growth);
	private static final SurfaceRules.RuleSource MISTSHALE = stateRule(PagamosBlocks.mistshale);

	private static final SurfaceRules.RuleSource GLOWSTONE = stateRule(Blocks.GLOWSTONE);
	private static final SurfaceRules.RuleSource DDD = stateRule(Blocks.SHROOMLIGHT);

	public static SurfaceRules.RuleSource basicData()
	{
		SurfaceRules.ConditionSource surfacerules$conditionsource1 = SurfaceRules.yBlockCheck(VerticalAnchor.absolute(32), 0);
		// SurfaceRules.ConditionSource onSurface =
		// SurfaceRules.yBlockCheck(VerticalAnchor.belowTop(5), 0);
		SurfaceRules.ConditionSource isHole = SurfaceRules.hole();

		SurfaceRules.ConditionSource underWater = SurfaceRules.waterBlockCheck(-1, 0);

		/*RuleSource topCrystalIce = SurfaceRules.ifTrue(SurfaceRules.yBlockCheck(VerticalAnchor.absolute(85), 0), SurfaceRules.ifTrue(SurfaceRules.not(SurfaceRules.yBlockCheck(VerticalAnchor.absolute(110), 0)), CRYSTAL_ICE));*/

		Builder<SurfaceRules.RuleSource> builder = ImmutableList.builder();
		builder.add(SurfaceRules.ifTrue(SurfaceRules.verticalGradient("bedrock_floor", VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(5)), BEDROCK));
		builder.add(SurfaceRules.ifTrue(SurfaceRules.not(SurfaceRules.verticalGradient("bedrock_roof", VerticalAnchor.belowTop(5), VerticalAnchor.top())), BEDROCK));

		// builder.add(SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR,
		// SurfaceRules.sequence(SurfaceRules.ifTrue(underWater, COBBLED_BERGSTONE),
		// BERGSTONE)));

		builder.add(SurfaceRules.ifTrue(SurfaceRules.DEEP_UNDER_FLOOR, SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.isBiome(PagamosBiomes.MISTY_DRIFTS.getKey()), MISTSHALE), SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.ifTrue(SurfaceRules.isBiome(PagamosBiomes.TITANIC_WILDS.getKey()), TITIAN_GROWTH)), BERGSTONE)));

		builder.add(SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, SurfaceRules.sequence(SurfaceRules.ifTrue(SurfaceRules.not(surfacerules$conditionsource1), SurfaceRules.ifTrue(isHole, CRYSTAL_ICE)))));

		return SurfaceRules.sequence(builder.build().toArray((source) -> new SurfaceRules.RuleSource[source]));
	}

	private static RuleSource stateRule(Block block)
	{
		return stateRule(block.defaultBlockState());
	}

	private static RuleSource stateRule(BlockState state)
	{
		return SurfaceRules.state(state);
	}
}
