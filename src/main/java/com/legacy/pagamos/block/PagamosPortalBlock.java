package com.legacy.pagamos.block;

import com.legacy.pagamos.data.PagamosTags;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosDimensions;
import com.legacy.pagamos.registry.PagamosPoiTypes;
import com.legacy.pagamos.registry.PagamosSounds;
import com.legacy.structure_gel.api.block.GelPortalBlock;
import com.legacy.structure_gel.api.dimension.portal.GelPortalForcer.PortalCreator;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class PagamosPortalBlock extends GelPortalBlock
{
	private static final RandomSource SOUND_RAND = RandomSource.create();
	public static final MapCodec<PagamosPortalBlock> CODEC = simpleCodec(PagamosPortalBlock::new);

	@Override
	public MapCodec<? extends GelPortalBlock> codec()
	{
		return CODEC;
	}

	public PagamosPortalBlock(BlockBehaviour.Properties props)
	{
		super(props);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public void animateTick(BlockState stateIn, Level worldIn, BlockPos pos, RandomSource rand)
	{
		if (rand.nextInt(100) == 0)
		{
			worldIn.playLocalSound((double) pos.getX() + 0.5D, (double) pos.getY() + 0.5D, (double) pos.getZ() + 0.5D, PagamosSounds.BLOCK_PORTAL_AMBIENT, SoundSource.BLOCKS, 0.5F, rand.nextFloat() * 0.4F + 0.8F, false);
		}

		for (int i = 0; i < 2; ++i)
		{
			double d0 = (double) ((float) pos.getX() + rand.nextFloat());
			double d1 = (double) ((float) pos.getY() + rand.nextFloat());
			double d2 = (double) ((float) pos.getZ() + rand.nextFloat());
			double d3 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			double d4 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			double d5 = ((double) rand.nextFloat() - 0.5D) * 0.5D;
			int j = rand.nextInt(2) * 2 - 1;

			if (worldIn.getBlockState(pos.west()).getBlock() != this && worldIn.getBlockState(pos.east()).getBlock() != this)
			{
				d0 = (double) pos.getX() + 0.5D + 0.25D * (double) j;
				d3 = (double) (rand.nextFloat() * 2.0F * (float) j);
			}
			else
			{
				d2 = (double) pos.getZ() + 0.5D + 0.25D * (double) j;
				d5 = (double) (rand.nextFloat() * 2.0F * (float) j);
			}

			double div = 8.0D;
			worldIn.addParticle(ParticleTypes.SNOWFLAKE, d0, d1, d2, d3 / div, d4 / div, d5 / div);
		}
	}

	// override to prevent piglins spawning
	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
	}

	@Override
	public ResourceKey<Level> getDestination()
	{
		return PagamosDimensions.pagamosKey();
	}

	@Override
	public ResourceKey<Level> getSource()
	{
		return Level.OVERWORLD;
	}

	@Override
	public PortalCreator getPortalCreator()
	{
		return PortalCreator.SURFACE_BIASED;
	}

	@Override
	public ResourceKey<PoiType> getPortalPoi()
	{
		return PagamosPoiTypes.PAGAMOS_PORTAL.getKey();
	}

	@Override
	public boolean isFrame(BlockState state, BlockGetter level, BlockPos pos)
	{
		return state.is(PagamosTags.Blocks.PAGAMOS_PORTAL_FRAME) || super.isFrame(state, level, pos);
	}

	@Override
	public BlockState getFrameBlock()
	{
		return Blocks.SNOW_BLOCK.defaultBlockState();
	}

	@Override
	public BlockState getIgnitionBlock()
	{
		return PagamosBlocks.blue_fire.defaultBlockState();
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public net.minecraft.client.resources.sounds.SoundInstance getTriggerSound()
	{
		return net.minecraft.client.resources.sounds.SimpleSoundInstance.forLocalAmbience(PagamosSounds.BLOCK_PORTAL_TRIGGER, (SOUND_RAND.nextFloat() * 0.2F) + 0.9F, 0.35F);
	}

	@OnlyIn(Dist.CLIENT)
	@Override
	public net.minecraft.client.resources.sounds.SoundInstance getTravelSound()
	{
		return net.minecraft.client.resources.sounds.SimpleSoundInstance.forLocalAmbience(PagamosSounds.BLOCK_PORTAL_TRAVEL, (SOUND_RAND.nextFloat() * 0.2F) + 0.9F, 1.0F);
	}
}