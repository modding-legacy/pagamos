package com.legacy.pagamos.block;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.HalfTransparentBlock;
import net.minecraft.world.entity.Entity;
import net.minecraft.core.BlockPos;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.level.Level;

public class CreeperIceBlock extends HalfTransparentBlock
{
	public CreeperIceBlock(Block.Properties properties)
	{
		super(properties);
	}

	@Override
	public void entityInside(BlockState state, Level worldIn, BlockPos pos, Entity entityIn)
	{
		entityIn.makeStuckInBlock(state, new Vec3(0.25D, (double) 0.05F, 0.25D));
	}
}