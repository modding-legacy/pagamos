package com.legacy.pagamos.block;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.AmethystClusterBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class CrystalClusterBlock extends AmethystClusterBlock
{
	public CrystalClusterBlock(float height, float aabbOffset, Properties properties)
	{
		super(height, aabbOffset, properties);
	}

	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource random)
	{
	}

	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
	}

	@Override
	protected void onProjectileHit(Level level, BlockState state, BlockHitResult hitResult, Projectile projectile)
	{
	}
}
