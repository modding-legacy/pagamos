package com.legacy.pagamos.block;

import java.util.function.Supplier;

import javax.annotation.Nullable;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class PagamosFlowerPotBlock extends FlowerPotBlock
{
	public PagamosFlowerPotBlock(@Nullable Supplier<FlowerPotBlock> emptyPot, Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		super(emptyPot, flower, properties);
		((FlowerPotBlock) Blocks.FLOWER_POT).addPlant(BuiltInRegistries.BLOCK.getKey(flower.get()), () -> this);
	}

	public PagamosFlowerPotBlock(Supplier<? extends Block> flower, BlockBehaviour.Properties properties)
	{
		this(() -> (FlowerPotBlock) Blocks.FLOWER_POT, flower, properties);
	}

	public PagamosFlowerPotBlock(java.util.function.Supplier<? extends Block> flower)
	{
		this(flower, BlockBehaviour.Properties.of().strength(0.0F));
	}
}
