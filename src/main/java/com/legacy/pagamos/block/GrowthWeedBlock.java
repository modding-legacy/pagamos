package com.legacy.pagamos.block;

import com.legacy.pagamos.data.PagamosTags;

import net.minecraft.core.BlockPos;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.TallGrassBlock;
import net.minecraft.world.level.block.state.BlockState;

public class GrowthWeedBlock extends TallGrassBlock
{
	public GrowthWeedBlock(Properties props)
	{
		super(props);
	}

	@Override
	public boolean isValidBonemealTarget(LevelReader level, BlockPos pos, BlockState state)
	{
		return false;
	}

	@Override
	public boolean isBonemealSuccess(Level level, RandomSource rand, BlockPos pos, BlockState state)
	{
		return false;
	}
	
	@Override
	protected boolean mayPlaceOn(BlockState state, BlockGetter level, BlockPos pos)
	{
		return super.mayPlaceOn(state, level, pos) || state.is(PagamosTags.Blocks.CAN_SUSTAIN_TITIAN_PLANT);
	}
}
