package com.legacy.pagamos.block;

import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosItems;

import net.minecraft.core.BlockPos;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.CaveVinesPlantBlock;
import net.minecraft.world.level.block.GrowingPlantHeadBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;

public class SalmonVinesPlantBlock extends CaveVinesPlantBlock
{
	public SalmonVinesPlantBlock(Properties props)
	{
		super(props);
	}
	
	@Override
	protected InteractionResult useWithoutItem(BlockState state, Level level, BlockPos pos, Player player, BlockHitResult hitResult)
	{
		return SalmonVinesBlock.useSalmonBase(player, state, level, pos);
	}

	@Override
	protected GrowingPlantHeadBlock getHeadBlock()
	{
		return (GrowingPlantHeadBlock) PagamosBlocks.salmon_vines;
	}

	@Override
	protected ItemStack getCloneItemStack(LevelReader level, BlockPos pos, BlockState state, boolean p_386659_)
	{
		return PagamosItems.titian_berry.getDefaultInstance();
	}
}
