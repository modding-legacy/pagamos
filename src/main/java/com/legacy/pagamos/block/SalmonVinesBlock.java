package com.legacy.pagamos.block;

import javax.annotation.Nullable;

import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosItems;

import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.CaveVinesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.BlockHitResult;

public class SalmonVinesBlock extends CaveVinesBlock
{
	public SalmonVinesBlock(Properties props)
	{
		super(props);
	}

	@Override
	protected InteractionResult useWithoutItem(BlockState state, Level level, BlockPos pos, Player player, BlockHitResult hitResult)
	{
		return useSalmonBase(player, state, level, pos);
	}

	@Override
	protected Block getBodyBlock()
	{
		return PagamosBlocks.salmon_vines_plant;
	}

	@Override
	protected ItemStack getCloneItemStack(LevelReader level, BlockPos pos, BlockState state, boolean p_386659_)
	{
		return PagamosItems.titian_berry.getDefaultInstance();
	}

	static InteractionResult useSalmonBase(@Nullable Entity entity, BlockState state, Level level, BlockPos pos)
	{
		if (state.getValue(BERRIES))
		{
			Block.popResource(level, pos, new ItemStack(PagamosItems.titian_berry, 1));
			float f = Mth.randomBetween(level.random, 0.8F, 1.2F);
			level.playSound(null, pos, SoundEvents.CAVE_VINES_PICK_BERRIES, SoundSource.BLOCKS, 1.0F, f);
			BlockState blockstate = state.setValue(BERRIES, Boolean.valueOf(false));
			level.setBlock(pos, blockstate, 2);
			level.gameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Context.of(entity, blockstate));
			return InteractionResult.SUCCESS;
		}
		else
		{
			return InteractionResult.PASS;
		}
	}

}
