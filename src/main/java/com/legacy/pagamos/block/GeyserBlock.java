package com.legacy.pagamos.block;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class GeyserBlock extends Block
{
	private static final VoxelShape BOTTOM = Block.box(1, 0, 1, 15, 8, 15);
	private static final VoxelShape TOP = Block.box(3, 8, 3, 13, 16, 13);
	private static final VoxelShape GEYSER_SHAPE = Shapes.or(TOP, BOTTOM);

	public GeyserBlock(Properties props)
	{
		super(props);
	}

	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource rand)
	{
		level.addParticle(ParticleTypes.SMOKE, pos.getX() + 0.5 + rand.nextDouble() / 4.0 * (rand.nextBoolean() ? 1 : -1), pos.getY() + 1, pos.getZ() + 0.5 + rand.nextDouble() / 4.0 * (rand.nextBoolean() ? 1 : -1), 0.0, 0.005, 0.0);
		level.addAlwaysVisibleParticle(ParticleTypes.CAMPFIRE_COSY_SMOKE, true, pos.getX() + 0.5 + rand.nextDouble() / 3.0 * (rand.nextBoolean() ? 1 : -1), pos.getY() + 1, pos.getZ() + 0.5 + rand.nextDouble() / 3.0 * (rand.nextBoolean() ? 1 : -1), 0.0, 0.07, 0.0);
	}

	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
	}

	@Override
	protected boolean isRandomlyTicking(BlockState state)
	{
		return true;
	}

	@Override
	protected VoxelShape getShape(BlockState state, BlockGetter level, BlockPos pos, CollisionContext context)
	{
		return GEYSER_SHAPE;
	}
}
