package com.legacy.pagamos.block;

import java.util.Optional;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;
import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.item.FallingBlockEntity;
import net.minecraft.world.entity.projectile.Projectile;
import net.minecraft.world.entity.projectile.ThrownTrident;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ScheduledTickAccess;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.PointedDripstoneBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.DripstoneThickness;
import net.minecraft.world.level.material.Fluid;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

/**
 * FIXME: Rewrite most of this PLEASE GOD. AT methods that can be saved, or else
 * just rewrite stuff to keep things simple. We don't need all the liquid stuff
 */
@Deprecated
public class PointedBergstoneBlock extends PointedDripstoneBlock
{
	// FIXME: AT all of these shapes
	private static final VoxelShape TIP_MERGE_SHAPE = Block.box(5.0, 0.0, 5.0, 11.0, 16.0, 11.0);
	private static final VoxelShape TIP_SHAPE_UP = Block.box(5.0, 0.0, 5.0, 11.0, 11.0, 11.0);
	private static final VoxelShape TIP_SHAPE_DOWN = Block.box(5.0, 5.0, 5.0, 11.0, 16.0, 11.0);
	private static final VoxelShape FRUSTUM_SHAPE = Block.box(4.0, 0.0, 4.0, 12.0, 16.0, 12.0);
	private static final VoxelShape MIDDLE_SHAPE = Block.box(3.0, 0.0, 3.0, 13.0, 16.0, 13.0);
	private static final VoxelShape BASE_SHAPE = Block.box(2.0, 0.0, 2.0, 14.0, 16.0, 14.0);

	public static final BooleanProperty SNOWY = BlockStateProperties.SNOWY;

	public PointedBergstoneBlock(Properties properties)
	{
		super(properties);
		this.registerDefaultState(super.defaultBlockState().setValue(SNOWY, false));
	}

	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		super.createBlockStateDefinition(builder);
		
        builder.add(SNOWY);
	}

	@Override
	protected boolean canSurvive(BlockState p_154137_, LevelReader p_154138_, BlockPos p_154139_)
	{
		return isValidPointedBergstonePlacement(p_154138_, p_154139_, p_154137_.getValue(TIP_DIRECTION));
	}

	@Override
	protected BlockState updateShape(BlockState p_154147_, LevelReader p_374104_, ScheduledTickAccess p_374078_, BlockPos p_154151_, Direction p_154148_, BlockPos p_154152_, BlockState p_154149_, RandomSource p_374393_)
	{
		if (p_154147_.getValue(WATERLOGGED))
		{
			p_374078_.scheduleTick(p_154151_, Fluids.WATER, Fluids.WATER.getTickDelay(p_374104_));
		}

		if (p_154148_ != Direction.UP && p_154148_ != Direction.DOWN)
		{
			return p_154147_;
		}
		else
		{
			Direction direction = p_154147_.getValue(TIP_DIRECTION);
			if (direction == Direction.DOWN && p_374078_.getBlockTicks().hasScheduledTick(p_154151_, this))
			{
				return p_154147_;
			}
			else if (p_154148_ == direction.getOpposite() && !this.canSurvive(p_154147_, p_374104_, p_154151_))
			{
				if (direction == Direction.DOWN)
				{
					p_374078_.scheduleTick(p_154151_, this, 2);
				}
				else
				{
					p_374078_.scheduleTick(p_154151_, this, 1);
				}

				return p_154147_;
			}
			else
			{
				boolean flag = p_154147_.getValue(THICKNESS) == DripstoneThickness.TIP_MERGE;
				DripstoneThickness dripstonethickness = calculateDripstoneThickness(p_374104_, p_154151_, direction, flag);
				return p_154147_.setValue(THICKNESS, dripstonethickness);
			}
		}
	}

	@Override
	protected void onProjectileHit(Level p_154042_, BlockState p_154043_, BlockHitResult p_154044_, Projectile p_154045_)
	{
		if (!p_154042_.isClientSide)
		{
			BlockPos blockpos = p_154044_.getBlockPos();
			if (p_154042_ instanceof ServerLevel serverlevel && p_154045_.mayInteract(serverlevel, blockpos) && p_154045_.mayBreak(serverlevel) && p_154045_ instanceof ThrownTrident && p_154045_.getDeltaMovement().length() > 0.6)
			{
				p_154042_.destroyBlock(blockpos, true);
			}
		}
	}

	@Override
	public void fallOn(Level p_154047_, BlockState p_154048_, BlockPos p_154049_, Entity p_154050_, float p_154051_)
	{
		if (p_154048_.getValue(TIP_DIRECTION) == Direction.UP && p_154048_.getValue(THICKNESS) == DripstoneThickness.TIP)
		{
			p_154050_.causeFallDamage(p_154051_ + 2.0F, 2.0F, p_154047_.damageSources().stalagmite());
		}
		else
		{
			super.fallOn(p_154047_, p_154048_, p_154049_, p_154050_, p_154051_);
		}
	}

	@Override
	public void animateTick(BlockState p_221870_, Level p_221871_, BlockPos p_221872_, RandomSource p_221873_)
	{
	}

	@Override
	protected void tick(BlockState p_221865_, ServerLevel p_221866_, BlockPos p_221867_, RandomSource p_221868_)
	{
		if (isStalagmite(p_221865_) && !this.canSurvive(p_221865_, p_221866_, p_221867_))
		{
			p_221866_.destroyBlock(p_221867_, true);
		}
		else
		{
			spawnFallingStalactite(p_221865_, p_221866_, p_221867_);
		}
	}

	@Nullable
	@Override
	public BlockState getStateForPlacement(BlockPlaceContext p_154040_)
	{
		LevelAccessor level = p_154040_.getLevel();
		BlockPos blockpos = p_154040_.getClickedPos();
		Direction direction = p_154040_.getNearestLookingVerticalDirection().getOpposite();
		Direction tipDir = calculateTipDirection(level, blockpos, direction);
		if (tipDir == null)
		{
			return null;
		}
		else
		{
			boolean flag = !p_154040_.isSecondaryUseActive();
			DripstoneThickness dripstonethickness = calculateDripstoneThickness(level, blockpos, tipDir, flag);
			return dripstonethickness == null ? null : this.defaultBlockState().setValue(TIP_DIRECTION, tipDir).setValue(THICKNESS, dripstonethickness).setValue(SNOWY, level.getBlockState(blockpos.relative(tipDir.getOpposite())).is(BlockTags.SNOW)).setValue(WATERLOGGED, Boolean.valueOf(level.getFluidState(blockpos).getType() == Fluids.WATER));
		}
	}

	@Override
	protected FluidState getFluidState(BlockState p_154235_)
	{
		return p_154235_.getValue(WATERLOGGED) ? Fluids.WATER.getSource(false) : super.getFluidState(p_154235_);
	}

	@Override
	protected VoxelShape getOcclusionShape(BlockState p_154170_)
	{
		return Shapes.empty();
	}

	@Override
	protected VoxelShape getShape(BlockState p_154117_, BlockGetter p_154118_, BlockPos p_154119_, CollisionContext p_154120_)
	{
		DripstoneThickness dripstonethickness = p_154117_.getValue(THICKNESS);
		VoxelShape voxelshape;
		if (dripstonethickness == DripstoneThickness.TIP_MERGE)
		{
			voxelshape = TIP_MERGE_SHAPE;
		}
		else if (dripstonethickness == DripstoneThickness.TIP)
		{
			if (p_154117_.getValue(TIP_DIRECTION) == Direction.DOWN)
			{
				voxelshape = TIP_SHAPE_DOWN;
			}
			else
			{
				voxelshape = TIP_SHAPE_UP;
			}
		}
		else if (dripstonethickness == DripstoneThickness.FRUSTUM)
		{
			voxelshape = FRUSTUM_SHAPE;
		}
		else if (dripstonethickness == DripstoneThickness.MIDDLE)
		{
			voxelshape = MIDDLE_SHAPE;
		}
		else
		{
			voxelshape = BASE_SHAPE;
		}

		Vec3 vec3 = p_154117_.getOffset(p_154119_);
		return voxelshape.move(vec3.x, 0.0, vec3.z);
	}

	@Override
	protected boolean isCollisionShapeFullBlock(BlockState p_181235_, BlockGetter p_181236_, BlockPos p_181237_)
	{
		return false;
	}

	@Override
	protected float getMaxHorizontalOffset()
	{
		return 0.125F;
	}

	@Override
	public void onBrokenAfterFall(Level p_154059_, BlockPos p_154060_, FallingBlockEntity p_154061_)
	{
		if (!p_154061_.isSilent())
		{
			p_154059_.levelEvent(1045, p_154060_, 0);
		}
	}

	@Override
	public DamageSource getFallDamageSource(Entity p_254432_)
	{
		return p_254432_.damageSources().fallingStalactite(p_254432_);
	}

	private static void spawnFallingStalactite(BlockState state, ServerLevel level, BlockPos pos)
	{
		BlockPos.MutableBlockPos blockpos$mutableblockpos = pos.mutable();
		BlockState blockstate = state;

		while (isStalactite(blockstate))
		{
			FallingBlockEntity fallingblockentity = FallingBlockEntity.fall(level, blockpos$mutableblockpos, blockstate);
			if (isTip(blockstate, true))
			{
				int i = Math.max(1 + pos.getY() - blockpos$mutableblockpos.getY(), 6);
				float f = 1.0F * (float) i;
				fallingblockentity.setHurtsEntities(f, 40);
				break;
			}

			blockpos$mutableblockpos.move(Direction.DOWN);
			blockstate = level.getBlockState(blockpos$mutableblockpos);
		}
	}

	@VisibleForTesting
	public static void growStalactiteOrStalagmiteIfPossible(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		BlockState blockstate = level.getBlockState(pos.above(1));
		BlockState blockstate1 = level.getBlockState(pos.above(2));
		if (canGrow(blockstate, blockstate1))
		{
			BlockPos blockpos = findTip(state, level, pos, 7, false);
			if (blockpos != null)
			{
				BlockState blockstate2 = level.getBlockState(blockpos);
				if (canDrip(blockstate2) && canTipGrow(blockstate2, level, blockpos))
				{
					if (random.nextBoolean())
					{
						grow(level, blockpos, Direction.DOWN);
					}
					else
					{
						growStalagmiteBelow(level, blockpos);
					}
				}
			}
		}
	}

	private static void growStalagmiteBelow(ServerLevel level, BlockPos pos)
	{
		BlockPos.MutableBlockPos blockpos$mutableblockpos = pos.mutable();

		for (int i = 0; i < 10; i++)
		{
			blockpos$mutableblockpos.move(Direction.DOWN);
			BlockState blockstate = level.getBlockState(blockpos$mutableblockpos);
			if (!blockstate.getFluidState().isEmpty())
			{
				return;
			}

			if (isUnmergedTipWithDirection(blockstate, Direction.UP) && canTipGrow(blockstate, level, blockpos$mutableblockpos))
			{
				grow(level, blockpos$mutableblockpos, Direction.UP);
				return;
			}

			if (isValidPointedBergstonePlacement(level, blockpos$mutableblockpos, Direction.UP) && !level.isWaterAt(blockpos$mutableblockpos.below()))
			{
				grow(level, blockpos$mutableblockpos.below(), Direction.UP);
				return;
			}

			// FIXME
			/*if (!canDripThrough(level, blockpos$mutableblockpos, blockstate))
				return;*/
		}
	}

	private static void grow(ServerLevel server, BlockPos pos, Direction direction)
	{
		BlockPos blockpos = pos.relative(direction);
		BlockState blockstate = server.getBlockState(blockpos);
		if (isUnmergedTipWithDirection(blockstate, direction.getOpposite()))
		{
			createMergedTips(blockstate, server, blockpos);
		}
		else if (blockstate.isAir() || blockstate.is(Blocks.WATER))
		{
			createBergstone(server, blockpos, direction, DripstoneThickness.TIP);
		}
	}

	private static void createBergstone(LevelAccessor level, BlockPos pos, Direction direction, DripstoneThickness thickness)
	{
		BlockState blockstate = PagamosBlocks.pointed_bergstone.defaultBlockState().setValue(TIP_DIRECTION, direction).setValue(THICKNESS, thickness).setValue(WATERLOGGED, Boolean.valueOf(level.getFluidState(pos).getType() == Fluids.WATER)).setValue(SNOWY, level.getBlockState(pos.relative(direction.getOpposite())).is(BlockTags.SNOW));
		level.setBlock(pos, blockstate, 3);
	}

	private static void createMergedTips(BlockState state, LevelAccessor level, BlockPos pos)
	{
		BlockPos blockpos;
		BlockPos blockpos1;
		if (state.getValue(TIP_DIRECTION) == Direction.UP)
		{
			blockpos1 = pos;
			blockpos = pos.above();
		}
		else
		{
			blockpos = pos;
			blockpos1 = pos.below();
		}

		createBergstone(level, blockpos, Direction.DOWN, DripstoneThickness.TIP_MERGE);
		createBergstone(level, blockpos1, Direction.UP, DripstoneThickness.TIP_MERGE);
	}

	private static void spawnDripParticle(Level level, BlockPos pos, BlockState state, Fluid p_fluid)
	{
		Vec3 vec3 = state.getOffset(pos);
		double d0 = 0.0625;
		double d1 = (double) pos.getX() + 0.5 + vec3.x;
		double d2 = (double) ((float) (pos.getY() + 1) - 0.6875F) - 0.0625;
		double d3 = (double) pos.getZ() + 0.5 + vec3.z;
		Fluid fluid = getDripFluid(level, p_fluid);
		ParticleOptions particleoptions = fluid.getFluidType().getDripInfo() != null ? fluid.getFluidType().getDripInfo().dripParticle() : ParticleTypes.DRIPPING_DRIPSTONE_WATER;
		if (particleoptions != null)
			level.addParticle(particleoptions, d1, d2, d3, 0.0, 0.0, 0.0);
	}

	@Nullable
	private static BlockPos findTip(BlockState state, LevelAccessor level, BlockPos pos, int maxIterations, boolean isTipMerge)
	{
		if (isTip(state, isTipMerge))
		{
			return pos;
		}
		else
		{
			Direction direction = state.getValue(TIP_DIRECTION);
			BiPredicate<BlockPos, BlockState> bipredicate = (p_373975_, p_373976_) -> p_373976_.is(PagamosBlocks.pointed_bergstone) && p_373976_.getValue(TIP_DIRECTION) == direction;
			return findBlockVertical(level, pos, direction.getAxisDirection(), bipredicate, p_154168_ -> isTip(p_154168_, isTipMerge), maxIterations).orElse(null);
		}
	}

	@Nullable
	private static Direction calculateTipDirection(LevelReader level, BlockPos pos, Direction dir)
	{
		Direction direction;
		if (isValidPointedBergstonePlacement(level, pos, dir))
		{
			direction = dir;
		}
		else
		{
			if (!isValidPointedBergstonePlacement(level, pos, dir.getOpposite()))
			{
				return null;
			}

			direction = dir.getOpposite();
		}

		return direction;
	}

	private static DripstoneThickness calculateDripstoneThickness(LevelReader level, BlockPos pos, Direction dir, boolean isTipMerge)
	{
		Direction direction = dir.getOpposite();
		BlockState blockstate = level.getBlockState(pos.relative(dir));
		if (isPointedBergstoneWithDirection(blockstate, direction))
		{
			return !isTipMerge && blockstate.getValue(THICKNESS) != DripstoneThickness.TIP_MERGE ? DripstoneThickness.TIP : DripstoneThickness.TIP_MERGE;
		}
		else if (!isPointedBergstoneWithDirection(blockstate, dir))
		{
			return DripstoneThickness.TIP;
		}
		else
		{
			DripstoneThickness dripstonethickness = blockstate.getValue(THICKNESS);
			if (dripstonethickness != DripstoneThickness.TIP && dripstonethickness != DripstoneThickness.TIP_MERGE)
			{
				BlockState blockstate1 = level.getBlockState(pos.relative(direction));
				return !isPointedBergstoneWithDirection(blockstate1, dir) ? DripstoneThickness.BASE : DripstoneThickness.MIDDLE;
			}
			else
			{
				return DripstoneThickness.FRUSTUM;
			}
		}
	}

	public static boolean canDrip(BlockState state)
	{
		return isStalactite(state) && state.getValue(THICKNESS) == DripstoneThickness.TIP && !state.getValue(WATERLOGGED);
	}

	private static boolean canTipGrow(BlockState state, ServerLevel level, BlockPos pos)
	{
		Direction direction = state.getValue(TIP_DIRECTION);
		BlockPos blockpos = pos.relative(direction);
		BlockState blockstate = level.getBlockState(blockpos);
		if (!blockstate.getFluidState().isEmpty())
		{
			return false;
		}
		else
		{
			return blockstate.isAir() ? true : isUnmergedTipWithDirection(blockstate, direction.getOpposite());
		}
	}

	/*private static Optional<BlockPos> findRootBlock(Level level, BlockPos pos, BlockState state, int maxIterations)
	{
		Direction direction = state.getValue(TIP_DIRECTION);
		BiPredicate<BlockPos, BlockState> bipredicate = (p_373972_, p_373973_) -> p_373973_.is(PagamosBlocks.pointed_bergstone) && p_373973_.getValue(TIP_DIRECTION) == direction;
		return findBlockVertical(level, pos, direction.getOpposite().getAxisDirection(), bipredicate, p_154245_ -> !p_154245_.is(PagamosBlocks.pointed_bergstone), maxIterations);
	}*/

	private static boolean isValidPointedBergstonePlacement(LevelReader level, BlockPos pos, Direction dir)
	{
		BlockPos blockpos = pos.relative(dir.getOpposite());
		BlockState blockstate = level.getBlockState(blockpos);
		return blockstate.isFaceSturdy(level, blockpos, dir) || isPointedBergstoneWithDirection(blockstate, dir);
	}

	private static boolean isTip(BlockState state, boolean isTipMerge)
	{
		if (!state.is(PagamosBlocks.pointed_bergstone))
		{
			return false;
		}
		else
		{
			DripstoneThickness dripstonethickness = state.getValue(THICKNESS);
			return dripstonethickness == DripstoneThickness.TIP || isTipMerge && dripstonethickness == DripstoneThickness.TIP_MERGE;
		}
	}

	private static boolean isUnmergedTipWithDirection(BlockState state, Direction dir)
	{
		return isTip(state, false) && state.getValue(TIP_DIRECTION) == dir;
	}

	private static boolean isStalactite(BlockState state)
	{
		return isPointedBergstoneWithDirection(state, Direction.DOWN);
	}

	private static boolean isStalagmite(BlockState state)
	{
		return isPointedBergstoneWithDirection(state, Direction.UP);
	}

	private static boolean isStalactiteStartPos(BlockState state, LevelReader level, BlockPos pos)
	{
		return isStalactite(state) && !level.getBlockState(pos.above()).is(PagamosBlocks.pointed_bergstone);
	}

	@Override
	protected boolean isPathfindable(BlockState p_154112_, PathComputationType p_154115_)
	{
		return false;
	}

	private static boolean isPointedBergstoneWithDirection(BlockState state, Direction dir)
	{
		return state.is(PagamosBlocks.pointed_bergstone) && state.getValue(TIP_DIRECTION) == dir;
	}

	private static boolean canGrow(BlockState dripstoneState, BlockState state)
	{
		return dripstoneState.is(PagamosBlocks.bergstone) && state.is(Blocks.WATER) && state.getFluidState().isSource();
	}

	private static Fluid getDripFluid(Level level, Fluid fluid)
	{
		if (fluid.isSame(Fluids.EMPTY))
		{
			return level.dimensionType().ultraWarm() ? Fluids.LAVA : Fluids.WATER;
		}
		else
		{
			return fluid;
		}
	}

	private static Optional<BlockPos> findBlockVertical(LevelAccessor level, BlockPos pos, Direction.AxisDirection axis, BiPredicate<BlockPos, BlockState> positionalStatePredicate, Predicate<BlockState> statePredicate, int maxIterations)
	{
		Direction direction = Direction.get(axis, Direction.Axis.Y);
		BlockPos.MutableBlockPos blockpos$mutableblockpos = pos.mutable();

		for (int i = 1; i < maxIterations; i++)
		{
			blockpos$mutableblockpos.move(direction);
			BlockState blockstate = level.getBlockState(blockpos$mutableblockpos);
			if (statePredicate.test(blockstate))
			{
				return Optional.of(blockpos$mutableblockpos.immutable());
			}

			if (level.isOutsideBuildHeight(blockpos$mutableblockpos.getY()) || !positionalStatePredicate.test(blockpos$mutableblockpos, blockstate))
			{
				return Optional.empty();
			}
		}

		return Optional.empty();
	}
}
