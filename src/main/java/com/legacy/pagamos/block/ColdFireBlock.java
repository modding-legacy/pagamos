package com.legacy.pagamos.block;

import com.legacy.pagamos.data.PagamosTags;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;

public class ColdFireBlock extends BaseFireBlock
{
	public static final MapCodec<ColdFireBlock> CODEC = simpleCodec(ColdFireBlock::new);

	@Override
	public MapCodec<ColdFireBlock> codec()
	{
		return CODEC;
	}

	public ColdFireBlock(BlockBehaviour.Properties properties)
	{
		super(properties, 0.0F);
	}

	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		BlockState support = level.getBlockState(pos.below());
		if (support.is(PagamosTags.Blocks.BLUE_FIRE_INFINIBURN))
			return;

		level.destroyBlock(pos, false);
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
	}

	@Override
	protected void entityInside(BlockState state, Level level, BlockPos pos, Entity entity)
	{
		if (entity.canFreeze() && level instanceof ServerLevel sl)
		{
			if (entity.hurtServer(sl, level.damageSources().freeze(), 2.0F))
				entity.setTicksFrozen(Math.max(entity.getTicksFrozen(), entity.getTicksRequiredToFreeze() + (10 * 20)));
		}
	}

	@Override
	protected void onPlace(BlockState state, Level level, BlockPos pos, BlockState oldState, boolean isMoving)
	{
		super.onPlace(state, level, pos, oldState, isMoving);
	}

	@Override
	protected boolean canBurn(BlockState state)
	{
		return false;
	}
}
