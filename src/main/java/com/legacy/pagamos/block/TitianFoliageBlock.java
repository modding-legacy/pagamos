package com.legacy.pagamos.block;

import com.legacy.pagamos.data.PagamosTags;
import com.mojang.serialization.MapCodec;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.state.BlockState;

public class TitianFoliageBlock extends BushBlock
{
	public static final MapCodec<TitianFoliageBlock> CODEC = simpleCodec(TitianFoliageBlock::new);

	@Override
	protected MapCodec<? extends TitianFoliageBlock> codec()
	{
		return CODEC;
	}

	public TitianFoliageBlock(Properties props)
	{
		super(props);
	}

	@Override
	protected boolean mayPlaceOn(BlockState state, BlockGetter level, BlockPos pos)
	{
		return super.mayPlaceOn(state, level, pos) || state.is(PagamosTags.Blocks.CAN_SUSTAIN_TITIAN_PLANT);
	}
}
