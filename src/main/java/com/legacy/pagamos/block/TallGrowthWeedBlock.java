package com.legacy.pagamos.block;

import com.legacy.pagamos.data.PagamosTags;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.DoublePlantBlock;
import net.minecraft.world.level.block.state.BlockState;

public class TallGrowthWeedBlock extends DoublePlantBlock
{
	public TallGrowthWeedBlock(Properties props)
	{
		super(props);
	}

	@Override
	protected boolean mayPlaceOn(BlockState state, BlockGetter level, BlockPos pos)
	{
		return super.mayPlaceOn(state, level, pos) || state.is(PagamosTags.Blocks.CAN_SUSTAIN_TITIAN_PLANT);
	}
}
