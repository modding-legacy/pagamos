package com.legacy.pagamos.block;

import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.NyliumBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.lighting.LightEngine;
import net.neoforged.neoforge.common.util.TriState;

public class MossGrowthBlock extends NyliumBlock
{
	public MossGrowthBlock(Properties props)
	{
		super(props);
	}

	private static boolean canStayMossy(BlockState state, LevelReader reader, BlockPos pos)
	{
		BlockPos blockpos = pos.above();
		BlockState blockstate = reader.getBlockState(blockpos);
		int i = LightEngine.getLightBlockInto(state, blockstate, Direction.UP, blockstate.getLightBlock());
		return i < 15;
	}

	@Override
	protected void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		if (!canStayMossy(state, level, pos))
			level.setBlockAndUpdate(pos, PagamosBlocks.bergstone.defaultBlockState());
	}

	@Override
	public TriState canSustainPlant(BlockState state, BlockGetter level, BlockPos soilPosition, Direction facing, BlockState plant)
	{
		return super.canSustainPlant(state, level, soilPosition, facing, plant);
	}

	@Override
	public void performBonemeal(ServerLevel p_221825_, RandomSource p_221826_, BlockPos p_221827_, BlockState p_221828_)
	{
		/*BlockState blockstate = p_221825_.getBlockState(p_221827_);
		BlockPos blockpos = p_221827_.above();
		ChunkGenerator chunkgenerator = p_221825_.getChunkSource().getGenerator();
		Registry<ConfiguredFeature<?, ?>> registry = p_221825_.registryAccess().lookupOrThrow(Registries.CONFIGURED_FEATURE);
		
		if (blockstate.is(PagamosBlocks.titian_growth))
		{
			this.place(registry, NetherFeatures.CRIMSON_FOREST_VEGETATION_BONEMEAL, p_221825_, chunkgenerator, p_221826_, blockpos);
		}*/
	}
}
