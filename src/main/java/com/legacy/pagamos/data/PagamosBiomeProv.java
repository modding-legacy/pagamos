package com.legacy.pagamos.data;

import com.legacy.pagamos.registry.PagamosFeatures;
import com.legacy.pagamos.registry.PagamosParticles;
import com.legacy.pagamos.registry.PagamosSounds;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.world.level.biome.AmbientAdditionsSettings;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.AmbientParticleSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;

public interface PagamosBiomeProv
{
	int DEFAULT_FOG_COLOR = 0x0b1026, WATER_COLOR = 0xCCFFFF, WATER_FOG_COLOR = 0x95c7c7;

	int SHIMMERING_SHEETS_FOG_COLOR = 0x151c3b, TITANIC_FOG_COLOR = 0x5d3d18, MISTY_DRIFTS_FOG_COLOR = 0x59647b;

	public static Biome shiveringWastes(BootstrapContext<?> bootstrap)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		PagamosFeatures.addDefaultSpawns(spawns);

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.CRYSTAL_CLUMP_UNCOMMON);

		PagamosFeatures.addDefaultFeatures(builder);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(-2.0F).downfall(0.0F).specialEffects(defaultEffects(DEFAULT_FOG_COLOR).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	public static Biome shimmeringSheets(BootstrapContext<?> bootstrap)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		PagamosFeatures.addDefaultSpawns(spawns);

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.CRYSTAL_CLUMP_COMMON);

		PagamosFeatures.addDefaultFeatures(builder);

		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.HUGE_CRYSTAL);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(-2.0F).downfall(0.0F).specialEffects(defaultEffects(SHIMMERING_SHEETS_FOG_COLOR).ambientParticle(new AmbientParticleSettings(PagamosParticles.CRYSTAL_SHAVING, 0.003F)).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	public static Biome mistyDrifts(BootstrapContext<?> bootstrap)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		PagamosFeatures.addDefaultSpawns(spawns);

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		PagamosFeatures.addDefaultFeatures(builder);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(-2.0F).downfall(0.0F).specialEffects(defaultEffects(MISTY_DRIFTS_FOG_COLOR).ambientLoopSound(PagamosSounds.MISTY_DRIFTS_LOOP).ambientAdditionsSound(new AmbientAdditionsSettings(PagamosSounds.MISTY_DRIFTS_ADDITIONS, 0.004D)).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	public static Biome titanicWilds(BootstrapContext<?> bootstrap)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		PagamosFeatures.addDefaultSpawns(spawns);

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		PagamosFeatures.addDefaultFeatures(builder);

		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.SALMON_VINES);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.HUGE_MAROON_MUSHROOM);

		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.TITANWEED_64);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.TALL_TITANEWEED_16);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.MAROON_MUSHROOM_10);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.LUMIBULB_10);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(-2.0F).downfall(0.0F).specialEffects(defaultEffects(TITANIC_FOG_COLOR).ambientParticle(new AmbientParticleSettings(PagamosParticles.TITIAN_SPORE, 0.004F)).ambientLoopSound(PagamosSounds.TITANIC_WILDS_LOOP).ambientAdditionsSound(new AmbientAdditionsSettings(PagamosSounds.TITANIC_WILDS_ADDITIONS, 0.004D)).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	private static BiomeSpecialEffects.Builder defaultEffects(int fog)
	{
		// @formatter:off
		return new BiomeSpecialEffects.Builder()
				.ambientLoopSound(PagamosSounds.FROZEN_HELL_LOOP)
				.ambientAdditionsSound(new AmbientAdditionsSettings(PagamosSounds.FROZEN_HELL_ADDITIONS, 0.004D))
				.waterColor(WATER_COLOR)
				.waterFogColor(WATER_FOG_COLOR)
				.fogColor(fog)
				.skyColor(fog)
				/*.ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS)*/;
		// @formatter:on
	}

	public static class RegistrarBuilder extends BiomeGenerationSettings.Builder
	{
		public RegistrarBuilder(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter)
		{
			super(featureGetter, carverGetter);
		}

		public BiomeGenerationSettings.Builder addFeature(GenerationStep.Decoration step, Pointer<PlacedFeature> registrar)
		{
			return super.addFeature(step, registrar.getKey());
		}
	}
}
