package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosBlocks.*;
import static com.legacy.pagamos.registry.PagamosItems.*;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.google.common.collect.ImmutableList;
import com.legacy.pagamos.Pagamos;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.tags.TagKey;
import net.minecraft.world.flag.FeatureFlagSet;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.BlastingRecipe;
import net.minecraft.world.item.crafting.CampfireCookingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.item.crafting.SmokingRecipe;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.SlabBlock;
import net.neoforged.neoforge.common.Tags;

@SuppressWarnings("unused")
public class PagamosRecipeProv extends VanillaRecipeProvider
{
	public static class Runner extends RecipeProvider.Runner
	{
		public Runner(PackOutput output, CompletableFuture<HolderLookup.Provider> registries)
		{
			super(output, registries);
		}

		@Override
		protected RecipeProvider createRecipeProvider(HolderLookup.Provider lookup, RecipeOutput output)
		{
			return new PagamosRecipeProv(lookup, output);
		}

		@Override
		public String getName()
		{
			return Pagamos.MODID + " Recipe Gen";
		}
	}

	private String hasItem = "has_item";

	private HolderGetter<Item> items;

	private PagamosRecipeProv(HolderLookup.Provider lookupProvider, RecipeOutput output)
	{
		super(lookupProvider, output);
		this.items = lookupProvider.lookupOrThrow(Registries.ITEM);
	}

	@Override
	protected void buildRecipes()
	{
		/*this.generateForEnabledBlockFamilies(FeatureFlagSet.of(FeatureFlags.VANILLA));*/

		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, bergstone, bergstone_slab, bergstone_stairs, bergstone_wall, true);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, bergstone_bricks, bergstone_brick_slab, bergstone_brick_stairs, bergstone_brick_wall, true);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, polished_bergstone, polished_bergstone_slab, polished_bergstone_stairs, polished_bergstone_wall, true);

		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, cobbled_bergstone, cobbled_bergstone_slab, cobbled_bergstone_stairs, cobbled_bergstone_wall, true);

		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, mistshale, mistshale_slab, mistshale_stairs, mistshale_wall, true);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, mistshale_bricks, mistshale_brick_slab, mistshale_brick_stairs, mistshale_brick_wall, true);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, polished_mistshale, polished_mistshale_slab, polished_mistshale_stairs, polished_mistshale_wall, true);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, mistshale_tiles, mistshale_tile_slab, mistshale_tile_stairs, mistshale_tile_wall, true);

		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, crystal_plated_tiles, crystal_plated_tile_slab, crystal_plated_tile_stairs, crystal_plated_tile_wall, true);

		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, igloo_bricks, igloo_brick_slab, igloo_brick_stairs, igloo_brick_wall, true);

		this.twoByTwoPacker(RecipeCategory.BUILDING_BLOCKS, polished_bergstone, bergstone, 4);
		this.twoByTwoPacker(RecipeCategory.BUILDING_BLOCKS, bergstone_bricks, polished_bergstone, 4);
		this.twoByTwoPacker(RecipeCategory.BUILDING_BLOCKS, polished_mistshale, mistshale, 4);
		this.twoByTwoPacker(RecipeCategory.BUILDING_BLOCKS, mistshale_bricks, polished_mistshale, 4);

		this.shaped(RecipeCategory.BUILDING_BLOCKS, crystal_plated_tiles, 4).define('A', azure_crystal_block).define('M', mauve_crystal_block).pattern("MA").pattern("AM").unlockedBy(hasItem, has(azure_crystal_block)).unlockedBy(hasItem, has(mauve_crystal_block)).save(this.output);

		// adds extra stuff to base bergstone/mistshale
		stoneCutting(bergstone, List.of(bergstone_bricks, bergstone_brick_slab, bergstone_brick_stairs, bergstone_brick_wall, polished_bergstone, polished_bergstone_slab, polished_bergstone_stairs, polished_bergstone_wall, chiseled_bergstone));
		stoneCutting(mistshale, List.of(mistshale_bricks, mistshale_brick_slab, mistshale_brick_stairs, mistshale_brick_wall, polished_mistshale, polished_mistshale_slab, polished_mistshale_stairs, polished_mistshale_wall, mistshale_tiles, mistshale_tile_slab, mistshale_tile_stairs, mistshale_tile_wall));
		stoneCutting(mistshale_bricks, List.of(mistshale_tiles, mistshale_tile_slab, mistshale_tile_stairs, mistshale_tile_wall));

		this.nineBlockStorageRecipes(RecipeCategory.MISC, tourmaline, RecipeCategory.BUILDING_BLOCKS, tourmaline_block);
		this.nineBlockStorageRecipes(RecipeCategory.MISC, sapphire, RecipeCategory.BUILDING_BLOCKS, sapphire_block);

		this.twoByTwoPacker(RecipeCategory.BUILDING_BLOCKS, azure_crystal_block, azure_crystal_shard);

		this.twoByTwoPacker(RecipeCategory.BUILDING_BLOCKS, igloo_bricks, ice_brick);

		this.threeByThreePacker(RecipeCategory.BUILDING_BLOCKS, crystal_ice, ice_shard);

		this.shapeless(RecipeCategory.TOOLS, flint_and_lapis).requires(Items.FLINT).requires(Tags.Items.STORAGE_BLOCKS_LAPIS).unlockedBy(hasItem, has(Tags.Items.GEMS_LAPIS)).unlockedBy(hasItem, has(Items.FLINT)).unlockedBy(hasItem, has(Items.OBSIDIAN));

		// TODO: This used to be bricks, and made 2. Do we keep that...?
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.MISC, ice_rod, 1).define('#', ice_shard).pattern("#").pattern("#").unlockedBy(hasItem, has(ice_shard)).save(this.output);

		var iceRod = PagamosTags.Items.ICE_RODS;
		var tourmalineTag = PagamosTags.Items.TOURMALINE_TOOL_MATERIALS;
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, tourmaline_helmet).define('#', tourmalineTag).pattern("###").pattern("# #").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, tourmaline_chestplate).define('#', tourmalineTag).pattern("# #").pattern("###").pattern("###").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, tourmaline_leggings).define('#', tourmalineTag).pattern("###").pattern("# #").pattern("# #").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, tourmaline_boots).define('#', tourmalineTag).pattern("# #").pattern("# #").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);

		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, tourmaline_sword).define('#', iceRod).define('X', tourmalineTag).pattern("X").pattern("X").pattern("#").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, tourmaline_pickaxe).define('#', iceRod).define('X', tourmalineTag).pattern("XXX").pattern(" # ").pattern(" # ").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, tourmaline_axe).define('#', iceRod).define('X', tourmalineTag).pattern("XX").pattern("X#").pattern(" #").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, tourmaline_shovel).define('#', iceRod).define('X', tourmalineTag).pattern("X").pattern("#").pattern("#").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, tourmaline_hoe).define('#', iceRod).define('X', tourmalineTag).pattern("XX").pattern(" #").pattern(" #").unlockedBy(hasItem, has(tourmalineTag)).save(this.output);

		var sapphireTag = PagamosTags.Items.SAPPHIRE_TOOL_MATERIALS;
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, sapphire_helmet).define('#', sapphireTag).pattern("###").pattern("# #").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, sapphire_chestplate).define('#', sapphireTag).pattern("# #").pattern("###").pattern("###").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, sapphire_leggings).define('#', sapphireTag).pattern("###").pattern("# #").pattern("# #").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, sapphire_boots).define('#', sapphireTag).pattern("# #").pattern("# #").unlockedBy(hasItem, has(sapphireTag)).save(this.output);

		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, sapphire_sword).define('#', iceRod).define('X', sapphireTag).pattern("X").pattern("X").pattern("#").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, sapphire_pickaxe).define('#', iceRod).define('X', sapphireTag).pattern("XXX").pattern(" # ").pattern(" # ").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, sapphire_axe).define('#', iceRod).define('X', sapphireTag).pattern("XX").pattern("X#").pattern(" #").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, sapphire_shovel).define('#', iceRod).define('X', sapphireTag).pattern("X").pattern("#").pattern("#").unlockedBy(hasItem, has(sapphireTag)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, sapphire_hoe).define('#', iceRod).define('X', sapphireTag).pattern("XX").pattern(" #").pattern(" #").unlockedBy(hasItem, has(sapphireTag)).save(this.output);

		blasting(RecipeCategory.MISC, PagamosTags.Items.TOURMALINE_ORES, tourmaline, 0.9F);
		blasting(RecipeCategory.MISC, PagamosTags.Items.SAPPHIRE_ORES, sapphire, 0.9F);

		// cooking(frozen_antinatric_bricks,
		// PagamosBlocks.cracked_frozen_antinatric_bricks, 0.1F);
	}

	@Override
	protected void generateForEnabledBlockFamilies(FeatureFlagSet featureFlags)
	{
		PagamosBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateRecipe).forEach(family -> this.generateRecipes(family, featureFlags));
	}

	private void simple2x2(RecipeCategory cat, ItemLike item, ItemLike output, int amount)
	{
		ShapedRecipeBuilder.shaped(this.items, cat, output, amount).define('#', item).pattern("##").pattern("##").unlockedBy(hasItem, has(item)).save(this.output);
	}

	private void simple2x2(RecipeCategory cat, ItemLike item, ItemLike output)
	{
		simple2x2(cat, item, output, 1);
	}

	private void simple3x3(RecipeCategory cat, ItemLike item, ItemLike output, int amount)
	{
		ShapedRecipeBuilder.shaped(this.items, cat, output, amount).define('#', item).pattern("###").pattern("###").pattern("###").unlockedBy(hasItem, has(item)).save(this.output);
	}

	private void simple3x3(RecipeCategory cat, ItemLike item, ItemLike output)
	{
		simple3x3(cat, item, output, 1);
	}

	private void slabsStairs(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair)
	{
		slabs(cat, block, slab).save(this.output);
		stairs(cat, block, stair).save(this.output);
	}

	private void slabsStairsWalls(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair, ItemLike wall)
	{
		slabsStairs(cat, block, slab, stair);
		walls(block, wall);
	}

	private void slabsStairs(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair, boolean withStoneCutting)
	{
		slabsStairs(cat, block, slab, stair);

		if (withStoneCutting)
			stoneCutting(block, ImmutableList.of(slab, stair));
	}

	private void slabsStairsWalls(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair, ItemLike wall, boolean withStoneCutting)
	{
		slabsStairsWalls(cat, block, slab, stair, wall);

		if (withStoneCutting)
			stoneCutting(block, ImmutableList.of(slab, stair, wall));
	}

	private ShapedRecipeBuilder slabs(RecipeCategory cat, ItemLike ingredient, ItemLike slab)
	{
		return ShapedRecipeBuilder.shaped(this.items, cat, slab, 6).define('#', ingredient).pattern("###").unlockedBy(hasItem, has(ingredient));
	}

	private ShapedRecipeBuilder stairs(RecipeCategory cat, ItemLike ingredient, ItemLike stair)
	{
		return ShapedRecipeBuilder.shaped(this.items, cat, stair, 4).define('#', ingredient).pattern("#  ").pattern("## ").pattern("###").unlockedBy(hasItem, has(ingredient));
	}

	private void walls(ItemLike ingredient, ItemLike wall)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, wall, 6).define('#', ingredient).pattern("###").pattern("###").unlockedBy(hasItem, has(ingredient)).save(this.output);
	}

	private void fencesGates(ItemLike plank, ItemLike fence, ItemLike gate)
	{
		fences(plank, fence);
		gates(plank, gate);
	}

	private void fences(ItemLike plank, ItemLike fence)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, fence, 3).define('P', plank).define('S', Ingredient.of(this.items.getOrThrow(Tags.Items.RODS_WOODEN))).pattern("PSP").pattern("PSP").group("wooden_fence").unlockedBy(hasItem, has(plank)).save(this.output);
	}

	private void gates(ItemLike plank, ItemLike gate)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, gate).define('P', plank).define('S', Ingredient.of(this.items.getOrThrow(Tags.Items.RODS_WOODEN))).pattern("SPS").pattern("SPS").group("wooden_fence_gate").unlockedBy(hasItem, has(plank)).save(this.output);
	}

	private void stoneCutting(ItemLike ingredient, List<ItemLike> results)
	{
		results.forEach(result ->
		{
			SingleItemRecipeBuilder.stonecutting(Ingredient.of(ingredient), RecipeCategory.BUILDING_BLOCKS, result, result instanceof SlabBlock ? 2 : 1).unlockedBy(hasItem, has(ingredient)).save(this.output, Pagamos.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_stonecutting_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
		});
	}

	private void cooking(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
	}

	private <T extends AbstractCookingRecipe> void cooking(RecipeCategory category, ItemLike ingredient, ItemLike result, float exp, int time, RecipeSerializer<T> type, AbstractCookingRecipe.Factory<T> factory)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(ingredient), category, result, exp, time, type, factory).unlockedBy(hasItem, has(ingredient)).save(this.output, Pagamos.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_from_" + BuiltInRegistries.RECIPE_SERIALIZER.getKey(type).getPath() + "_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
	}

	private <T extends AbstractCookingRecipe> void cooking(RecipeCategory category, ItemLike ingredient, ItemLike result, float exp, int time, RecipeSerializer<T> type, AbstractCookingRecipe.Factory<T> factory, String group)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(ingredient), category, result, exp, time, type, factory).unlockedBy(hasItem, has(ingredient)).group(group).save(this.output, Pagamos.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_from_" + BuiltInRegistries.RECIPE_SERIALIZER.getKey(type).getPath() + "_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
	}

	private void smoking(TagKey<Item> ingredient, ItemLike result, float exp)
	{
		cooking(RecipeCategory.FOOD, ingredient, result, exp);
		cooking(RecipeCategory.FOOD, ingredient, result, exp, 100, RecipeSerializer.SMOKING_RECIPE, SmokingRecipe::new);
		cooking(RecipeCategory.FOOD, ingredient, result, exp, 600, RecipeSerializer.CAMPFIRE_COOKING_RECIPE, CampfireCookingRecipe::new);
	}

	private void blasting(RecipeCategory cat, TagKey<Item> ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp);
		cooking(cat, ingredient, result, exp, 100, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new);
	}

	private void cooking(RecipeCategory cat, TagKey<Item> ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
	}

	private <T extends AbstractCookingRecipe> void cooking(RecipeCategory cat, TagKey<Item> ingredient, ItemLike result, float exp, int time, RecipeSerializer<T> smeltingRecipe, AbstractCookingRecipe.Factory<T> recipeFactory)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(this.items.getOrThrow(ingredient)), cat, result, exp, time, smeltingRecipe, recipeFactory).unlockedBy(hasItem, has(ingredient)).save(this.output, Pagamos.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_from_" + BuiltInRegistries.RECIPE_SERIALIZER.getKey(smeltingRecipe).getPath()));
	}

	private void smoking(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
		cooking(cat, ingredient, result, exp, 100, RecipeSerializer.SMOKING_RECIPE, SmokingRecipe::new);
		cooking(cat, ingredient, result, exp, 600, RecipeSerializer.CAMPFIRE_COOKING_RECIPE, CampfireCookingRecipe::new);
	}

	private void blasting(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
		cooking(cat, ingredient, result, exp, 100, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new);
	}

	protected void twoByTwoPacker(RecipeCategory category, ItemLike packed, ItemLike unpacked, int count)
	{
		this.shaped(category, packed, count).define('#', unpacked).pattern("##").pattern("##").unlockedBy(getHasName(unpacked), this.has(unpacked)).save(this.output);
	}
}