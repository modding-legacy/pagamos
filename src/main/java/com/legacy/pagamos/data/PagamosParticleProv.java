package com.legacy.pagamos.data;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.registry.PagamosParticles;

import net.minecraft.data.PackOutput;
import net.neoforged.neoforge.common.data.ParticleDescriptionProvider;

public class PagamosParticleProv extends ParticleDescriptionProvider
{
	public PagamosParticleProv(PackOutput output)
	{
		super(output);
	}

	@Override
	protected void addDescriptions()
	{
		this.spriteSet(PagamosParticles.CRYSTAL_SHAVING, Pagamos.locate("crystal_shaving"), 2, false);
		this.spriteSet(PagamosParticles.TITIAN_SPORE, Pagamos.locate("titian_spore"), 2, false);
	}
}