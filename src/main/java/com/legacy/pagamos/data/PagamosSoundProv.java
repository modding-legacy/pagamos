package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosSounds.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.legacy.pagamos.Pagamos;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.neoforged.neoforge.common.data.SoundDefinition;
import net.neoforged.neoforge.common.data.SoundDefinition.Sound;
import net.neoforged.neoforge.common.data.SoundDefinition.SoundType;
import net.neoforged.neoforge.common.data.SoundDefinitionsProvider;

public class PagamosSoundProv extends SoundDefinitionsProvider
{
	public PagamosSoundProv(PackOutput output)
	{
		super(output, Pagamos.MODID);
	}

	@Override
	public void registerSounds()
	{
		String block = "pagamos_portal";
		this.add(BLOCK_PORTAL_AMBIENT, blockDef(block, "ambient", 1).subtitle(blockSub(block + ".ambient")));
		this.add(BLOCK_PORTAL_TRIGGER, blockDef(block, "trigger", 1).subtitle(blockSub(block + ".trigger")));
		this.add(BLOCK_PORTAL_TRAVEL, blockDef(block, "travel", 1).subtitle(blockSub(block + ".travel")));
		this.add(BLOCK_PORTAL_LIGHT, blockDef(block, "light", 1).subtitle(blockSub(block + ".light")));
		this.add(BLOCK_PORTAL_DESTROYED, blockDef(block, "destroy", 1).subtitle(blockSub(block + ".destroy")));

		String entity = "crystal_phoenix";
		this.add(ENTITY_CRYSTAL_PHOENIX_IDLE, entityDef(entity, "idle", 1));
		this.add(ENTITY_CRYSTAL_PHOENIX_HURT, definition().with(event(ENTITY_CRYSTAL_PHOENIX_IDLE)).subtitle(entitySub(entity + ".hurt")));
		this.add(ENTITY_CRYSTAL_PHOENIX_DEATH, entityDef(entity, "death", 1));
		this.add(ENTITY_CRYSTAL_PHOENIX_ATTACK, entityDef(entity, "attack", 1));

		entity = "ice_creeper";
		this.add(ENTITY_ICE_CREEPER_HURT, definition().with(event(SoundEvents.CREEPER_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(ENTITY_ICE_CREEPER_DEATH, definition().with(event(SoundEvents.CREEPER_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(ENTITY_ICE_CREEPER_PRIMED, definition().with(event(SoundEvents.CREEPER_PRIMED)).subtitle(entitySub(entity + ".primed")));

		this.add(RANDOM_ICE_CRACK, definition().with(mod("ice_crack")));

		String addy = "ambient/additions/";
		
		var fhaDef = definition();

		for (int i = 1; i <= 3; i++)
			fhaDef.with(mod(addy + "echo_" + i));

		this.add(FROZEN_HELL_LOOP.value(), definition().with(mod("ambient/frozen_hell_loop").preload()));
		this.add(FROZEN_HELL_ADDITIONS.value(), fhaDef);

		this.add(MISTY_DRIFTS_LOOP.value(), definition().with(mod("ambient/misty_drifts_loop").preload()));

		var taDef = definition();

		for (int i = 1; i <= 3; i++)
			taDef.with(mod(addy + "titanic_wilds/hum_" + i));

		for (int i = 1; i <= 6; i++)
			taDef.with(mod(addy + "titanic_wilds/moist_" + i));

		this.add(TITANIC_WILDS_LOOP.value(), definition().with(mod("ambient/titanic_wilds_loop").preload()));
		this.add(TITANIC_WILDS_ADDITIONS.value(), taDef);

	}

	protected static SoundDefinition def(Dir dir, String name, String subtitle)
	{
		return def(dir, name, 1, subtitle);
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle)
	{
		return def(dir, name, count, subtitle, s ->
		{
		});
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle, Consumer<Sound> extra)
	{
		var def = count > 1 ? definition().with(mod(dir.folderName.concat(name), count, extra)) : definition().with(mod(dir.folderName.concat(name), extra));
		return def.subtitle(dir == Dir.ENTITY ? entitySub(subtitle) : dir == Dir.BLOCKS ? blockSub(subtitle) : dir == Dir.ITEMS ? itemSub(subtitle) : subtitle);
	}

	protected static SoundDefinition.Sound mod(final String name)
	{
		return mod(name, s ->
		{
		});
	}

	protected static SoundDefinition.Sound mod(final String name, Consumer<Sound> extra)
	{
		var sound = sound(Pagamos.locate(name));
		extra.accept(sound);
		return sound;
	}

	protected static Sound[] mod(String name, int count)
	{
		return mod(name, count, s ->
		{
		});
	}

	protected static Sound[] mod(String name, int count, Consumer<Sound> extra)
	{
		List<Sound> sounds = new ArrayList<>();

		for (int i = 1; i <= count; ++i)
		{
			Sound sound = mod(name.concat("_" + i));
			extra.accept(sound);
			sounds.add(sound);
		}
		return sounds.toArray(new Sound[count]);
	}

	protected static SoundDefinition.Sound event(final SoundEvent event)
	{
		return sound(BuiltInRegistries.SOUND_EVENT.getKey(event), SoundType.EVENT);
	}

	protected static String subMc(final String name)
	{
		return "subtitles.".concat(name);
	}

	protected static String sub(final String name)
	{
		return subMc(Pagamos.MODID + "." + name);
	}

	protected static String blockSub(final String name)
	{
		return sub("block.".concat(name));
	}

	protected static String itemSub(final String name)
	{
		return sub("item.".concat(name));
	}

	protected static String entitySub(final String name)
	{
		return sub("entity.".concat(name));
	}

	protected static SoundDefinition musicDef(final String name)
	{
		return musicDef(name, false);
	}

	protected static SoundDefinition musicDef(final String name, boolean preload)
	{
		String music = "music/";
		return definition().with(mod(music.concat(name)).stream().preload(preload));
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count, String subtitle)
	{
		return def(Dir.ENTITY, entityName + "/" + soundName, count, entityName + "." + subtitle);
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count)
	{
		return entityDef(entityName, soundName, count, soundName);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count)
	{
		return blockDef(blockName, soundName, count, 0);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count, final int attenuationDistance)
	{
		String defName = (blockName + "/" + soundName);
		String defSubtitle = blockName + "." + soundName;

		return def(Dir.BLOCKS, defName, count, defSubtitle, s ->
		{
			if (attenuationDistance > 0)
				s.attenuationDistance(attenuationDistance);
		});
	}

	protected static SoundDefinition itemDef(String itemName, String soundName, int count, String subtitle)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName + "." + subtitle);
	}

	protected static SoundDefinition itemDef(String itemName, String soundName, int count)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName);
	}

	private enum Dir
	{
		BLOCKS("block/"), ITEMS("item/"), ENTITY("entity/"), MUSIC("music/");

		public final String folderName;

		Dir(String folderName)
		{
			this.folderName = folderName;
		}
	}
}
