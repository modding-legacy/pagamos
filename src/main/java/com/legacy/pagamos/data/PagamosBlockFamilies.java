package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosBlocks.*;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.data.BlockFamily;
import net.minecraft.data.BlockFamily.Builder;
import net.minecraft.world.level.block.Block;

public class PagamosBlockFamilies
{
	private static final List<BlockFamily> FAMILIES = new ArrayList<>();

	// @formatter:off
	public static final BlockFamily BERGSTONE = family(bergstone)
			//.button(bergstone_button)
			//.pressurePlate(bergstone_pressure_plate)
			.slab(bergstone_slab)
			.stairs(bergstone_stairs)
			.wall(bergstone_wall)
			.getFamily();
	
	public static final BlockFamily COBBLED_BERGSTONE = family(cobbled_bergstone)
			.slab(cobbled_bergstone_slab)
			.stairs(cobbled_bergstone_stairs)
			.wall(cobbled_bergstone_wall)
			.chiseled(chiseled_bergstone)
			.getFamily();
	
	public static final BlockFamily POLISHED_BERGSTONE = family(polished_bergstone)
			.slab(polished_bergstone_slab)
			.stairs(polished_bergstone_stairs)
			.wall(polished_bergstone_wall)
			.getFamily();

	public static final BlockFamily BERGSTONE_BRICKS = family(bergstone_bricks)
			.slab(bergstone_brick_slab)
			.stairs(bergstone_brick_stairs)
			.wall(bergstone_brick_wall)
			.getFamily();
	
	public static final BlockFamily IGLOO_BRICKS = family(igloo_bricks)
			.slab(igloo_brick_slab)
			.stairs(igloo_brick_stairs)
			.wall(igloo_brick_wall)
			.getFamily();
	
	public static final BlockFamily MISTSHALE = family(mistshale)
			.slab(mistshale_slab)
			.stairs(mistshale_stairs)
			.wall(mistshale_wall)
			.getFamily();
	
	public static final BlockFamily POLISHED_MISTSHALE = family(polished_mistshale)
			.slab(polished_mistshale_slab)
			.stairs(polished_mistshale_stairs)
			.wall(polished_mistshale_wall)
			.getFamily();

	public static final BlockFamily MISTSHALE_BRICKS = family(mistshale_bricks)
			.slab(mistshale_brick_slab)
			.stairs(mistshale_brick_stairs)
			.wall(mistshale_brick_wall)
			.getFamily();
	
	public static final BlockFamily MISTSHALE_TILES = family(mistshale_tiles)
			.slab(mistshale_tile_slab)
			.stairs(mistshale_tile_stairs)
			.wall(mistshale_tile_wall)
			.getFamily();
	
	public static final BlockFamily CRYSTAL_PLATED_TILES = family(crystal_plated_tiles)
			.slab(crystal_plated_tile_slab)
			.stairs(crystal_plated_tile_stairs)
			.wall(crystal_plated_tile_wall)
			.getFamily();

	// @formatter:on

	private static Builder family(Block block)
	{
		var builder = new BlockFamily.Builder(block);

		FAMILIES.add(builder.getFamily());

		return builder;
	}

	public static List<BlockFamily> getFamilies()
	{
		return FAMILIES;
	}
}