package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosBlocks.*;
import static com.legacy.pagamos.registry.PagamosItems.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import javax.annotation.Nullable;

import com.google.common.collect.ImmutableList;
import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosEquipmentAssets;

import net.minecraft.client.data.models.BlockModelGenerators;
import net.minecraft.client.data.models.EquipmentAssetProvider;
import net.minecraft.client.data.models.ItemModelGenerators;
import net.minecraft.client.data.models.ItemModelOutput;
import net.minecraft.client.data.models.ModelProvider;
import net.minecraft.client.data.models.blockstates.BlockStateGenerator;
import net.minecraft.client.data.models.blockstates.Condition;
import net.minecraft.client.data.models.blockstates.MultiPartGenerator;
import net.minecraft.client.data.models.blockstates.MultiVariantGenerator;
import net.minecraft.client.data.models.blockstates.PropertyDispatch;
import net.minecraft.client.data.models.blockstates.Variant;
import net.minecraft.client.data.models.blockstates.VariantProperties;
import net.minecraft.client.data.models.model.ItemModelUtils;
import net.minecraft.client.data.models.model.ModelInstance;
import net.minecraft.client.data.models.model.ModelLocationUtils;
import net.minecraft.client.data.models.model.ModelTemplate;
import net.minecraft.client.data.models.model.ModelTemplates;
import net.minecraft.client.data.models.model.TextureMapping;
import net.minecraft.client.data.models.model.TextureSlot;
import net.minecraft.client.data.models.model.TexturedModel;
import net.minecraft.client.renderer.item.ItemModel;
import net.minecraft.client.renderer.special.TridentSpecialRenderer;
import net.minecraft.client.resources.model.EquipmentClientInfo;
import net.minecraft.core.Direction;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.equipment.EquipmentAsset;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.DripstoneThickness;

public class PagamosModelProv extends ModelProvider
{
	public PagamosModelProv(PackOutput output)
	{
		super(output, Pagamos.MODID);
	}

	@Override
	public CompletableFuture<?> run(CachedOutput output)
	{
		return super.run(output);
	}

	@Override
	protected void registerModels(BlockModelGenerators blockModels, ItemModelGenerators itemModels)
	{
		var items = new Items(itemModels.itemModelOutput, itemModels.modelOutput);
		items.run();

		var states = new States(blockModels.blockStateOutput, blockModels.itemModelOutput, blockModels.modelOutput);
		states.run();
	}

	public static class Items extends ItemModelGenerators
	{
		public Items(ItemModelOutput output, BiConsumer<ResourceLocation, ModelInstance> models)
		{
			super(output, models);
		}

		@Override
		public void run()
		{
			this.basicItem(azure_crystal_shard);
			this.basicItem(mauve_crystal_shard);
			this.basicItem(ice_shard);
			this.basicItem(tourmaline);
			this.basicItem(sapphire);
			this.basicItem(phoenix_feather);
			this.basicItem(ice_rod);
			this.basicItem(flint_and_lapis);
			this.basicItem(ice_brick);

			this.basicItem(titian_berry);

			this.handheldItem(tourmaline_sword);
			this.handheldItem(tourmaline_pickaxe);
			this.handheldItem(tourmaline_axe);
			this.handheldItem(tourmaline_shovel);
			this.handheldItem(tourmaline_hoe);

			this.handheldItem(sapphire_sword);
			this.handheldItem(sapphire_pickaxe);
			this.handheldItem(sapphire_axe);
			this.handheldItem(sapphire_shovel);
			this.handheldItem(sapphire_hoe);

			this.generateSpear(phoenix_spear);

			this.generateTrimmableItem(tourmaline_helmet, PagamosEquipmentAssets.TOURMALINE, SLOT_HELMET, false);
			this.generateTrimmableItem(tourmaline_chestplate, PagamosEquipmentAssets.TOURMALINE, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(tourmaline_leggings, PagamosEquipmentAssets.TOURMALINE, SLOT_LEGGINS, false);
			this.generateTrimmableItem(tourmaline_boots, PagamosEquipmentAssets.TOURMALINE, SLOT_BOOTS, false);

			this.generateTrimmableItem(sapphire_helmet, PagamosEquipmentAssets.SAPPHIRE, SLOT_HELMET, false);
			this.generateTrimmableItem(sapphire_chestplate, PagamosEquipmentAssets.SAPPHIRE, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(sapphire_leggings, PagamosEquipmentAssets.SAPPHIRE, SLOT_LEGGINS, false);
			this.generateTrimmableItem(sapphire_boots, PagamosEquipmentAssets.SAPPHIRE, SLOT_BOOTS, false);

			this.generateSpawnEgg(yeti_spawn_egg, 0xffffff, 0xc1bc9f);
			this.generateSpawnEgg(crystal_phoenix_spawn_egg, 0x4083f1, 0xb4cdf7);
			this.generateSpawnEgg(ice_creeper_spawn_egg, 0xb5dee4, 0);
		}

		private void basicItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_ITEM);
		}

		private void handheldItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_HANDHELD_ITEM);
		}

		public void generateSpear(Item spearItem)
		{
			ItemModel.Unbaked basicModel = ItemModelUtils.plainModel(this.createFlatItemModel(spearItem, ModelTemplates.FLAT_ITEM));
			ItemModel.Unbaked holding = ItemModelUtils.plainModel(ModelLocationUtils.getModelLocation(spearItem, "_handheld"));
			ItemModel.Unbaked throwing = holding;/*ItemModelUtils.specialModel(ModelLocationUtils.getModelLocation(spearItem, "_throwing"), new TridentSpecialRenderer.Unbaked());*/
			ItemModel.Unbaked holdingThrowingDecider = ItemModelUtils.conditional(ItemModelUtils.isUsingItem(), throwing, holding);
			this.itemModelOutput.accept(spearItem, createFlatModelDispatch(basicModel, holdingThrowingDecider));
		}
	}

	public static class Equipment extends EquipmentAssetProvider
	{
		private final PackOutput.PathProvider pathProvider;

		public Equipment(PackOutput output)
		{
			super(output);
			this.pathProvider = output.createPathProvider(PackOutput.Target.RESOURCE_PACK, "equipment");
		}

		@Override
		public CompletableFuture<?> run(CachedOutput p_387304_)
		{
			Map<ResourceKey<EquipmentAsset>, EquipmentClientInfo> map = new HashMap<>();
			bootstrap((asset, info) ->
			{
				if (map.putIfAbsent(asset, info) != null)
					throw new IllegalStateException("Tried to register equipment asset twice for id: " + asset);
			});

			return DataProvider.saveAll(p_387304_, EquipmentClientInfo.CODEC, this.pathProvider::json, map);
		}

		static void bootstrap(BiConsumer<ResourceKey<EquipmentAsset>, EquipmentClientInfo> output)
		{
			output.accept(PagamosEquipmentAssets.TOURMALINE, onlyHumanoid(Pagamos.find("sapphire")));
			output.accept(PagamosEquipmentAssets.SAPPHIRE, onlyHumanoid(Pagamos.find("sapphire")));
		}
	}

	public static class States extends BlockModelGenerators
	{
		public States(Consumer<BlockStateGenerator> blockStateOutput, ItemModelOutput itemModelOutput, BiConsumer<ResourceLocation, ModelInstance> modelOutput)
		{
			super(blockStateOutput, itemModelOutput, modelOutput);
		}

		@Override
		public void run()
		{
			fullBlockModelCustomGenerators = new HashMap<Block, BlockModelGenerators.BlockStateGeneratorSupplier>(fullBlockModelCustomGenerators);
			fullBlockModelCustomGenerators.put(bergstone, BlockModelGenerators::createMirroredCubeGenerator);
			/*fullBlockModelCustomGenerators.put(igloo_bricks, States::createTranslucentCubeGenerator);*/

			texturedModels = new HashMap<Block, TexturedModel>(texturedModels);
			texturedModels.put(igloo_bricks, TexturedModel.createDefault(TextureMapping::cube, ModelTemplates.CUBE_ALL.extend().renderType(ResourceLocation.parse("translucent")).build()).get(igloo_bricks));

			PagamosBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateModel).forEach(fam -> this.family(fam.getBaseBlock()).generateFor(fam));

			// this.createRotatedMirroredVariantBlock(packed_snow);

			this.simpleBlock(packed_snow);

			this.simpleBlock(creeper_ice, "translucent");
			this.simpleBlock(crystal_ice, "translucent");

			this.simpleBlock(snowed_tourmaline_ore);
			this.simpleBlock(bergstone_tourmaline_ore);
			this.simpleBlock(tourmaline_block);

			this.simpleBlock(snowed_sapphire_ore);
			this.simpleBlock(bergstone_sapphire_ore);
			this.simpleBlock(sapphire_block);

			this.createPortal(pagamos_portal);

			this.createFurnace(freezer, TexturedModel.ORIENTABLE_ONLY_TOP);

			this.createBlueFire();

			this.simpleBlock(azure_crystal_block);
			this.simpleBlock(mauve_crystal_block);
			this.simpleBlock(budding_azure_crystal_block);
			this.simpleBlock(budding_mauve_crystal_block);

			this.createCrystalClusters();

			this.createGrowthBlock(titian_growth, bergstone);

			this.createCrossBlock(short_titianeweed, BlockModelGenerators.PlantType.NOT_TINTED);
			this.registerSimpleItemModel(short_titianeweed, this.createFlatItemModelWithBlockTexture(short_titianeweed.asItem(), short_titianeweed));
			this.createDoublePlantWithDefaultItem(tall_titianeweed, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(maroon_mushroom, potted_maroon_mushroom, BlockModelGenerators.PlantType.NOT_TINTED);

			this.createCrossBlock(lumibulb, BlockModelGenerators.PlantType.EMISSIVE_NOT_TINTED);
			this.registerSimpleItemModel(lumibulb, this.createFlatItemModelWithBlockTexture(lumibulb.asItem(), lumibulb));

			this.simpleBlock(salmon_moss);

			this.createMaroonMushroomBlock();

			this.createNonTemplateModelBlock(mist_geyser);

			this.createPointedDripstone();

			this.createSalmonVines();
		}

		public void createCrystalClusters()
		{
			/*this.createAmethystCluster(small_azure_crystal_bud);
			this.createAmethystCluster(medium_azure_crystal_bud);
			this.createAmethystCluster(large_azure_crystal_bud);*/
			this.createAmethystCluster(azure_crystal);

			/*this.createAmethystCluster(small_mauve_crystal_bud);
			this.createAmethystCluster(medium_mauve_crystal_bud);
			this.createAmethystCluster(large_mauve_crystal_bud);*/
			this.createAmethystCluster(mauve_crystal);
		}

		public void createSalmonVines()
		{
			Block vine = salmon_vines, plant = salmon_vines_plant;
			var template = extendWithType("cutout", ModelTemplates.CROSS);
			var emissiveTemplate = extendWithType("cutout", ModelTemplates.CROSS_EMISSIVE);

			ResourceLocation resourcelocation = emissiveTemplate.create(vine, TextureMapping.crossEmissive(vine), this.modelOutput);
			ResourceLocation resourcelocation1 = emissiveTemplate.createWithSuffix(vine, "_berry", new TextureMapping().put(TextureSlot.CROSS, TextureMapping.getBlockTexture(vine, "_berry")).put(TextureSlot.CROSS_EMISSIVE, TextureMapping.getBlockTexture(vine, "_berry_emissive")), this.modelOutput);
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(vine).with(createBooleanModelDispatch(BlockStateProperties.BERRIES, resourcelocation1, resourcelocation)));

			ResourceLocation resourcelocation2 = this.createSuffixedVariant(plant, "", template, TextureMapping::cross);
			ResourceLocation resourcelocation3 = this.createSuffixedVariant(plant, "_berry", template, TextureMapping::cross);

			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(plant).with(createBooleanModelDispatch(BlockStateProperties.BERRIES, resourcelocation3, resourcelocation2)));
		}

		public static final TextureSlot PORTAL_SLOT = TextureSlot.create("portal");

		public void createPortal(Block portal)
		{
			var ns = ModelTemplates.create(PORTAL_SLOT).extend().renderType("translucent").parent(ResourceLocation.withDefaultNamespace("block/nether_portal_ns"));
			var ew = ModelTemplates.create(PORTAL_SLOT).extend().renderType("translucent").parent(ResourceLocation.withDefaultNamespace("block/nether_portal_ew"));

			ResourceLocation tex = TextureMapping.getBlockTexture(portal);
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(portal).with(PropertyDispatch.property(BlockStateProperties.HORIZONTAL_AXIS).generate((axis) ->
			{
				boolean isEw = axis == Direction.Axis.Z;
				return Variant.variant().with(VariantProperties.MODEL, (isEw ? ew : ns).build().createWithSuffix(portal, "_" + (isEw ? "ew" : "ns"), new TextureMapping().put(PORTAL_SLOT, tex).copyForced(PORTAL_SLOT, TextureSlot.PARTICLE), this.modelOutput));
			})));
		}

		public void createBlueFire()
		{
			Block booster = blue_fire;
			List<ResourceLocation> list = this.createFloorFireModels(booster);
			List<ResourceLocation> list1 = this.createSideFireModels(booster);
			this.blockStateOutput.accept(MultiPartGenerator.multiPart(booster).with(wrapModels(list, p_387713_ -> p_387713_)).with(wrapModels(list1, p_387314_ -> p_387314_)).with(wrapModels(list1, p_388922_ -> p_388922_.with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90))).with(wrapModels(list1, p_388318_ -> p_388318_.with(VariantProperties.Y_ROT, VariantProperties.Rotation.R180))).with(wrapModels(list1, p_386556_ -> p_386556_.with(VariantProperties.Y_ROT, VariantProperties.Rotation.R270))));

			/*ModelTemplates.FLAT_ITEM.create(ModelLocationUtils.getModelLocation(booster), TextureMapping.layer0(TextureMapping.getBlockTexture(booster).withSuffix("_0")), this.modelOutput);*/
		}

		public void createMaroonMushroomBlock()
		{
			Block mushroomBlock = PagamosBlocks.maroon_mushroom_block;
			ResourceLocation resourcelocation = ModelTemplates.SINGLE_FACE.create(mushroomBlock, TextureMapping.defaultTexture(mushroomBlock), this.modelOutput);
			ResourceLocation resourcelocation1 = ModelTemplates.SINGLE_FACE.createWithSuffix(mushroomBlock, "_inside", TextureMapping.defaultTexture(TextureMapping.getBlockTexture(mushroomBlock, "_inner")), this.modelOutput);
			this.blockStateOutput.accept(MultiPartGenerator.multiPart(mushroomBlock).with(Condition.condition().term(BlockStateProperties.NORTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation)).with(Condition.condition().term(BlockStateProperties.EAST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90).with(VariantProperties.UV_LOCK, true)).with(Condition.condition().term(BlockStateProperties.SOUTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R180).with(VariantProperties.UV_LOCK, true)).with(Condition.condition().term(BlockStateProperties.WEST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R270).with(VariantProperties.UV_LOCK, true)).with(Condition.condition().term(BlockStateProperties.UP, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation).with(VariantProperties.X_ROT, VariantProperties.Rotation.R270).with(VariantProperties.UV_LOCK, true)).with(Condition.condition().term(BlockStateProperties.DOWN, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation).with(VariantProperties.X_ROT, VariantProperties.Rotation.R90).with(VariantProperties.UV_LOCK, true)).with(Condition.condition().term(BlockStateProperties.NORTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation1)).with(Condition.condition().term(BlockStateProperties.EAST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90).with(VariantProperties.UV_LOCK, false)).with(Condition.condition().term(BlockStateProperties.SOUTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R180).with(VariantProperties.UV_LOCK, false)).with(Condition.condition().term(BlockStateProperties.WEST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R270).with(VariantProperties.UV_LOCK, false)).with(Condition.condition().term(BlockStateProperties.UP, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.X_ROT, VariantProperties.Rotation.R270).with(VariantProperties.UV_LOCK, false)).with(Condition.condition().term(BlockStateProperties.DOWN, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.X_ROT, VariantProperties.Rotation.R90).with(VariantProperties.UV_LOCK, false)));
			this.registerSimpleItemModel(mushroomBlock, TexturedModel.CUBE.createWithSuffix(mushroomBlock, "_inventory", this.modelOutput));
		}

		@Override
		public void createPointedDripstone()
		{
			Block block = pointed_bergstone;
			PropertyDispatch.C3<Direction, DripstoneThickness, Boolean> c2 = PropertyDispatch.properties(BlockStateProperties.VERTICAL_DIRECTION, BlockStateProperties.DRIPSTONE_THICKNESS, BlockStateProperties.SNOWY);

			for (DripstoneThickness thickness : DripstoneThickness.values())
			{
				c2.select(Direction.UP, thickness, false, this.createPointedBergstoneVariant(Direction.UP, thickness));

				c2.select(Direction.UP, thickness, true, this.createPointedBergstoneVariant(Direction.UP, thickness, true));

				c2.select(Direction.DOWN, thickness, false, this.createPointedBergstoneVariant(Direction.DOWN, thickness));

				c2.select(Direction.DOWN, thickness, true, this.createPointedBergstoneVariant(Direction.DOWN, thickness, true));
			}

			/*c2.generate((dir, thickness, snowy) ->
			{
				return null;
			});*/

			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(block).with(c2));

			this.registerSimpleItemModel(block.asItem(), createFlatItemModelWithBlockTexture(block.asItem(), block, "_up_tip"));
		}

		public Variant createPointedBergstoneVariant(Direction direction, DripstoneThickness dripstoneThickness)
		{
			return this.createPointedBergstoneVariant(direction, dripstoneThickness, false);
		}

		public Variant createPointedBergstoneVariant(Direction direction, DripstoneThickness dripstoneThickness, boolean showSnow)
		{
			String s = "_" + direction.getSerializedName() + "_" + dripstoneThickness.getSerializedName();

			String snowyName = s + "_snowy";
			boolean isBase = dripstoneThickness == DripstoneThickness.BASE;

			if (showSnow && isBase)
				s = snowyName;

			TextureMapping texturemapping = TextureMapping.cross(TextureMapping.getBlockTexture(pointed_bergstone, s));

			if (showSnow && !isBase)
				s = snowyName;

			return Variant.variant().with(VariantProperties.MODEL, extendWithType("cutout", ModelTemplates.POINTED_DRIPSTONE).createWithSuffix(pointed_bergstone, s, texturemapping, this.modelOutput));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createPlant(Block flower, Block pot, BlockModelGenerators.PlantType type)
		{
			this.createCrossBlock(flower, type);
			TextureMapping texturemapping = type.getPlantTextureMapping(flower);
			ResourceLocation resourcelocation = type.getCrossPot().extend().renderType(ResourceLocation.parse("cutout")).build().create(pot, texturemapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(pot, resourcelocation));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createDoublePlant(Block block, BlockModelGenerators.PlantType plantType)
		{
			ResourceLocation resourcelocation = this.createSuffixedVariant(block, "_top", extendWithType("cutout", plantType.getCross()), TextureMapping::cross);
			ResourceLocation resourcelocation1 = this.createSuffixedVariant(block, "_bottom", extendWithType("cutout", plantType.getCross()), TextureMapping::cross);
			this.createDoubleBlock(block, resourcelocation, resourcelocation1);
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createCrossBlock(Block flower, BlockModelGenerators.PlantType type, TextureMapping mapping)
		{
			ResourceLocation resourcelocation = type.getCross().extend().renderType(ResourceLocation.parse("cutout")).build().create(flower, mapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(flower, resourcelocation));
		}

		public void ladder(Block block)
		{
			ResourceLocation modelName = ModelTemplates.create("ladder", TextureSlot.TEXTURE, TextureSlot.PARTICLE).extend().renderType("cutout").build().create(block, new TextureMapping().put(TextureSlot.TEXTURE, TextureMapping.getBlockTexture(block)).put(TextureSlot.PARTICLE, TextureMapping.getBlockTexture(block)), modelOutput);
			MultiVariantGenerator gen = MultiVariantGenerator.multiVariant(block, Variant.variant().with(VariantProperties.MODEL, modelName));
			this.blockStateOutput.accept(gen);

			this.registerSimpleFlatItemModel(block);
		}

		public void createTexturedFarmland(Block dirt, Block farmland)
		{
			TextureMapping texturemapping = new TextureMapping().put(TextureSlot.DIRT, TextureMapping.getBlockTexture(dirt)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(farmland));
			TextureMapping texturemapping1 = new TextureMapping().put(TextureSlot.DIRT, TextureMapping.getBlockTexture(dirt)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(farmland, "_moist"));
			ResourceLocation resourcelocation = ModelTemplates.FARMLAND.create(farmland, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = ModelTemplates.FARMLAND.create(TextureMapping.getBlockTexture(farmland, "_moist"), texturemapping1, this.modelOutput);
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(farmland).with(createEmptyOrFullDispatch(BlockStateProperties.MOISTURE, 7, resourcelocation1, resourcelocation)));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createDoor(Block door)
		{
			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.door(door);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation5 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation6 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation7 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			this.registerSimpleFlatItemModel(door.asItem());
			this.blockStateOutput.accept(createDoor(door, resourcelocation, resourcelocation1, resourcelocation2, resourcelocation3, resourcelocation4, resourcelocation5, resourcelocation6, resourcelocation7));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createGlassBlocks(Block block, Block pane)
		{
			this.createTrivialCube(block);

			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.pane(block, pane);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_POST).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_SIDE).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_SIDE_ALT).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_NOSIDE).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_NOSIDE_ALT).create(pane, texturemapping, this.modelOutput);
			Item item = pane.asItem();
			this.registerSimpleItemModel(item, this.createFlatItemModelWithBlockTexture(item, block));
			this.blockStateOutput.accept(MultiPartGenerator.multiPart(pane).with(Variant.variant().with(VariantProperties.MODEL, resourcelocation)).with(Condition.condition().term(BlockStateProperties.NORTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation1)).with(Condition.condition().term(BlockStateProperties.EAST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.SOUTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation2)).with(Condition.condition().term(BlockStateProperties.WEST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation2).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.NORTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation3)).with(Condition.condition().term(BlockStateProperties.EAST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation4)).with(Condition.condition().term(BlockStateProperties.SOUTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation4).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.WEST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation3).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R270)));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public List<ResourceLocation> createFloorFireModels(Block block)
		{
			String renderType = "translucent";
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.FIRE_FLOOR).create(ModelLocationUtils.getModelLocation(block, "_floor0"), TextureMapping.fire0(block), this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.FIRE_FLOOR).create(ModelLocationUtils.getModelLocation(block, "_floor1"), TextureMapping.fire1(block), this.modelOutput);
			return ImmutableList.of(resourcelocation, resourcelocation1);
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public List<ResourceLocation> createSideFireModels(Block p_387079_)
		{
			String renderType = "translucent";

			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.FIRE_SIDE).create(ModelLocationUtils.getModelLocation(p_387079_, "_side0"), TextureMapping.fire0(p_387079_), this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.FIRE_SIDE).create(ModelLocationUtils.getModelLocation(p_387079_, "_side1"), TextureMapping.fire1(p_387079_), this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.FIRE_SIDE_ALT).create(ModelLocationUtils.getModelLocation(p_387079_, "_side_alt0"), TextureMapping.fire0(p_387079_), this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.FIRE_SIDE_ALT).create(ModelLocationUtils.getModelLocation(p_387079_, "_side_alt1"), TextureMapping.fire1(p_387079_), this.modelOutput);
			return ImmutableList.of(resourcelocation, resourcelocation1, resourcelocation2, resourcelocation3);
		}

		/**
		 * Copy of super, but with added render type arg and item model gen
		 */
		@Override
		public void createAmethystCluster(Block amethystBlock)
		{
			this.registerSimpleItemModel(amethystBlock, this.createFlatItemModelWithBlockTexture(amethystBlock.asItem(), amethystBlock));
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(amethystBlock, Variant.variant().with(VariantProperties.MODEL, extendWithType("cutout", ModelTemplates.CROSS).create(amethystBlock, TextureMapping.cross(amethystBlock), this.modelOutput))).with(this.createColumnWithFacing()));
		}

		private static ModelTemplate extendWithType(String renderType, ModelTemplate template)
		{
			return template.extend().renderType(ResourceLocation.parse(renderType)).build();
		}

		public void simpleBlock(Block block)
		{
			this.simpleBlock(block, (String) null);
		}

		public void simpleBlock(Block block, @Nullable String renderType)
		{
			this.createTrivialBlock(block, renderType != null ? TexturedModel.createDefault(TextureMapping::cube, ModelTemplates.CUBE_ALL.extend().renderType(ResourceLocation.parse(renderType)).build()) : TexturedModel.CUBE);
		}

		public static BlockStateGenerator createTranslucentCubeGenerator(Block cubeBlock, ResourceLocation location, TextureMapping textureMapping, BiConsumer<ResourceLocation, ModelInstance> modelOutput)
		{
			ResourceLocation resourcelocation = ModelTemplates.CUBE_ALL.extend().renderType(ResourceLocation.parse("translucent")).build().create(cubeBlock, textureMapping, modelOutput);
			return createSimpleBlock(cubeBlock, resourcelocation);
		}

		public void createGrowthBlock(Block mossBlock, Block bottom)
		{
			TextureMapping texturemapping = new TextureMapping().put(TextureSlot.BOTTOM, TextureMapping.getBlockTexture(bottom)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(mossBlock)).put(TextureSlot.SIDE, TextureMapping.getBlockTexture(mossBlock, "_side"));
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(mossBlock, createRotatedVariants(ModelTemplates.CUBE_BOTTOM_TOP.create(mossBlock, texturemapping, this.modelOutput))));
		}
	}
}
