package com.legacy.pagamos.data;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import com.legacy.pagamos.Pagamos;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.DetectedVersion;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.data.event.GatherDataEvent;

@EventBusSubscriber(modid = Pagamos.MODID, bus = Bus.MOD)
public class PagamosDataGen
{
	@SubscribeEvent
	public static void gatherData(GatherDataEvent.Client event)
	{
		DataGenerator gen = event.getGenerator();
		PackOutput output = gen.getPackOutput();
		boolean run = true;

		/*PagamosFeatures.Configured.init();
		PagamosFeatures.Placements.init();*/

		DatapackBuiltinEntriesProvider provider = new DatapackBuiltinEntriesProvider(output, event.getLookupProvider(), RegistrarHandler.injectRegistries(new RegistrySetBuilder()), Set.of(Pagamos.MODID));

		var lookup = provider.getRegistryProvider();
		gen.addProvider(run, provider);

		BlockTagsProvider blockTagProv = new PagamosTagProv.BlockTagProv(gen, lookup);
		gen.addProvider(run, blockTagProv);
		gen.addProvider(run, new PagamosTagProv.ItemTagProv(gen, blockTagProv.contentsGetter(), lookup));
		gen.addProvider(run, new PagamosTagProv.BiomeTagProv(gen, lookup));

		gen.addProvider(run, new PagamosRecipeProv.Runner(output, lookup));
		
		/*gen.addProvider(run, new PagamosAdvancementProv(gen, event.getLookupProvider()));*/
		gen.addProvider(run, new PagamosLootProv(gen, event.getLookupProvider()));

		/*gen.addProvider(run, new PagamosDataMapProv(output, lookup));*/

		gen.addProvider(run, new PagamosModelProv(output));
		gen.addProvider(run, new PagamosModelProv.Equipment(output));

		gen.addProvider(run, new PagamosLangProv(output, lookup));
		gen.addProvider(run, new PagamosSoundProv(output));

		gen.addProvider(run, new PagamosParticleProv(output));

		gen.addProvider(run, packMcmeta(output, "Pagamos resources"));

		PackOutput legacyPackOutput = gen.getPackOutput(String.format("assets/%s/legacy_pack", Pagamos.MODID));
		gen.addProvider(run, packMcmeta(legacyPackOutput, "Pagamos' classic textures from pre-1.14"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}

	private record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
	{

		public static <D extends DataProvider> NestedDataProvider<D> of(D provider, String namePrefix)
		{
			return new NestedDataProvider<>(provider, namePrefix);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cachedOutput)
		{
			return this.provider.run(cachedOutput);
		}

		@Override
		public String getName()
		{
			return this.namePrefix + "/" + this.provider.getName();
		}
	}
}
