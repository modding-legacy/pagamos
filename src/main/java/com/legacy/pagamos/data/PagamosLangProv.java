package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosBlocks.*;
import static com.legacy.pagamos.registry.PagamosSounds.*;
import static com.legacy.pagamos.registry.PagamosItems.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.legacy.pagamos.Pagamos;

import net.minecraft.Util;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.client.KeyMapping;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.LanguageProvider;
import net.neoforged.neoforge.common.util.Lazy;

public class PagamosLangProv extends LanguageProvider
{
	private final CompletableFuture<HolderLookup.Provider> lookup;

	public PagamosLangProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(packOutput, Pagamos.MODID, "en_us");
		this.lookup = lookup;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void addTranslations()
	{
		Set<Block> ignoredBlocks = new HashSet<>();

		Map<Block, String> blockOverrides = Util.make(new HashMap<>(), m ->
		{
			m.put(tourmaline_block, "Block of Tourmaline");
			m.put(sapphire_block, "Block of Sapphire");

			for (var ignored : ignoredBlocks)
				m.put(ignored, "");
		});

		this.addDefault(Registries.BLOCK, blockOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));

		// this.addDefault(Registries.STRUCTURE, Map.of());

		Set<Item> ignoredItems = new HashSet<>();

		Map<Item, String> itemOverrides = Util.make(new HashMap<>(), m ->
		{
			m.put(flint_and_lapis, "Flint and Lapis");

			for (var ignored : ignoredItems)
				m.put(ignored, "");
		});

		this.addDefault(Registries.ITEM, itemOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));

		Map<EntityType<?>, String> entityOverrides = Util.make(new HashMap<>(), m ->
		{
		});

		this.addDefault(Registries.ENTITY_TYPE, entityOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));
		this.addDefault(Registries.BIOME, Map.of());
		this.addDefault(Registries.CHUNK_GENERATOR, Map.of());
		// this.addDefault(Registries.ENCHANTMENT, Map.of());

		/*this.addDefault(Registries.MOB_EFFECT, Map.of());
		this.addDefault(Registries.TRIM_PATTERN, Map.of());
		this.addDefault(Registries.TRIM_MATERIAL, Map.of());*/

		this.add("advancements.pagamos.root.title", "Pagamos");
		/*this.addAdvancement(PagamosAdvancementProv.root, "Pagamos", "A frozen wasteland.");*/

		this.add(ENTITY_CRYSTAL_PHOENIX_IDLE, "Crystal Phoenix screeches");
		this.add(ENTITY_CRYSTAL_PHOENIX_HURT, "Crystal Phoenix hurts");
		this.add(ENTITY_CRYSTAL_PHOENIX_DEATH, "Crystal Phoenix dies");
		this.add(ENTITY_CRYSTAL_PHOENIX_ATTACK, "Crystal Phoenix shoots");

		this.add(ENTITY_ICE_CREEPER_HURT, "Ice Creeper hurts");
		this.add(ENTITY_ICE_CREEPER_DEATH, "Ice Creeper dies");
		this.add(ENTITY_ICE_CREEPER_PRIMED, "Ice Creeper hisses");

		this.add(BLOCK_PORTAL_LIGHT, "Pagamos Portal bursts");
		this.add(BLOCK_PORTAL_DESTROYED, "Pagamos Portal fades");

		this.add(BLOCK_PORTAL_AMBIENT, "Pagamos Portal blows");
		this.add(BLOCK_PORTAL_TRIGGER, "Pagamos Portal rumbles");
		this.add(BLOCK_PORTAL_TRAVEL, "Pagamos Portal rumbles aggressively");

		/*this.add(Pagamos.MODID + ".configuration.section.pagamos.server.toml.title", "Pagamos World Configuration");
		this.add(Pagamos.MODID + ".configuration.section.pagamos.server.toml", "World Settings");*/
	}

	public void serverConf(String key, String title, String desc)
	{
		String serverConf = Pagamos.MODID + ".server.";
		this.add(serverConf.concat(key), title);
		this.add(serverConf.concat(key + ".tooltip"), desc);
	}

	public void addKeybind(Lazy<KeyMapping> key, String translation)
	{
		this.add(key.get().getName(), translation);
	}

	public void addPainting(ResourceKey<PaintingVariant> painting, String title, String author)
	{
		ResourceLocation key = painting.location();
		String s = "painting." + key.getNamespace() + "." + key.getPath() + ".";
		this.add(s + "title", title);
		this.add(s + "author", author);
	}

	public void addAttribute(Supplier<Attribute> attribute, String name)
	{
		this.add(attribute.get().getDescriptionId(), name);
	}

	public void addPotion(Supplier<MobEffect> potion)
	{
		ResourceLocation key = BuiltInRegistries.MOB_EFFECT.getKey(potion.get());
		String path = key.getPath();
		String name = this.toName(path);
		this.add("item.minecraft.potion.effect." + path, "Potion of " + name);
		this.add("item.minecraft.splash_potion.effect." + path, "Splash Potion of " + name);
		this.add("item.minecraft.lingering_potion.effect." + path, "Lingering Potion of " + name);
		this.add("item.minecraft.tipped_arrow.effect." + path, "Arrow of " + name);
	}

	private void addDamageType(ResourceKey<DamageType> damageType, String deathMessage, @Nullable String playerKillMessage)
	{
		try
		{
			String messageID = "death.attack." + this.lookup.get().lookupOrThrow(Registries.DAMAGE_TYPE).getOrThrow(damageType).value().msgId();
			this.add(messageID, deathMessage);

			if (playerKillMessage != null && !playerKillMessage.isBlank())
				this.add(messageID + ".player", playerKillMessage);
		}
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
	}

	private void addAdvancement(AdvancementHolder advancement, String title, String desc)
	{
		advancement.value().display().ifPresent(display ->
		{
			this.add(display.getTitle().getString(), title);
			this.add(display.getDescription().getString(), desc);
		});
	}

	private <T> void addDefault(ResourceKey<Registry<T>> registry, Map<ResourceLocation, String> overrides)
	{
		this.addDefault_(registry, overrides.entrySet().stream().collect(Collectors.toMap(e -> ResourceKey.create(registry, e.getKey()), e -> e.getValue())));
	}

	private <T> void addDefault_(ResourceKey<Registry<T>> registry, Map<ResourceKey<T>, String> overrides)
	{
		try
		{
			this.lookup.get().lookupOrThrow(registry).listElementIds().distinct().filter(key -> Pagamos.MODID.equals(key.location().getNamespace())).filter(key ->
			{
				return !overrides.containsKey(key) && !(registry.location().equals(Registries.ITEM.location()) && BuiltInRegistries.ITEM.getValue(key.location()) instanceof BlockItem bi && (bi.getDescriptionId().startsWith("block")));
			}).forEach(this::add);
		}
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
		overrides.forEach(this::add);
	}

	private void add(ResourceKey<?> key)
	{
		// temp brick thing, remove when we actually remap that stuff
		this.add(key, this.toName(key).replace("Stonebrick ", "Stone Brick ").replace("Stonebrick", "Stone Bricks"));
	}

	private void add(ResourceKey<?> key, String translation)
	{
		this.add(this.makeDescriptionID(key), translation);
	}

	private void add(Component key, String translation)
	{
		if (key.getContents() instanceof TranslatableContents trans)
			this.add(trans.getKey(), translation);
		else
			LOGGER.error("Tried to add non translatable component to lang prov. Contents: {}", translation);
	}

	private void add(Supplier<SoundEvent> sound, String translation)
	{
		this.add(sound.get(), translation);
	}

	private void add(Holder<SoundEvent> sound, String translation)
	{
		this.add(sound.value(), translation);
	}

	private void add(SoundEvent sound, String translation)
	{
		this.add("subtitles." + Pagamos.MODID + "." + sound.location().getPath(), translation);
	}

	private void addItemInfo(Supplier<Item> item, String key, String translation)
	{
		var resourceKey = BuiltInRegistries.ITEM.getResourceKey(item.get()).get();
		ResourceLocation location = resourceKey.location();
		this.add(Util.makeDescriptionId(resourceKey.registry().getPath().replace('/', '.'), ResourceLocation.fromNamespaceAndPath(location.getNamespace(), location.getPath() + "." + key)), translation);
	}

	/**
	 * Adds support for the Music Manager mod.
	 * 
	 * https://www.curseforge.com/minecraft/mc-mods/music-manager
	 */
	public void addMusicManagerPhrases(String author, SoundEvent... sounds)
	{
		for (SoundEvent sound : sounds)
		{
			ResourceLocation loc = sound.location();
			String name = loc.getPath().replace("music.", "");
			String mm = "sounds.musicmanager.MODID.music.".replace("MODID", loc.getNamespace());
			this.add(mm + name, author + " - " + this.toName(name));
		}
	}

	// Converts camel case to a proper name. snowy_temple -> Snowy Temple
	private String toName(ResourceKey<?> key)
	{
		String suffix;
		if (key.registry().equals(Registries.TRIM_MATERIAL.location()))
			suffix = " Material";
		else if (key.registry().equals(Registries.TRIM_PATTERN.location()))
			suffix = " Armor Trim";
		else
			suffix = "";

		return this.toName(key.location().getPath()) + suffix;
	}

	private String toName(String key)
	{
		String[] words = key.split("_");
		for (int i = words.length - 1; i > -1; i--)
		{
			if (!words[i].equals("of")) // don't capitalize "of"
				words[i] = words[i].substring(0, 1).toUpperCase(Locale.ENGLISH) + words[i].substring(1).toLowerCase(Locale.ENGLISH);
		}
		return String.join(" ", words);
	}

	private String makeDescriptionID(ResourceKey<?> resourceKey)
	{
		String registryPath = resourceKey.registry().getPath();
		if (registryPath.equals("custom_stat"))
			registryPath = "stat";
		return Util.makeDescriptionId(registryPath.replace('/', '.').replace("worldgen.", ""), resourceKey.location()).replace("entity_type", "entity").replace("mob_effect", "effect");
	}

	private Set<String> existing = new HashSet<>();

	public void add(String key, String value)
	{
		if (!value.isBlank() && existing.add(key))
			super.add(key, value);
	}

	public void add(String key, Lazy<VillagerProfession> profession, String value)
	{
		ResourceLocation profLoc = BuiltInRegistries.VILLAGER_PROFESSION.getKey(profession.get());
		this.add(key + "." + profLoc.getNamespace() + "." + profLoc.getPath(), value);
	}
}
