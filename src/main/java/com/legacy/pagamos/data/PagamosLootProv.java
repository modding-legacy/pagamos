package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosBlocks.*;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosEntityTypes;
import com.legacy.pagamos.registry.PagamosItems;

import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.WritableRegistry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.LootTableSubProvider;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.resources.ResourceKey;
import net.minecraft.util.ProblemReporter;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.Portal;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.NestedLootTable;
import net.minecraft.world.level.storage.loot.functions.EnchantRandomlyFunction;
import net.minecraft.world.level.storage.loot.functions.EnchantWithLevelsFunction;
import net.minecraft.world.level.storage.loot.functions.EnchantedCountIncreaseFunction;
import net.minecraft.world.level.storage.loot.functions.LootItemConditionalFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemDamageFunction;
import net.minecraft.world.level.storage.loot.functions.SmeltItemFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.NumberProvider;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class PagamosLootProv extends LootTableProvider
{

	public PagamosLootProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(ChestGen::new, LootContextParamSets.CHEST), new LootTableProvider.SubProviderEntry(BlockGen::new, LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(EntityGen::new, LootContextParamSets.ENTITY)), lookup);
	}

	@Override
	protected void validate(WritableRegistry<LootTable> writableregistry, ValidationContext validationcontext, ProblemReporter.Collector problemreporter$collector)
	{
		/*writableregistry.listElements().forEach(lootTable -> lootTable.value().validate(validationcontext.setContextKeySet(lootTable.value().getParamSet()).enterElement("{" + lootTable.key().location() + "}", lootTable.key())));*/
	}

	private static class BlockGen extends BlockLootSubProvider
	{
		protected BlockGen(HolderLookup.Provider lookup)
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags(), lookup);
		}

		@Override
		protected void generate()
		{
			blocks().forEach(block ->
			{
				if (block == snowed_tourmaline_ore || block == bergstone_tourmaline_ore)
					add(block, this.createOreDrop(block, PagamosItems.tourmaline));
				else if (block == snowed_sapphire_ore || block == bergstone_sapphire_ore)
					add(block, this.createOreDrop(block, PagamosItems.sapphire));
				else if (block == bergstone)
					silkOrElse(block, cobbled_bergstone);
				else if (block == azure_crystal)
					silkOrElse(block, PagamosItems.azure_crystal_shard, ConstantValue.exactly(4));
				else if (block == mauve_crystal)
					silkOrElse(block, PagamosItems.mauve_crystal_shard, ConstantValue.exactly(4));
				else if (block == crystal_ice)
					silkOrElse(block, PagamosItems.ice_shard, UniformGenerator.between(0, 4));
				else if (block == titian_growth)
					silkOrElse(block, cobbled_bergstone);
				else if (block == short_titianeweed)
					this.add(block, this.createShearsOnlyDrop(block));
				else if (block == tall_titianeweed)
					this.add(block, this.createShearsOnlyDrop(block));
				else if (block instanceof FlowerPotBlock)
					dropPottedContents(block);
				else
					dropSelf(block);
			});
		}

		private void silkOrElse(Block withSilk, ItemLike without)
		{
			this.add(withSilk, (b) -> createSingleItemTableWithSilkTouch(b, without));
		}

		private void silkOrElse(Block withSilk, ItemLike without, NumberProvider count)
		{
			this.add(withSilk, (b) -> createSingleItemTableWithSilkTouch(b, without, count));
		}

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(Pagamos.MODID) && (b.asItem() != Blocks.AIR.asItem() || b instanceof FlowerPotBlock) && !(b instanceof Portal));
		}
	}

	private static class EntityGen extends VanillaEntityLoot implements LootPoolUtil
	{
		public EntityGen(HolderLookup.Provider lookup)
		{
			super(lookup);
		}

		@Override
		public void generate()
		{
			LootPool.Builder rawFish = lootingPool(Items.COD, 0, 2, 1, 1);
			this.add(PagamosEntityTypes.YETI, tableOf(rawFish));

			LootPool.Builder phoenixFeather = lootingPool(PagamosItems.phoenix_feather, 1, 2, 1, 1);
			LootPool.Builder azureShard = lootingPool(PagamosItems.azure_crystal_shard, 1, 3, 0, 3);
			LootPool.Builder mauveShard = lootingPool(PagamosItems.mauve_crystal_shard, 1, 3, 0, 3);

			this.add(PagamosEntityTypes.CRYSTAL_PHOENIX, tableOf(List.of(phoenixFeather, azureShard, mauveShard)));

			var creeperTable = poolOf(List.of(NestedLootTable.lootTableReference(EntityType.CREEPER.getDefaultLootTable().get())));
			LootPool.Builder iceShard = lootingPool(PagamosItems.ice_shard, 1, 2, 0, 1);

			this.add(PagamosEntityTypes.ICE_CREEPER, tableOf(List.of(creeperTable, iceShard)));
		}

		private LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting)
		{
			return basicPool(item, min, max).apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(minLooting, maxLooting)));
		}

		/*private String entityName(EntityType<?> entity)
		{
			return ForgeRegistries.ENTITY_TYPES.getKey(entity).getPath();
		}*/

		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return BuiltInRegistries.ENTITY_TYPE.stream().filter(e -> BuiltInRegistries.ENTITY_TYPE.getKey(e).getNamespace().contains(Pagamos.MODID));
		}
	}

	public static record ChestGen(HolderLookup.Provider registries) implements LootPoolUtil, LootTableSubProvider
	{
		@Override
		public void generate(BiConsumer<ResourceKey<LootTable>, LootTable.Builder> consumer)
		{
		}
	}

	/**
	 * Interface with basic loot table generators
	 * 
	 * @author David
	 *
	 */
	public interface LootPoolUtil
	{
		/**
		 * Creates a table from the given loot pools.
		 * 
		 * @param pools
		 * @return
		 */
		default LootTable.Builder tableOf(List<LootPool.Builder> pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			pools.forEach(pool -> table.withPool(pool));
			return table;
		}

		/**
		 * Creates a table from the given loot pool.
		 * 
		 * @param pool
		 * @return
		 */
		default LootTable.Builder tableOf(LootPool.Builder pool)
		{
			return LootTable.lootTable().withPool(pool);
		}

		/**
		 * Creates a loot pool with the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item, int min, int max)
		{
			return LootPool.lootPool().add(basicEntry(item, min, max));
		}

		/**
		 * Creates a loot pool with the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item)
		{
			return LootPool.lootPool().add(basicEntry(item));
		}

		/**
		 * Creates a loot pool that will give a random item from the list.
		 * 
		 * @param items
		 * @return
		 */
		default LootPool.Builder randItemPool(List<ItemLike> items)
		{
			return poolOf(items.stream().map((i) -> basicEntry(i)).collect(Collectors.toList()));
		}

		/**
		 * Creates a loot pool with multiple entries. One of these entries will be
		 * picked at random each time the pool rolls.
		 * 
		 * @param lootEntries
		 * @return
		 */
		default LootPool.Builder poolOf(List<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			lootEntries.forEach(entry -> pool.add(entry));
			return pool;
		}

		/**
		 * Creates a loot entry for the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item, int min, int max)
		{
			return basicEntry(item).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)));
		}

		/**
		 * Creates a loot entry for the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}

		/**
		 * Sets the damage of the item (percentage)
		 * 
		 * @param min 0 - 100
		 * @param max 0 - 100
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> setDamage(int min, int max)
		{
			return SetItemDamageFunction.setDamage(UniformGenerator.between(min / 100F, max / 100F));
		}

		/**
		 * Cooks the item if the predicate passes
		 * 
		 * @param predicate
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> smeltItem(EntityPredicate.Builder predicate)
		{
			return SmeltItemFunction.smelted().when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.THIS, predicate));
		}

		/**
		 * Enchants the item randomly between the levels provided
		 * 
		 * @param minLevel
		 * @param maxLevel
		 * @return
		 */
		default LootItemConditionalFunction.Builder<?> enchant(HolderLookup.Provider registries, int minLevel, int maxLevel)
		{
			return EnchantWithLevelsFunction.enchantWithLevels(registries, UniformGenerator.between(minLevel, maxLevel));
		}

		/**
		 * Enchants the item randomly with the enchantments passed
		 * 
		 * @param enchantments
		 * @return
		 */
		@SuppressWarnings("unchecked")
		default LootItemConditionalFunction.Builder<?> enchant(Holder<Enchantment>... enchantments)
		{
			EnchantRandomlyFunction.Builder func = new EnchantRandomlyFunction.Builder();
			for (Holder<Enchantment> enchantment : enchantments)
				func.withEnchantment(enchantment);
			return func;
		}
	}
}
