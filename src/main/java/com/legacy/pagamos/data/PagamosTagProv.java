package com.legacy.pagamos.data;

import static com.legacy.pagamos.registry.PagamosBlocks.*;
import static com.legacy.pagamos.registry.PagamosItems.*;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.registry.PagamosBiomes;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.WallBlock;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;

@SuppressWarnings("unchecked")
public class PagamosTagProv
{
	public static class BlockTagProv extends BlockTagsProvider
	{
		public BlockTagProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, Pagamos.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			mod(prov);
			vanilla(prov);
			forge(prov);
		}

		void mod(Provider prov)
		{
			this.tag(PagamosTags.Blocks.BLUE_FIRE_INFINIBURN).add(bergstone, bergstone_slab, bergstone_stairs, cobbled_bergstone, cobbled_bergstone_slab, cobbled_bergstone_stairs, mistshale, mistshale_slab, mistshale_stairs, titian_growth);

			this.tag(PagamosTags.Blocks.DRIPSTONES_CAN_GENERATE_ON).addTag(BlockTags.SNOW).add(bergstone, cobbled_bergstone);

			this.tag(PagamosTags.Blocks.CRYSTAL_CAN_GENERATE_ON).addTags(Tags.Blocks.STONES, Tags.Blocks.COBBLESTONES, BlockTags.SNOW);
			this.tag(PagamosTags.Blocks.CRYSTAL_CAN_REPLACE).addTags(BlockTags.ICE, BlockTags.REPLACEABLE);

			this.tag(PagamosTags.Blocks.PAGAMOS_PORTAL_FRAME).add(Blocks.SNOW_BLOCK, packed_snow);

			this.tag(PagamosTags.Blocks.TOURMALINE_ORES).add(snowed_tourmaline_ore, bergstone_tourmaline_ore);
			this.tag(PagamosTags.Blocks.SAPPHIRE_ORES).add(snowed_sapphire_ore, bergstone_tourmaline_ore);

			this.tag(PagamosTags.Blocks.STORAGE_BLOCKS_TOURMALINE).add(tourmaline_block);
			this.tag(PagamosTags.Blocks.STORAGE_BLOCKS_SAPPHIRE).add(sapphire_block);

			this.tag(PagamosTags.Blocks.CRYSTAL_BLOCKS).add(azure_crystal_block, mauve_crystal_block, budding_azure_crystal_block, budding_mauve_crystal_block);
			this.tag(PagamosTags.Blocks.CRYSTAL_CLUSTERS).add(azure_crystal, mauve_crystal);
			this.tag(PagamosTags.Blocks.CRYSTAL_BUDS);

			this.tag(PagamosTags.Blocks.CAN_SUSTAIN_TITIAN_PLANT).add(titian_growth).addTags(BlockTags.DIRT, BlockTags.NYLIUM);
		}

		void vanilla(Provider prov)
		{
			addMatching(block -> block instanceof SlabBlock, BlockTags.SLABS);
			addMatching(block -> block instanceof StairBlock, BlockTags.STAIRS);
			addMatching(block -> block instanceof WallBlock, BlockTags.WALLS);

			/*this.tag(BlockTags.STONE_BUTTONS).add(bergstone_button);
			this.tag(BlockTags.STONE_PRESSURE_PLATES).add(bergstone_pressure_plate);*/

			this.tag(BlockTags.BEACON_BASE_BLOCKS).addTags(PagamosTags.Blocks.STORAGE_BLOCKS_TOURMALINE, PagamosTags.Blocks.STORAGE_BLOCKS_SAPPHIRE);

			this.tag(BlockTags.FIRE).add(blue_fire);
			this.tag(BlockTags.SNOW).add(packed_snow);
			this.tag(BlockTags.MOSS_REPLACEABLE).add(bergstone);
			this.tag(BlockTags.SCULK_REPLACEABLE).add(bergstone);
			this.tag(BlockTags.ICE).add(crystal_ice);

			// TODO
			this.tag(BlockTags.NETHER_CARVER_REPLACEABLES).add(bergstone, packed_snow, mistshale, titian_growth);

			this.tag(BlockTags.STONE_BRICKS).add(bergstone_bricks);
			this.tag(BlockTags.MUSHROOM_GROW_BLOCK).add(titian_growth);

			/**
			 * Go to PagamosEvents.onPlayerRightClickBlock to enable debug output for blocks
			 * without tools
			 */
			/*this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(bergstone_button, bergstone_pressure_plate);*/
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(bergstone, bergstone_slab, bergstone_stairs, bergstone_wall, bergstone_bricks, bergstone_brick_slab, bergstone_brick_stairs, bergstone_brick_wall, cobbled_bergstone, cobbled_bergstone_slab, cobbled_bergstone_stairs, cobbled_bergstone_wall, polished_bergstone, chiseled_bergstone);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(mistshale, mistshale_slab, mistshale_stairs, mistshale_wall, mistshale_bricks, mistshale_brick_slab, mistshale_brick_stairs, mistshale_brick_wall, polished_mistshale, polished_mistshale_slab, polished_mistshale_stairs, polished_mistshale_wall, mistshale_tiles, mistshale_tile_slab, mistshale_tile_stairs, mistshale_tile_wall);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(igloo_bricks, igloo_brick_slab, igloo_brick_stairs, igloo_brick_wall);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(crystal_plated_tiles, crystal_plated_tile_slab, crystal_plated_tile_stairs, crystal_plated_tile_wall);

			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(bergstone_tourmaline_ore, bergstone_sapphire_ore);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(tourmaline_block, sapphire_block);

			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(freezer, crystal_ice, creeper_ice);

			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).addTags(PagamosTags.Blocks.CRYSTAL_BLOCKS, PagamosTags.Blocks.CRYSTAL_CLUSTERS, PagamosTags.Blocks.CRYSTAL_BUDS);

			this.tag(BlockTags.MINEABLE_WITH_AXE);

			this.tag(BlockTags.MINEABLE_WITH_SHOVEL).add(packed_snow, snowed_tourmaline_ore, snowed_sapphire_ore);

			this.tag(BlockTags.MINEABLE_WITH_HOE);

			this.tag(BlockTags.NEEDS_DIAMOND_TOOL).addTags(PagamosTags.Blocks.STORAGE_BLOCKS_TOURMALINE, PagamosTags.Blocks.TOURMALINE_ORES, PagamosTags.Blocks.STORAGE_BLOCKS_SAPPHIRE, PagamosTags.Blocks.SAPPHIRE_ORES);
		}

		void forge(Provider prov)
		{
			this.tag(Tags.Blocks.COBBLESTONES).add(cobbled_bergstone);
			this.tag(Tags.Blocks.STONES).add(bergstone, mistshale);
			this.tag(Tags.Blocks.ORES).addTags(PagamosTags.Blocks.TOURMALINE_ORES, PagamosTags.Blocks.SAPPHIRE_ORES);
			this.tag(Tags.Blocks.STORAGE_BLOCKS).addTags(PagamosTags.Blocks.STORAGE_BLOCKS_TOURMALINE, PagamosTags.Blocks.STORAGE_BLOCKS_SAPPHIRE);

			this.tag(Tags.Blocks.BUDDING_BLOCKS).add(budding_azure_crystal_block, budding_mauve_crystal_block);
			this.tag(Tags.Blocks.BUDS).addTag(PagamosTags.Blocks.CRYSTAL_BUDS);
			this.tag(Tags.Blocks.CLUSTERS).addTag(PagamosTags.Blocks.CRYSTAL_CLUSTERS);

		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(Pagamos.MODID) && condition.apply(block));
		}

		@SafeVarargs
		private void addMatching(Function<Block, Boolean> condition, TagKey<Block>... tags)
		{
			for (TagKey<Block> tag : tags)
				getMatching(condition).forEach(this.tag(tag)::add);
		}

		@SuppressWarnings("unused")
		private void addMatching(TagKey<Block> blockTag, Function<Block, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(blockTag)::add);
		}

		@Override
		public String getName()
		{
			return "Pagamos Block Tags";
		}
	}

	public static class ItemTagProv extends ItemTagsProvider
	{
		public ItemTagProv(DataGenerator generatorIn, CompletableFuture<TagLookup<Block>> blocktagProvIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, blocktagProvIn, Pagamos.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			mod();
			vanilla();
			forge();
		}

		void mod()
		{
			this.copy(PagamosTags.Blocks.TOURMALINE_ORES, PagamosTags.Items.TOURMALINE_ORES);
			this.copy(PagamosTags.Blocks.SAPPHIRE_ORES, PagamosTags.Items.SAPPHIRE_ORES);

			this.copy(PagamosTags.Blocks.STORAGE_BLOCKS_TOURMALINE, PagamosTags.Items.STORAGE_BLOCKS_TOURMALINE);
			this.copy(PagamosTags.Blocks.STORAGE_BLOCKS_SAPPHIRE, PagamosTags.Items.STORAGE_BLOCKS_SAPPHIRE);

			this.tag(PagamosTags.Items.GEMS_TOURMALINE).add(tourmaline);
			this.tag(PagamosTags.Items.GEMS_SAPPHIRE).add(sapphire);

			this.tag(PagamosTags.Items.REPAIRS_TOURMALINE_ARMOR).addTag(PagamosTags.Items.GEMS_TOURMALINE);
			this.tag(PagamosTags.Items.REPAIRS_SAPPHIRE_ARMOR).addTag(PagamosTags.Items.GEMS_SAPPHIRE);

			this.tag(PagamosTags.Items.TOURMALINE_TOOL_MATERIALS).addTag(PagamosTags.Items.GEMS_TOURMALINE);
			this.tag(PagamosTags.Items.SAPPHIRE_TOOL_MATERIALS).addTag(PagamosTags.Items.GEMS_SAPPHIRE);

			this.tag(PagamosTags.Items.ICE_RODS).add(ice_rod);
		}

		void vanilla()
		{
			this.copy(BlockTags.SLABS, ItemTags.SLABS);
			this.copy(BlockTags.STAIRS, ItemTags.STAIRS);
			this.copy(BlockTags.WALLS, ItemTags.WALLS);
			// this.copy(BlockTags.STONE_BUTTONS, ItemTags.STONE_BUTTONS);
			this.copy(BlockTags.STONE_BRICKS, ItemTags.STONE_BRICKS);

			this.tag(ItemTags.STONE_TOOL_MATERIALS).add(cobbled_bergstone.asItem());
			this.tag(ItemTags.STONE_CRAFTING_MATERIALS).add(cobbled_bergstone.asItem());

			this.tag(ItemTags.PICKAXES).add(tourmaline_pickaxe, sapphire_pickaxe);
			this.tag(ItemTags.AXES).add(tourmaline_axe, sapphire_axe);
			this.tag(ItemTags.SHOVELS).add(tourmaline_shovel, sapphire_shovel);
			this.tag(ItemTags.HOES).add(tourmaline_hoe, sapphire_hoe);
			this.tag(ItemTags.SWORDS).add(tourmaline_sword, sapphire_sword);

			this.tag(ItemTags.CLUSTER_MAX_HARVESTABLES).add(tourmaline_pickaxe, sapphire_pickaxe);

			this.tag(ItemTags.TRIMMABLE_ARMOR).add(tourmaline_helmet, tourmaline_chestplate, tourmaline_leggings, tourmaline_boots);
			this.tag(ItemTags.TRIMMABLE_ARMOR).add(sapphire_helmet, sapphire_chestplate, sapphire_leggings, sapphire_boots);
			this.tag(ItemTags.HEAD_ARMOR).add(tourmaline_helmet, sapphire_helmet);
			this.tag(ItemTags.CHEST_ARMOR).add(tourmaline_chestplate, sapphire_chestplate);
			this.tag(ItemTags.LEG_ARMOR).add(tourmaline_leggings, sapphire_leggings);
			this.tag(ItemTags.FOOT_ARMOR).add(tourmaline_boots, sapphire_boots);

			/*ForgeRegistries.ITEMS.getValues().stream().filter(item -> item.getRegistryName().getNamespace().equals(Pagamos.MODID) && item instanceof MusicDiscItem).forEach(this.getOrCreateBuilder(ItemTags.MUSIC_DISCS)::add);*/
		}

		void forge()
		{
			this.copy(Tags.Blocks.ORES, Tags.Items.ORES);
			this.copy(Tags.Blocks.STORAGE_BLOCKS, Tags.Items.STORAGE_BLOCKS);
			this.copy(Tags.Blocks.COBBLESTONES, Tags.Items.COBBLESTONES);
			// this.copy(Tags.Blocks.BUDDING_BLOCKS, Tags.Items.BUDDING_BLOCKS);
			// this.copy(Tags.Blocks.BUDS, Tags.Items.BUDS);
			// this.copy(Tags.Blocks.CLUSTERS, Tags.Items.CLUSTERS);

			this.tag(Tags.Items.GEMS).addTags(PagamosTags.Items.GEMS_TOURMALINE, PagamosTags.Items.GEMS_SAPPHIRE);
			this.tag(Tags.Items.BRICKS).add(ice_brick);

			this.tag(Tags.Items.RODS).addTag(PagamosTags.Items.ICE_RODS);

			this.tag(Tags.Items.MELEE_WEAPON_TOOLS).add(tourmaline_sword, sapphire_sword, tourmaline_axe, sapphire_axe);
			this.tag(Tags.Items.MINING_TOOL_TOOLS).add(tourmaline_pickaxe, sapphire_pickaxe);
		}

		@Override
		public String getName()
		{
			return "Pagamos Item Tags";
		}
	}

	public static class BiomeTagProv extends BiomeTagsProvider
	{
		public BiomeTagProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, Pagamos.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			mod();
			vanilla();
			forge();
		}

		void mod()
		{
			this.add(PagamosTags.Biomes.IS_PAGAMOS, PagamosBiomes.SHIVERING_WASTES, PagamosBiomes.SHIMMERING_SHEETS, PagamosBiomes.MISTY_DRIFTS, PagamosBiomes.TITANIC_WILDS);
		}

		void vanilla()
		{
		}

		void forge()
		{
		}

		/*private void add(TagKey<Biome> tag, Collection<Pointer<Biome>> list)
		{
			this.tag(tag).add(list.stream().map((r) -> r.getKey()).toArray(ResourceKey[]::new));
		}*/

		private void add(TagKey<Biome> tag, Pointer<Biome>... registrars)
		{
			this.tag(tag).add(Arrays.stream(registrars).map((r) -> r.getKey()).toArray(ResourceKey[]::new));
		}

		@Override
		public String getName()
		{
			return "Pagamos Biome Tags";
		}
	}
}
