package com.legacy.pagamos.data;

import com.legacy.pagamos.Pagamos;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;

public interface PagamosTags
{
	public static interface Blocks
	{
		TagKey<Block> BLUE_FIRE_INFINIBURN = tag("blue_fire_infiniburn");

		TagKey<Block> DRIPSTONES_CAN_GENERATE_ON = tag("dripstones_can_generate_on");
		TagKey<Block> CRYSTAL_CAN_GENERATE_ON = tag("crystal_can_generate_on");
		TagKey<Block> CRYSTAL_CAN_REPLACE = tag("crystal_can_replace");

		TagKey<Block> PAGAMOS_PORTAL_FRAME = tag("pagamos_portal_frame");

		TagKey<Block> TOURMALINE_ORES = tag("ores/tourmaline");
		TagKey<Block> SAPPHIRE_ORES = tag("ores/sapphire");

		TagKey<Block> STORAGE_BLOCKS_TOURMALINE = tag("storage_blocks/tourmaline");
		TagKey<Block> STORAGE_BLOCKS_SAPPHIRE = tag("storage_blocks/sapphire");

		TagKey<Block> CRYSTAL_BLOCKS = tag("crystal_blocks");
		TagKey<Block> CRYSTAL_CLUSTERS = tag("crystal_clusters");
		TagKey<Block> CRYSTAL_BUDS = tag("crystal_buds");

		TagKey<Block> CAN_SUSTAIN_TITIAN_PLANT = tag("can_sustan_titian_plant");

		private static TagKey<Block> tag(String name)
		{
			return BlockTags.create(Pagamos.locate(name));
		}
	}

	public static interface Items
	{
		TagKey<Item> TOURMALINE_ORES = tag("ores/tourmaline");
		TagKey<Item> SAPPHIRE_ORES = tag("ores/sapphire");

		TagKey<Item> GEMS_TOURMALINE = tag("gems/tourmaline");
		TagKey<Item> GEMS_SAPPHIRE = tag("gems/sapphire");

		TagKey<Item> STORAGE_BLOCKS_TOURMALINE = tag("storage_blocks/tourmaline");
		TagKey<Item> STORAGE_BLOCKS_SAPPHIRE = tag("storage_blocks/sapphire");

		TagKey<Item> REPAIRS_TOURMALINE_ARMOR = tag("repairs_tourmaline_armor");
		TagKey<Item> REPAIRS_SAPPHIRE_ARMOR = tag("repairs_sapphire_armor");

		TagKey<Item> TOURMALINE_TOOL_MATERIALS = tag("tourmaline_tool_materials");
		TagKey<Item> SAPPHIRE_TOOL_MATERIALS = tag("sapphire_tool_materials");

		TagKey<Item> ICE_RODS = forgeTag("rods/ice");

		private static TagKey<Item> tag(String name)
		{
			return ItemTags.create(Pagamos.locate(name));
		}

		private static TagKey<Item> forgeTag(String name)
		{
			return ItemTags.create(ResourceLocation.fromNamespaceAndPath("c", name));
		}
	}

	public static interface Entities
	{
		// TagKey<EntityType<?>> TEMP = tag("temp");

		private static TagKey<EntityType<?>> tag(String name)
		{
			return TagKey.create(Registries.ENTITY_TYPE, Pagamos.locate(name));
		}
	}

	public static interface Biomes
	{
		TagKey<Biome> IS_PAGAMOS = tag("is_pagamos");

		private static TagKey<Biome> tag(String key)
		{
			return TagKey.create(Registries.BIOME, Pagamos.locate(key));
		}
	}
}
