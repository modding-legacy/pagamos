package com.legacy.pagamos.registry;

import com.legacy.pagamos.Pagamos;

import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.equipment.EquipmentAsset;
import net.minecraft.world.item.equipment.EquipmentAssets;

public interface PagamosEquipmentAssets
{
	ResourceKey<EquipmentAsset> TOURMALINE = createId("tourmaline");
	ResourceKey<EquipmentAsset> SAPPHIRE = createId("sapphire");

	static ResourceKey<EquipmentAsset> createId(String key)
	{
		return ResourceKey.create(EquipmentAssets.ROOT_ID, Pagamos.locate(key));
	}
}
