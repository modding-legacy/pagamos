package com.legacy.pagamos.registry;

import com.legacy.pagamos.Pagamos;
import com.mojang.blaze3d.vertex.DefaultVertexFormat;
import com.mojang.blaze3d.vertex.VertexFormat;

import net.minecraft.client.renderer.ShaderDefines;
import net.minecraft.client.renderer.ShaderProgram;
import net.neoforged.neoforge.client.event.RegisterShadersEvent;

public class PagamosShaders
{
	public static final ShaderProgram LIGHTMAP = register("pagamos_lightmap", DefaultVertexFormat.BLIT_SCREEN);

	private static ShaderProgram register(String key, VertexFormat format)
	{
		return register(key, format, ShaderDefines.EMPTY);
	}

	private static ShaderProgram register(String key, VertexFormat format, ShaderDefines defines)
	{
		ShaderProgram shaderprogram = new ShaderProgram(Pagamos.locate("core/" + key), format, defines);
		return shaderprogram;
	}

	public static void init(RegisterShadersEvent event)
	{
		event.registerShader(LIGHTMAP);
	}
}
