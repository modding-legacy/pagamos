package com.legacy.pagamos.registry;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.entity.CrystalPhoenixEntity;
import com.legacy.pagamos.entity.IceCreeperEntity;
import com.legacy.pagamos.entity.YetiEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacementTypes;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.RegisterSpawnPlacementsEvent;
import net.neoforged.neoforge.event.entity.RegisterSpawnPlacementsEvent.Operation;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PagamosEntityTypes
{
	public static final EntityType<YetiEntity> YETI = buildEntity("yeti", EntityType.Builder.of(YetiEntity::new, MobCategory.CREATURE).sized(0.9F, 3F).eyeHeight(2.6F));

	public static final EntityType<CrystalPhoenixEntity> CRYSTAL_PHOENIX = buildEntity("crystal_phoenix", EntityType.Builder.of(CrystalPhoenixEntity::new, MobCategory.MONSTER).sized(2F, 2.5F).fireImmune());
	public static final EntityType<IceCreeperEntity> ICE_CREEPER = buildEntity("ice_creeper", EntityType.Builder.of(IceCreeperEntity::new, MobCategory.MONSTER).sized(0.6F, 1.7F));

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("yeti", YETI);
		register("crystal_phoenix", CRYSTAL_PHOENIX);
		register("ice_creeper", ICE_CREEPER);
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, Pagamos.locate(name), () -> type);
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(ResourceKey.create(Registries.ENTITY_TYPE, Pagamos.locate(key)));
	}

	public static void registerPlacements(RegisterSpawnPlacementsEvent event)
	{
		event.register(PagamosEntityTypes.YETI, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, YetiEntity::canSpawn, Operation.REPLACE);
		event.register(PagamosEntityTypes.CRYSTAL_PHOENIX, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, CrystalPhoenixEntity::canSpawn, Operation.REPLACE);
		event.register(PagamosEntityTypes.ICE_CREEPER, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, IceCreeperEntity::canSpawn, Operation.REPLACE);
	}

	public static void registerPlacementOverrides(RegisterSpawnPlacementsEvent event)
	{
		event.register(EntityType.STRAY, null, null, (entityType, level, spawnReason, pos, random) ->
		{
			return level.getLevel() != null && level.getLevel().dimension() == PagamosDimensions.pagamosKey() && Monster.checkMonsterSpawnRules(entityType, level, spawnReason, pos, random);
		}, Operation.OR);
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(PagamosEntityTypes.YETI, YetiEntity.registerAttributes().build());
		event.put(PagamosEntityTypes.CRYSTAL_PHOENIX, CrystalPhoenixEntity.createAttributes().build());
		event.put(PagamosEntityTypes.ICE_CREEPER, IceCreeperEntity.createAttributes().build());
	}
}
