package com.legacy.pagamos.registry;

import com.legacy.pagamos.Pagamos;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.neoforged.neoforge.common.util.DeferredSoundType;

public interface PagamosSounds
{
	SoundEvent BLOCK_PORTAL_AMBIENT = create("block.pagamos_portal.ambient");
	SoundEvent BLOCK_PORTAL_TRAVEL = create("block.pagamos_portal.travel");
	SoundEvent BLOCK_PORTAL_LIGHT = create("block.pagamos_portal.light");
	SoundEvent BLOCK_PORTAL_DESTROYED = create("block.pagamos_portal.destroy");
	SoundEvent BLOCK_PORTAL_TRIGGER = create("block.pagamos_portal.trigger");

	// SoundEvent yeti_idle = register("entity.yeti.idle");
	// SoundEvent yeti_curious = register("entity.yeti.curious");
	// SoundEvent yeti_hurt = register("entity.yeti.hurt");
	// SoundEvent yeti_death = register("entity.yeti.death");

	SoundEvent ENTITY_CRYSTAL_PHOENIX_IDLE = create("entity.crystal_phoenix.idle");
	SoundEvent ENTITY_CRYSTAL_PHOENIX_HURT = create("entity.crystal_phoenix.hurt");
	SoundEvent ENTITY_CRYSTAL_PHOENIX_DEATH = create("entity.crystal_phoenix.death");
	SoundEvent ENTITY_CRYSTAL_PHOENIX_ATTACK = create("entity.crystal_phoenix.attack");

	SoundEvent ENTITY_ICE_CREEPER_HURT = create("entity.ice_creeper.hurt");
	SoundEvent ENTITY_ICE_CREEPER_DEATH = create("entity.ice_creeper.death");
	SoundEvent ENTITY_ICE_CREEPER_PRIMED = create("entity.ice_creeper.primed");

	SoundEvent RANDOM_ICE_CRACK = create("random.ice.crack");

	Holder<SoundEvent> FROZEN_HELL_LOOP = createForHolder("ambient.frozen_hell.loop");
	Holder<SoundEvent> FROZEN_HELL_ADDITIONS = createForHolder("ambient.frozen_hell.additions");

	Holder<SoundEvent> MISTY_DRIFTS_LOOP = createForHolder("ambient.misty_drifts.loop");
	Holder<SoundEvent> MISTY_DRIFTS_ADDITIONS = createForHolder("ambient.misty_drifts.additions");

	Holder<SoundEvent> TITANIC_WILDS_LOOP = createForHolder("ambient.titanic_wilds.loop");
	Holder<SoundEvent> TITANIC_WILDS_ADDITIONS = createForHolder("ambient.titanic_wilds.additions");

	DeferredSoundType MOSS_GROWTH_SOUND_TYPE = new DeferredSoundType(1.0F, 1.0F, () -> SoundEvents.DEEPSLATE_BREAK, () -> SoundEvents.MOSS_STEP, () -> SoundEvents.DEEPSLATE_PLACE, () -> SoundEvents.DEEPSLATE_HIT, () -> SoundEvents.DEEPSLATE_FALL);

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = Pagamos.locate(name);
		return Registry.register(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}

	private static Holder<SoundEvent> createForHolder(String name)
	{
		ResourceLocation location = Pagamos.locate(name);
		return Registry.registerForHolder(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}
}