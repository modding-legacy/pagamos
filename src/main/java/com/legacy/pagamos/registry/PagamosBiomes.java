package com.legacy.pagamos.registry;

import java.util.function.Function;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.data.PagamosBiomeProv;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.world.level.biome.Biome;
import net.neoforged.neoforge.registries.RegisterEvent;

@RegistrarHolder
public class PagamosBiomes
{
	public static final RegistrarHandler<Biome> HANDLER = RegistrarHandler.getOrCreate(Registries.BIOME, Pagamos.MODID);

	public static final Pointer<Biome> SHIVERING_WASTES = create("shivering_wastes", PagamosBiomeProv::shiveringWastes);
	public static final Pointer<Biome> SHIMMERING_SHEETS = create("shimmering_sheets", PagamosBiomeProv::shimmeringSheets);
	public static final Pointer<Biome> MISTY_DRIFTS = create("misty_drifts", PagamosBiomeProv::mistyDrifts);
	public static final Pointer<Biome> TITANIC_WILDS = create("titanic_wilds", PagamosBiomeProv::titanicWilds);

	public static void init(RegisterEvent event)
	{
	}

	private static Pointer<Biome> create(String name, Function<BootstrapContext<?>, Biome> biome)
	{
		return HANDLER.createPointer(name, biome);
	}
}
