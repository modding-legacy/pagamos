package com.legacy.pagamos.registry;

import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.data.PagamosBiomeProv;
import com.legacy.pagamos.world.features.HugeCrystalFeature;
import com.legacy.pagamos.world.features.HugeMaroonMushroomFeature;
import com.legacy.pagamos.world.features.PointedBergstoneClusterFeature;
import com.legacy.pagamos.world.features.SmallCrystalClumpFeature;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.Registrar.Pointer;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.BiasedToBottomInt;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.util.valueproviders.WeightedListInt;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CaveVines;
import net.minecraft.world.level.block.CaveVinesBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.BlockColumnConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.RandomizedIntStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountOnEveryLayerPlacement;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.EnvironmentScanPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.NoiseThresholdCountPlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RandomOffsetPlacement;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.RuleTest;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PagamosFeatures
{
	public static final Feature<NoneFeatureConfiguration> HUGE_CRYSTAL = new HugeCrystalFeature(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> CRYSTAL_CLUMP = new SmallCrystalClumpFeature(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> HUGE_MAROON_MUSHROOM = new HugeMaroonMushroomFeature(NoneFeatureConfiguration.CODEC);

	public static final Feature<NoneFeatureConfiguration> POINTED_BERGSTONE_CLUSTER = new PointedBergstoneClusterFeature(NoneFeatureConfiguration.CODEC);

	public static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("huge_crystal", HUGE_CRYSTAL);
		register("crystal_clump", CRYSTAL_CLUMP);
		register("huge_maroon_mushroom", HUGE_MAROON_MUSHROOM);

		register("pointed_bergstone_cluster", POINTED_BERGSTONE_CLUSTER);

		Configured.init();
		Placements.init();
	}

	private static void register(String key, Feature<?> feature)
	{
		registerEvent.register(Registries.FEATURE, Pagamos.locate(key), () -> feature);
	}

	public static void addDefaultSpawns(MobSpawnSettings.Builder spawns)
	{
		spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(PagamosEntityTypes.YETI, 1, 1, 1));
		/*spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(PagamosEntityTypes.PHOENIX, 1, 1, 1));*/
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(PagamosEntityTypes.ICE_CREEPER, 1, 1, 1));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.STRAY, 3, 1, 1));

		// TODO: Real values
		spawns.addMobCharge(PagamosEntityTypes.YETI, 0.7D, 0.15D);

		spawns.addMobCharge(PagamosEntityTypes.ICE_CREEPER, 0.7D, 0.15D);
		spawns.addMobCharge(EntityType.STRAY, 0.7D, 0.15D);
	}

	public static void addDefaultFeatures(PagamosBiomeProv.RegistrarBuilder builder)
	{
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.BLUE_FIRE_PATCH);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_DECORATION, PagamosFeatures.Placements.POINTED_BERGSTONE_CLUSTER);

		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PagamosFeatures.Placements.MISTSHALE_BLOB);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PagamosFeatures.Placements.TOURMALINE_ORE);
		builder.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, PagamosFeatures.Placements.SAPPHIRE_ORE);
	}

	@RegistrarHolder
	public static class Configured
	{
		public static final RegistrarHandler<ConfiguredFeature<?, ?>> HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, Pagamos.MODID);

		public static final Pointer<ConfiguredFeature<?, ?>> HUGE_CRYSTAL = register("huge_crystal", PagamosFeatures.HUGE_CRYSTAL, () -> FeatureConfiguration.NONE);

		private static final Supplier<RuleTest> SNOW_RULE = () -> new BlockMatchTest(PagamosBlocks.packed_snow),
				BERGSTONE_RULE = () -> new BlockMatchTest(PagamosBlocks.bergstone);

		public static final Pointer<ConfiguredFeature<?, ?>> CRYSTAL_CLUMP = register("crystal_clump", PagamosFeatures.CRYSTAL_CLUMP, () -> FeatureConfiguration.NONE);

		public static final Pointer<ConfiguredFeature<?, ?>> POINTED_BERGSTONE_CLUSTER = register("pointed_bergstone_cluster", PagamosFeatures.POINTED_BERGSTONE_CLUSTER, () -> FeatureConfiguration.NONE);

		public static final Pointer<ConfiguredFeature<?, ?>> MISTSHALE_BLOB = register("mistshale_blob", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(SNOW_RULE.get(), PagamosBlocks.mistshale.defaultBlockState())), 33));
		public static final Pointer<ConfiguredFeature<?, ?>> TOURMALINE_ORE = register("tourmaline_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(SNOW_RULE.get(), PagamosBlocks.snowed_tourmaline_ore.defaultBlockState()), OreConfiguration.target(BERGSTONE_RULE.get(), PagamosBlocks.bergstone_tourmaline_ore.defaultBlockState())), 8));
		public static final Pointer<ConfiguredFeature<?, ?>> SAPPHIRE_ORE = register("sapphire_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(SNOW_RULE.get(), PagamosBlocks.snowed_sapphire_ore.defaultBlockState()), OreConfiguration.target(BERGSTONE_RULE.get(), PagamosBlocks.bergstone_sapphire_ore.defaultBlockState())), 8));

		public static final Pointer<ConfiguredFeature<?, ?>> BLUE_FIRE_PATCH = register("blue_fire_patch", Feature.RANDOM_PATCH, Configs.BLUE_FIRE);

		public static final Pointer<ConfiguredFeature<?, ?>> SALMON_VINES = register("salmon_vines", Feature.BLOCK_COLUMN, () ->
		{
			WeightedStateProvider plantStateProv = new WeightedStateProvider(SimpleWeightedRandomList.<BlockState>builder().add(PagamosBlocks.salmon_vines_plant.defaultBlockState(), 4).add(PagamosBlocks.salmon_vines_plant.defaultBlockState().setValue(CaveVines.BERRIES, true), 1));
			RandomizedIntStateProvider tipStateProv = new RandomizedIntStateProvider(new WeightedStateProvider(SimpleWeightedRandomList.<BlockState>builder().add(PagamosBlocks.salmon_vines.defaultBlockState(), 4).add(PagamosBlocks.salmon_vines.defaultBlockState().setValue(CaveVines.BERRIES, true), 1)), CaveVinesBlock.AGE, UniformInt.of(23, 25));

			return new BlockColumnConfiguration(List.of(BlockColumnConfiguration.layer(new WeightedListInt(SimpleWeightedRandomList.<IntProvider>builder().add(UniformInt.of(5, 19), 2).add(UniformInt.of(0, 2), 3).add(UniformInt.of(3, 6), 10).build()), plantStateProv), BlockColumnConfiguration.layer(ConstantInt.of(1), tipStateProv)), Direction.DOWN, BlockPredicate.ONLY_IN_AIR_PREDICATE, true);
		});

		public static final Pointer<ConfiguredFeature<?, ?>> HUGE_MAROON_MUSHROOM = register("huge_maroon_mushroom", PagamosFeatures.HUGE_MAROON_MUSHROOM, () -> FeatureConfiguration.NONE);

		public static final Pointer<ConfiguredFeature<?, ?>> TITANWEED_64 = register("titianweed_patch_64", Feature.RANDOM_PATCH, Configs.TITIANEWEED_64);
		public static final Pointer<ConfiguredFeature<?, ?>> TALL_TITANEWEED_16 = register("tall_titianweed_patch_16", Feature.RANDOM_PATCH, Configs.TALL_TITIANEWEED_16);
		public static final Pointer<ConfiguredFeature<?, ?>> MAROON_MUSHROOM_10 = register("maroon_mushroom_patch_10", Feature.RANDOM_PATCH, Configs.MAROON_MUSHROOM_10);
		public static final Pointer<ConfiguredFeature<?, ?>> LUMIBULB_10 = register("lumibulb_patch_10", Feature.RANDOM_PATCH, Configs.LUMIBULB_10);

		public static void init()
		{
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Supplier<FC> config)
		{
			return register(key, feature, (c) -> config);
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Function<BootstrapContext<?>, Supplier<FC>> config)
		{
			return HANDLER.createPointer(key, (c) -> new ConfiguredFeature<>(feature, config.apply(c).get()));
		}

		protected static interface Configs
		{
			Supplier<RandomPatchConfiguration> BLUE_FIRE = () -> FeatureUtils.simplePatchConfiguration(Feature.SIMPLE_BLOCK, new SimpleBlockConfiguration(BlockStateProvider.simple(PagamosBlocks.blue_fire)), List.of(PagamosBlocks.bergstone, PagamosBlocks.cobbled_bergstone, PagamosBlocks.mistshale));

			Supplier<RandomPatchConfiguration> TITIANEWEED_64 = () -> simplePatch("titianeweed_64", PagamosBlocks.short_titianeweed.defaultBlockState(), 64);
			Supplier<RandomPatchConfiguration> TALL_TITIANEWEED_16 = () -> simplePatch("tall_titianeweed_16", PagamosBlocks.tall_titianeweed.defaultBlockState(), 16);
			Supplier<RandomPatchConfiguration> MAROON_MUSHROOM_10 = () -> simplePatch("maroon_mushroom_10", PagamosBlocks.maroon_mushroom.defaultBlockState(), 10);
			Supplier<RandomPatchConfiguration> LUMIBULB_10 = () -> simplePatch("lumibulb_10", PagamosBlocks.lumibulb.defaultBlockState(), 10);

			public static RandomPatchConfiguration simplePatch(String name, BlockState state, int tries)
			{
				return simplePatch(name, state, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
			}

			public static RandomPatchConfiguration filteredPatch(String name, BlockState state, int tries, Block notBlock)
			{
				return simplePatch(name, state, tries, p -> PlacementUtils.filtered(p.getLeft(), p.getRight(), BlockPredicate.allOf(BlockPredicate.matchesBlocks(Blocks.AIR), BlockPredicate.not(BlockPredicate.matchesBlocks(BlockPos.ZERO.below(), notBlock)))));
			}

			@SuppressWarnings("unchecked")
			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration simplePatch(String name, BlockState state, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
			{
				return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(BlockStateProvider.simple(state)))));
			}

			public static RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries)
			{
				return weightedPatch(name, builder, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
			}

			@SuppressWarnings("unchecked")
			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
			{
				return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(new WeightedStateProvider(builder)))));
			}

			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration randomPatchConfig(String name, int tries, Holder<PlacedFeature> placedHolder)
			{
				return FeatureUtils.simpleRandomPatchConfiguration(tries, placedHolder);
			}

			public static SimpleWeightedRandomList.Builder<BlockState> weighted()
			{
				return SimpleWeightedRandomList.<BlockState>builder();
			}
		}
	}

	@SuppressWarnings({ "unused", "deprecation" })
	@RegistrarHolder
	public static class Placements
	{
		public static final RegistrarHandler<PlacedFeature> HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, Pagamos.MODID);

		public static final Pointer<PlacedFeature> MISTSHALE_BLOB = register("mistshale_blob", Configured.MISTSHALE_BLOB, countRange(127, 8));

		public static final Pointer<PlacedFeature> TOURMALINE_ORE = register("tourmaline_ore", Configured.TOURMALINE_ORE, countRange(64, 8));
		public static final Pointer<PlacedFeature> SAPPHIRE_ORE = register("sapphire_ore", Configured.SAPPHIRE_ORE, countRange(32, 4));

		public static final Pointer<PlacedFeature> HUGE_CRYSTAL = register("huge_crystal", Configured.HUGE_CRYSTAL, List.of(CountPlacement.of(BiasedToBottomInt.of(14, 18)), InSquarePlacement.spread(), PlacementUtils.RANGE_4_4, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> CRYSTAL_CLUMP_UNCOMMON = register("crystal_clump_uncommon", Configured.CRYSTAL_CLUMP, List.of(RarityFilter.onAverageOnceEvery(3), InSquarePlacement.spread(), PlacementUtils.RANGE_4_4, BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> CRYSTAL_CLUMP_COMMON = register("crystal_clump_common", Configured.CRYSTAL_CLUMP, List.of(CountPlacement.of(UniformInt.of(0, 3)), InSquarePlacement.spread(), PlacementUtils.RANGE_4_4, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> POINTED_BERGSTONE_CLUSTER = register("pointed_bergstone_cluster", Configured.POINTED_BERGSTONE_CLUSTER, List.of(CountPlacement.of(UniformInt.of(25, 30)), InSquarePlacement.spread(), PlacementUtils.RANGE_4_4, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> BLUE_FIRE_PATCH = register("blue_fire_patch", Configured.BLUE_FIRE_PATCH, List.of(CountPlacement.of(UniformInt.of(0, 6)), InSquarePlacement.spread(), PlacementUtils.RANGE_4_4, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> HUGE_MAROON_MUSHROOM = register("huge_maroon_mushroom", Configured.HUGE_MAROON_MUSHROOM, List.of(CountOnEveryLayerPlacement.of(2), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> SALMON_VINES = register("salmon_vines", Configured.SALMON_VINES, List.of(CountPlacement.of(BiasedToBottomInt.of(160, 188)), InSquarePlacement.spread(), PlacementUtils.RANGE_BOTTOM_TO_MAX_TERRAIN_HEIGHT, EnvironmentScanPlacement.scanningFor(Direction.UP, BlockPredicate.hasSturdyFace(Direction.DOWN), BlockPredicate.ONLY_IN_AIR_PREDICATE, 12), RandomOffsetPlacement.vertical(ConstantInt.of(-1)), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> TITANWEED_64 = register("titianweed_patch_64", Configured.TITANWEED_64, List.of(CountOnEveryLayerPlacement.of(4), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> TALL_TITANEWEED_16 = register("tall_titianweed_patch_16", Configured.TALL_TITANEWEED_16, List.of(CountOnEveryLayerPlacement.of(3), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> MAROON_MUSHROOM_10 = register("maroon_mushroom_patch_10", Configured.MAROON_MUSHROOM_10, List.of(CountOnEveryLayerPlacement.of(1), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> LUMIBULB_10 = register("lumibulb_patch_10", Configured.LUMIBULB_10, List.of(CountOnEveryLayerPlacement.of(2), BiomeFilter.biome()));

		public static void init()
		{
		}

		private static Pointer<PlacedFeature> register(String key, Pointer<ConfiguredFeature<?, ?>> feature, List<PlacementModifier> mods)
		{
			return HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods)));
		}

		private static List<PlacementModifier> countRange(int height, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.bottom(), VerticalAnchor.absolute(height)), BiomeFilter.biome());
		}

		private static List<PlacementModifier> countRange(int bottom, int height, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.absolute(bottom), VerticalAnchor.absolute(height)), BiomeFilter.biome());
		}

		private static List<PlacementModifier> countRange(HeightRangePlacement heightRange, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), heightRange, BiomeFilter.biome());
		}

		/*private static PlacementModifier range(int height)
		{
			// still from 0
			return HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(height));
		}
		
		private static List<PlacementModifier> count(int count)
		{
			return flower(count, false);
		}*/

		private static List<PlacementModifier> flower(int count, boolean average)
		{
			PlacementModifier modifier = average ? RarityFilter.onAverageOnceEvery(count) : CountPlacement.of(count);
			return List.of(modifier, InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
		}

		private static List<PlacementModifier> countNoise(double noise, int x, int y)
		{
			return countNoise(noise, x, y, PlacementUtils.HEIGHTMAP);
		}

		private static List<PlacementModifier> countNoise(double noise, int x, int y, PlacementModifier heightmap)
		{
			return List.of(NoiseThresholdCountPlacement.of(noise, x, y), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
		}
	}
}
