package com.legacy.pagamos.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.block.ColdFireBlock;
import com.legacy.pagamos.block.CreeperIceBlock;
import com.legacy.pagamos.block.CrystalClusterBlock;
import com.legacy.pagamos.block.FreezerBlock;
import com.legacy.pagamos.block.GeyserBlock;
import com.legacy.pagamos.block.GrowthWeedBlock;
import com.legacy.pagamos.block.MossGrowthBlock;
import com.legacy.pagamos.block.PagamosBreakableBlock;
import com.legacy.pagamos.block.PagamosPortalBlock;
import com.legacy.pagamos.block.PointedBergstoneBlock;
import com.legacy.pagamos.block.SalmonVinesBlock;
import com.legacy.pagamos.block.SalmonVinesPlantBlock;
import com.legacy.pagamos.block.TallGrowthWeedBlock;
import com.legacy.pagamos.block.TitianFoliageBlock;
import com.legacy.pagamos.block.TitianMushroomBlock;
import com.legacy.pagamos.util.BlockReg;
import com.legacy.structure_gel.api.block.GelPortalBlock;

import net.minecraft.core.BlockPos;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PagamosBlocks
{
	public static Block crystal_ice;

	public static Block azure_crystal_block, budding_azure_crystal_block, azure_crystal, mauve_crystal_block,
			budding_mauve_crystal_block, mauve_crystal;

	public static Block crystal_plated_tiles, crystal_plated_tile_slab, crystal_plated_tile_stairs,
			crystal_plated_tile_wall;

	public static GelPortalBlock pagamos_portal;

	public static Block freezer, packed_snow;

	public static Block bergstone, bergstone_slab, bergstone_stairs, bergstone_wall;

	public static Block cobbled_bergstone, cobbled_bergstone_slab, cobbled_bergstone_stairs, cobbled_bergstone_wall,
			chiseled_bergstone;

	public static Block polished_bergstone, polished_bergstone_slab, polished_bergstone_stairs, polished_bergstone_wall;

	public static Block bergstone_bricks, bergstone_brick_slab, bergstone_brick_stairs, bergstone_brick_wall;

	public static Block igloo_bricks, igloo_brick_slab, igloo_brick_stairs, igloo_brick_wall;

	public static Block mistshale, mistshale_slab, mistshale_stairs, mistshale_wall;
	public static Block polished_mistshale, polished_mistshale_slab, polished_mistshale_stairs, polished_mistshale_wall;
	public static Block mistshale_bricks, mistshale_brick_slab, mistshale_brick_stairs, mistshale_brick_wall;
	public static Block mistshale_tiles, mistshale_tile_slab, mistshale_tile_stairs, mistshale_tile_wall;

	public static Block creeper_ice;

	public static Block bergstone_tourmaline_ore, bergstone_sapphire_ore, snowed_tourmaline_ore, snowed_sapphire_ore,
			tourmaline_block, sapphire_block;

	public static Block blue_fire;

	public static Block short_titianeweed, tall_titianeweed, titian_growth, lumibulb, salmon_moss, maroon_mushroom,
			maroon_mushroom_block, salmon_vines, salmon_vines_plant;

	public static Block potted_maroon_mushroom;

	public static Block mist_geyser;

	public static Block pointed_bergstone;

	public static Map<String, Block> blockItemList = new HashMap<>();
	private static RegisterEvent registryEvent;

	@SuppressWarnings("deprecation")
	public static void init(RegisterEvent event)
	{
		registryEvent = event;

		pagamos_portal = register("pagamos_portal", PagamosPortalBlock::new, () -> rawCopy(Blocks.NETHER_PORTAL).noLootTable() /*(() -> Level.OVERWORLD, PagamosDimensions::pagamosType, () -> PoiType.NETHER_PORTAL, () -> (GelPortalBlock) pagamos_portal, () -> Blocks.SNOW_BLOCK.defaultBlockState())*/);

		packed_snow = register("packed_snow", Block::new, () -> Block.Properties.of().strength(2.0F, 0.2F).sound(SoundType.SNOW));
		crystal_ice = register("crystal_ice", PagamosBreakableBlock::new, () -> Block.Properties.of().strength(0.5F, 0.4F).sound(SoundType.GLASS).lightLevel((light) -> 9).friction(1.0F).noOcclusion());

		azure_crystal_block = register("azure_crystal_block", Block::new, copy(Blocks.GLOWSTONE));
		mauve_crystal_block = register("mauve_crystal_block", Block::new, copy(Blocks.GLOWSTONE));
		budding_azure_crystal_block = register("budding_azure_crystal_block", Block::new, copy(Blocks.GLOWSTONE));
		budding_mauve_crystal_block = register("budding_mauve_crystal_block", Block::new, copy(Blocks.GLOWSTONE));

		Supplier<BlockBehaviour.Properties> crystalProps = () -> rawCopy(Blocks.AMETHYST_CLUSTER).sound(SoundType.GLASS).emissiveRendering(PagamosBlocks::always);
		azure_crystal = register("azure_crystal", p -> new CrystalClusterBlock(7.0F, 3.0F, p), crystalProps);
		mauve_crystal = register("mauve_crystal", p -> new CrystalClusterBlock(7.0F, 3.0F, p), crystalProps);

		crystal_plated_tiles = register("crystal_plated_tiles", Block::new, () -> rawCopy(Blocks.GLOWSTONE).lightLevel(state -> 7));
		crystal_plated_tile_slab = BlockReg.slab("crystal_plated_tile_slab", crystal_plated_tiles);
		crystal_plated_tile_stairs = BlockReg.stairs("crystal_plated_tile_stairs", crystal_plated_tiles);
		crystal_plated_tile_wall = BlockReg.wall("crystal_plated_tile_wall", crystal_plated_tiles);

		short_titianeweed = register("short_titianeweed", GrowthWeedBlock::new, copy(Blocks.CRIMSON_ROOTS));
		tall_titianeweed = register("tall_titianeweed", TallGrowthWeedBlock::new, copy(Blocks.CRIMSON_ROOTS));
		titian_growth = register("titian_growth", MossGrowthBlock::new, () -> rawCopy(Blocks.DEEPSLATE).randomTicks().sound(PagamosSounds.MOSS_GROWTH_SOUND_TYPE));

		lumibulb = register("lumibulb", TitianFoliageBlock::new, () -> rawCopy(Blocks.CRIMSON_ROOTS).lightLevel((light) -> 10));

		salmon_moss = register("salmon_moss", Block::new, copy(Blocks.MOSS_BLOCK));

		maroon_mushroom = register("maroon_mushroom", p -> new TitianMushroomBlock(PagamosFeatures.Configured.HUGE_MAROON_MUSHROOM.getKey(), p), copy(Blocks.CRIMSON_FUNGUS));
		maroon_mushroom_block = register("maroon_mushroom_block", HugeMushroomBlock::new, copy(Blocks.NETHER_WART_BLOCK));

		mist_geyser = register("mist_geyser", GeyserBlock::new, () -> rawCopy(Blocks.BASALT).forceSolidOff().noOcclusion());

		bergstone = register("bergstone", Block::new, copy(Blocks.DEEPSLATE));
		bergstone_slab = BlockReg.slab("bergstone_slab", bergstone);
		bergstone_stairs = BlockReg.stairs("bergstone_stairs", bergstone);
		bergstone_wall = BlockReg.wall("bergstone_wall", bergstone);

		cobbled_bergstone = register("cobbled_bergstone", Block::new, copy(Blocks.COBBLED_DEEPSLATE));
		cobbled_bergstone_slab = BlockReg.slab("cobbled_bergstone_slab", cobbled_bergstone);
		cobbled_bergstone_stairs = BlockReg.stairs("cobbled_bergstone_stairs", cobbled_bergstone);
		cobbled_bergstone_wall = BlockReg.wall("cobbled_bergstone_wall", cobbled_bergstone);

		polished_bergstone = register("polished_bergstone", Block::new, copy(Blocks.POLISHED_DEEPSLATE));
		polished_bergstone_slab = BlockReg.slab("polished_bergstone_slab", polished_bergstone);
		polished_bergstone_stairs = BlockReg.stairs("polished_bergstone_stairs", polished_bergstone);
		polished_bergstone_wall = BlockReg.wall("polished_bergstone_wall", polished_bergstone);

		chiseled_bergstone = register("chiseled_bergstone", Block::new, copy(Blocks.CHISELED_DEEPSLATE));

		bergstone_bricks = register("bergstone_bricks", Block::new, copy(Blocks.DEEPSLATE_BRICKS));
		bergstone_brick_slab = BlockReg.slab("bergstone_brick_slab", bergstone_bricks);
		bergstone_brick_stairs = BlockReg.stairs("bergstone_brick_stairs", bergstone_bricks);
		bergstone_brick_wall = BlockReg.wall("bergstone_brick_wall", bergstone_bricks);

		igloo_bricks = register("igloo_bricks", PagamosBreakableBlock::new, () -> rawCopy(Blocks.PACKED_ICE).noOcclusion());
		igloo_brick_slab = BlockReg.slab("igloo_brick_slab", igloo_bricks);
		igloo_brick_stairs = BlockReg.stairs("igloo_brick_stairs", igloo_bricks);
		igloo_brick_wall = BlockReg.wall("igloo_brick_wall", igloo_bricks);

		mistshale = register("mistshale", Block::new, copy(Blocks.TUFF));
		mistshale_slab = BlockReg.slab("mistshale_slab", mistshale);
		mistshale_stairs = BlockReg.stairs("mistshale_stairs", mistshale);
		mistshale_wall = BlockReg.wall("mistshale_wall", mistshale);

		polished_mistshale = register("polished_mistshale", Block::new, copy(Blocks.POLISHED_TUFF));
		polished_mistshale_slab = BlockReg.slab("polished_mistshale_slab", polished_mistshale);
		polished_mistshale_stairs = BlockReg.stairs("polished_mistshale_stairs", polished_mistshale);
		polished_mistshale_wall = BlockReg.wall("polished_mistshale_wall", polished_mistshale);

		mistshale_bricks = register("mistshale_bricks", Block::new, copy(Blocks.TUFF_BRICKS));
		mistshale_brick_slab = BlockReg.slab("mistshale_brick_slab", mistshale_bricks);
		mistshale_brick_stairs = BlockReg.stairs("mistshale_brick_stairs", mistshale_bricks);
		mistshale_brick_wall = BlockReg.wall("mistshale_brick_wall", mistshale_bricks);

		mistshale_tiles = register("mistshale_tiles", Block::new, copy(Blocks.TUFF_BRICKS));
		mistshale_tile_slab = BlockReg.slab("mistshale_tile_slab", mistshale_tiles);
		mistshale_tile_stairs = BlockReg.stairs("mistshale_tile_stairs", mistshale_tiles);
		mistshale_tile_wall = BlockReg.wall("mistshale_tile_wall", mistshale_tiles);

		pointed_bergstone = register("pointed_bergstone", PointedBergstoneBlock::new, copy(Blocks.POINTED_DRIPSTONE));

		salmon_vines = register("salmon_vines", SalmonVinesBlock::new, () -> rawCopy(Blocks.CAVE_VINES).lightLevel(state -> 10));
		salmon_vines_plant = register("salmon_vines_plant", SalmonVinesPlantBlock::new, () -> rawCopy(Blocks.CAVE_VINES_PLANT).lightLevel(state -> 0));

		blue_fire = registerBlock("blue_fire", ColdFireBlock::new, () -> rawCopy(Blocks.FIRE).randomTicks());

		tourmaline_block = register("tourmaline_block", Block::new, copy(Blocks.DIAMOND_BLOCK));
		sapphire_block = register("sapphire_block", Block::new, copy(Blocks.DIAMOND_BLOCK));

		bergstone_tourmaline_ore = register("bergstone_tourmaline_ore", Block::new, copy(Blocks.DEEPSLATE_DIAMOND_ORE));
		bergstone_sapphire_ore = register("bergstone_sapphire_ore", Block::new, copy(Blocks.DEEPSLATE_DIAMOND_ORE));
		snowed_tourmaline_ore = register("snowed_tourmaline_ore", Block::new, () -> rawCopy(Blocks.DIAMOND_ORE).sound(SoundType.SNOW));
		snowed_sapphire_ore = register("snowed_sapphire_ore", Block::new, () -> rawCopy(Blocks.DIAMOND_ORE).sound(SoundType.SNOW));

		creeper_ice = register("creeper_ice", CreeperIceBlock::new, () -> rawCopy(Blocks.ICE).strength(0.2F, 0.0F));

		freezer = register("freezer", FreezerBlock::new, copy(Blocks.FURNACE));

		potted_maroon_mushroom = BlockReg.flowerPot("potted_maroon_mushroom", () -> maroon_mushroom);

		/*crystal_water = register("crystal_water", new BlockCrystalWater()).setCreativeTab(null);
		crystal_water_flowing = register("crystal_water_flowing", new BlockCrystalWaterFlowing()).setCreativeTab(null);*/
	}

	private static Supplier<BlockBehaviour.Properties> copy(Block from)
	{
		return () -> rawCopy(from);
	}

	private static BlockBehaviour.Properties rawCopy(Block from)
	{
		return BlockBehaviour.Properties.ofFullCopy(from);
	}

	public static <T extends Block> T register(String key, Function<BlockBehaviour.Properties, T> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		T block = registerBlock(key, blockFunc, newProps);
		blockItemList.put(key, block);
		return block;
	}

	public static <T extends Block> T registerBlock(String name, Function<BlockBehaviour.Properties, T> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		if (registryEvent != null)
		{
			BlockBehaviour.Properties props = newProps.get().setId(key(name));
			T block = blockFunc.apply(props);

			// System.out.println("registering " + name);
			registryEvent.register(Registries.BLOCK, Pagamos.locate(name), () -> block);

			return block;
		}

		return null;
	}

	private static ResourceKey<Block> key(String path)
	{
		return ResourceKey.create(Registries.BLOCK, Pagamos.locate(path));
	}

	private static boolean always(BlockState state, BlockGetter blockGetter, BlockPos pos)
	{
		return true;
	}

	/*private static boolean never(BlockState state, BlockGetter blockGetter, BlockPos pos)
	{
		return false;
	}*/
}
