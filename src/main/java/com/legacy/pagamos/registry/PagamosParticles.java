package com.legacy.pagamos.registry;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.client.particle.CrystalShavingParticle;
import com.legacy.pagamos.client.particle.TitianSporeParticle;

import net.minecraft.core.particles.ParticleType;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.core.registries.Registries;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PagamosParticles
{
	public static final SimpleParticleType CRYSTAL_SHAVING = new SimpleParticleType(false);
	public static final SimpleParticleType TITIAN_SPORE = new SimpleParticleType(false);

	public static void init(RegisterEvent event)
	{
		register(event, "crystal_shaving", CRYSTAL_SHAVING);
		register(event, "titian_spore", TITIAN_SPORE);
	}

	private static void register(RegisterEvent event, String key, ParticleType<?> particle)
	{
		event.register(Registries.PARTICLE_TYPE, Pagamos.locate(key), () -> particle);
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory
	{
		public static void init(net.neoforged.neoforge.client.event.RegisterParticleProvidersEvent event)
		{
			event.registerSpriteSet(CRYSTAL_SHAVING, CrystalShavingParticle.Factory::new);
			event.registerSpriteSet(TITIAN_SPORE, TitianSporeParticle.Factory::new);
		}
	}
}
