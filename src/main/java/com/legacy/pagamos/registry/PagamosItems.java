package com.legacy.pagamos.registry;

import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.item.FlintAndLapisItem;
import com.legacy.pagamos.item.PagamosArmorMaterial;
import com.legacy.pagamos.item.PagamosItemTiers;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.food.Foods;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.equipment.ArmorType;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.registries.RegisterEvent;

public class PagamosItems
{
	private static RegisterEvent registryEvent;

	public static Item azure_crystal_shard, mauve_crystal_shard, ice_shard, tourmaline, sapphire, phoenix_feather,
			ice_rod, flint_and_lapis, ice_brick;

	public static Item tourmaline_sword, tourmaline_pickaxe, tourmaline_axe, tourmaline_shovel, tourmaline_hoe;
	public static Item sapphire_sword, sapphire_pickaxe, sapphire_axe, sapphire_shovel, sapphire_hoe;

	public static Item tourmaline_helmet, tourmaline_chestplate, tourmaline_leggings, tourmaline_boots;
	public static Item sapphire_helmet, sapphire_chestplate, sapphire_leggings, sapphire_boots;

	/*public static Item crystal_sword, crystal_pickaxe, crystal_axe, crystal_shovel, ice_spear;*/

	public static Item phoenix_spear;

	public static Item titian_berry;

	public static Item yeti_spawn_egg, crystal_phoenix_spawn_egg, ice_creeper_spawn_egg;
	/*public static ToolMaterial tourmaline = EnumHelper.addToolMaterial("tourmaline", 2, 250, 12.8F, 8.0F, 14).setRepairItem(new ItemStack(IceItems.tourmaline));
	
	public static ToolMaterial ICE = EnumHelper.addToolMaterial("ICE", 1, 131, 4.0F, 1.0F, 5).setRepairItem(new ItemStack(IceItems.ice_shard));*/

	public static void init(RegisterEvent event)
	{
		registryEvent = event;
		registerBlockItems();

		yeti_spawn_egg = register("yeti_spawn_egg", p -> new SpawnEggItem(PagamosEntityTypes.YETI, p));
		crystal_phoenix_spawn_egg = register("crystal_phoenix_spawn_egg", p -> new SpawnEggItem(PagamosEntityTypes.CRYSTAL_PHOENIX, p));
		ice_creeper_spawn_egg = register("ice_creeper_spawn_egg", p -> new SpawnEggItem(PagamosEntityTypes.ICE_CREEPER, p));

		flint_and_lapis = register("flint_and_lapis", FlintAndLapisItem::new, () -> new Item.Properties().durability(64));
		azure_crystal_shard = register("azure_crystal_shard");
		mauve_crystal_shard = register("mauve_crystal_shard");
		ice_brick = register("ice_brick");
		ice_shard = register("ice_shard");
		tourmaline = register("tourmaline");
		sapphire = register("sapphire");
		phoenix_feather = register("phoenix_feather");
		phoenix_spear = register("phoenix_spear");
		ice_rod = register("ice_rod");

		titian_berry = register("titian_berry", p -> new BlockItem(PagamosBlocks.salmon_vines, p.useItemDescriptionPrefix()), () -> new Item.Properties().food(Foods.GLOW_BERRIES));

		tourmaline_sword = register("tourmaline_sword", p -> new SwordItem(PagamosItemTiers.TOURMALINE, 3, -2.4F, p));
		tourmaline_pickaxe = register("tourmaline_pickaxe", p -> new PickaxeItem(PagamosItemTiers.TOURMALINE, 1, -2.8F, p));
		tourmaline_axe = register("tourmaline_axe", p -> new AxeItem(PagamosItemTiers.TOURMALINE, 5.0F, -3.0F, p));
		tourmaline_shovel = register("tourmaline_shovel", p -> new ShovelItem(PagamosItemTiers.TOURMALINE, 1.5F, -3.0F, p));
		tourmaline_hoe = register("tourmaline_hoe", p -> new HoeItem(PagamosItemTiers.TOURMALINE, 0, 0.0F, p));

		sapphire_sword = register("sapphire_sword", p -> new SwordItem(PagamosItemTiers.SAPPHIRE, 3, -2.4F, p));
		sapphire_pickaxe = register("sapphire_pickaxe", p -> new PickaxeItem(PagamosItemTiers.SAPPHIRE, 1, -2.8F, p));
		sapphire_axe = register("sapphire_axe", p -> new AxeItem(PagamosItemTiers.SAPPHIRE, 5.0F, -3.0F, p));
		sapphire_shovel = register("sapphire_shovel", p -> new ShovelItem(PagamosItemTiers.SAPPHIRE, 1.5F, -3.0F, p));
		sapphire_hoe = register("sapphire_hoe", p -> new HoeItem(PagamosItemTiers.SAPPHIRE, 0, 0.0F, p));

		ArmorType helmet = ArmorType.HELMET, chestplate = ArmorType.CHESTPLATE, leggings = ArmorType.LEGGINGS,
				boots = ArmorType.BOOTS;
		tourmaline_helmet = register("tourmaline_helmet", p -> new ArmorItem(PagamosArmorMaterial.TOURMALINE, helmet, p));
		tourmaline_chestplate = register("tourmaline_chestplate", p -> new ArmorItem(PagamosArmorMaterial.TOURMALINE, chestplate, p));
		tourmaline_leggings = register("tourmaline_leggings", p -> new ArmorItem(PagamosArmorMaterial.TOURMALINE, leggings, p));
		tourmaline_boots = register("tourmaline_boots", p -> new ArmorItem(PagamosArmorMaterial.TOURMALINE, boots, p));

		sapphire_helmet = register("sapphire_helmet", p -> new ArmorItem(PagamosArmorMaterial.SAPPHIRE, helmet, p));
		sapphire_chestplate = register("sapphire_chestplate", p -> new ArmorItem(PagamosArmorMaterial.SAPPHIRE, chestplate, p));
		sapphire_leggings = register("sapphire_leggings", p -> new ArmorItem(PagamosArmorMaterial.SAPPHIRE, leggings, p));
		sapphire_boots = register("sapphire_boots", p -> new ArmorItem(PagamosArmorMaterial.SAPPHIRE, boots, p));
	}

	private static void registerBlockItems()
	{
		for (Entry<String, Block> block : PagamosBlocks.blockItemList.entrySet())
			register(block.getKey(), p -> new BlockItem(block.getValue(), p), () -> new Item.Properties().useBlockDescriptionPrefix());

		PagamosBlocks.blockItemList.clear();
	}

	private static Item register(String name)
	{
		return register(name, Item::new);
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc)
	{
		return register(name, itemFunc, null);
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc, @Nullable Supplier<Item.Properties> newProps)
	{
		if (newProps == null)
			newProps = () -> new Item.Properties();

		Item.Properties props = newProps.get().setId(key(name));
		Item item = itemFunc.apply(props);

		registryEvent.register(Registries.ITEM, Pagamos.locate(name), () -> item);
		return item;
	}

	private static ResourceKey<Item> key(String path)
	{
		return ResourceKey.create(Registries.ITEM, Pagamos.locate(path));
	}
}