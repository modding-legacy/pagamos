package com.legacy.pagamos.registry;

import java.util.List;
import java.util.OptionalLong;
import java.util.function.Function;

import org.joml.Vector3f;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.mixin.NoiseRouterDataInvoker;
import com.legacy.pagamos.world.PagamosSurfaceRuleData;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.DimensionRegistrar;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.MultiNoiseBiomeSource;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseRouter;
import net.minecraft.world.level.levelgen.NoiseSettings;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.phys.Vec3;

@RegistrarHolder
public class PagamosDimensions
{
	public static final ResourceLocation PAGAMOS_ID = Pagamos.locate("pagamos");
	public static final DimensionRegistrar PAGAMOS = createPagamos();

	public static void init()
	{
	}

	private static DimensionRegistrar createPagamos()
	{
		Function<BootstrapContext<?>, DimensionType> dimType = c -> dimType(OptionalLong.of(18000L), 0, PAGAMOS_ID, false, 15);
		Function<BootstrapContext<?>, NoiseGeneratorSettings> noiseSettings = c -> newDimensionSettings(NoiseSettings.create(0, 128, 1, 2), PagamosBlocks.packed_snow.defaultBlockState(), PagamosBlocks.crystal_ice.defaultBlockState(), NoiseRouterDataInvoker.nether(c.lookup(Registries.DENSITY_FUNCTION), c.lookup(Registries.NOISE)), PagamosSurfaceRuleData.basicData(), 32, false);

		
		/*Function<BootstrapContext<?>, FixedBiomeSource> tempSource = c -> new FixedBiomeSource(c.lookup(Registries.BIOME).getOrThrow(PagamosBiomes.TITANIC_WILDS.getKey()));*/
		Function<BootstrapContext<?>, BiomeSource> biomeSource = c -> buildBiomeSource(c.lookup(Registries.BIOME));

		Function<BootstrapContext<?>, ChunkGenerator> chunkGen = c -> new NoiseBasedChunkGenerator(biomeSource.apply(c), PAGAMOS.getNoiseSettings().getHolder(c).get());

		return new DimensionRegistrar(PAGAMOS_ID, dimType, noiseSettings, chunkGen);
	}

	public static ResourceKey<Level> pagamosKey()
	{
		return PAGAMOS.getLevelKey();
	}

	public static final Vec3 ICE_FOG_VEC = new Vec3(0.045D, 0.066D, 0.15D);

	public static void getLightmapColors(float partialTicks, float sunBrightness, float skyLight, float blockLight, Vector3f colors)
	{
		float light = 0.2F;
		float x = light + 0.07F;
		float y = light + 0.09F;
		float z = light + 0.3F;

		colors.add(new Vector3f(x, y, z));
	}

	public static NoiseGeneratorSettings newDimensionSettings(NoiseSettings noise, BlockState defaultBlock, BlockState defaultFluid, NoiseRouter noiseRouter, SurfaceRules.RuleSource surfaceRule, int seaLevel, boolean useLegacyRandomSource)
	{
		return new NoiseGeneratorSettings(noise, defaultBlock, defaultFluid, noiseRouter, surfaceRule, List.of(), seaLevel, false, false, false, useLegacyRandomSource);
	}

	public static DimensionType dimType(OptionalLong fixedTime, int minY, ResourceLocation effects, boolean bedWorks, int monsterSpawnBlockLightLimit)
	{
		boolean hasSkyLight = true;
		boolean hasCeiling = true;
		boolean ultrawarm = false;
		boolean natural = true;
		double coordinateScale = 8.0;
		boolean respawnAnchorWorks = true;
		int height = 256;
		int logicalHeight = 127;
		TagKey<Block> infiniburn = BlockTags.INFINIBURN_OVERWORLD; // TODO
		float ambientLight = 0.05F;

		boolean piglinSafe = false;
		boolean hasRaids = false;
		IntProvider monsterSpawnLightTest = UniformInt.of(0, 7);

		return new DimensionType(fixedTime, hasSkyLight, hasCeiling, ultrawarm, natural, coordinateScale, bedWorks, respawnAnchorWorks, minY, height, logicalHeight, infiniburn, effects, ambientLight, new DimensionType.MonsterSettings(piglinSafe, hasRaids, monsterSpawnLightTest, monsterSpawnBlockLightLimit));
	}

	public static BiomeSource buildBiomeSource(HolderGetter<Biome> biomes)
	{
		// @formatter:off
        return MultiNoiseBiomeSource.createFromList(new Climate.ParameterList<>(List.of(
        		Pair.of(Climate.parameters(-0.3F, -0.4F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(PagamosBiomes.SHIVERING_WASTES.getKey())),
				Pair.of(Climate.parameters(0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(PagamosBiomes.SHIMMERING_SHEETS.getKey())), 
				Pair.of(Climate.parameters(0.2F, 0.5F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(PagamosBiomes.MISTY_DRIFTS.getKey())), 
				Pair.of(Climate.parameters(0.6F, 0.8F, 0.0F, 0.0F, 0.0F, 0.0F, 0.0F), biomes.getOrThrow(PagamosBiomes.TITANIC_WILDS.getKey())))));
     // @formatter:on
	}

	/*public static void bootstrapDimType(BootstrapContext<DimensionType> bootstrap)
	{
		bootstrap.register(PAGAMOS.getType().getKey(), PAGAMOS.getType().getInstance().apply(bootstrap));
	}
	
	public static void bootstrapNoiseSettings(BootstrapContext<NoiseGeneratorSettings> bootstrap)
	{
		bootstrap.register(PAGAMOS.getNoiseSettings().getKey(), PAGAMOS.getNoiseSettings().getInstance().apply(bootstrap));
	}
	
	public static void bootstrapLevelStem(BootstrapContext<LevelStem> bootstrap)
	{
		bootstrap.register(PAGAMOS.getLevelStem().getKey(), PAGAMOS.getLevelStem().getInstance().apply(bootstrap));
	}*/
}
