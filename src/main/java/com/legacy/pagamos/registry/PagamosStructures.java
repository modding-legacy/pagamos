package com.legacy.pagamos.registry;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.world.structure.MossArchPieces;
import com.legacy.pagamos.world.structure.MossArchStructure;
import com.legacy.structure_gel.api.registry.RegistrarHolder;
import com.legacy.structure_gel.api.registry.registrar.RegistrarHandler;
import com.legacy.structure_gel.api.registry.registrar.StructureRegistrar;
import com.legacy.structure_gel.api.structure.GridStructurePlacement;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.structure.pools.StructureTemplatePool;

@RegistrarHolder
public class PagamosStructures
{
	public static final RegistrarHandler<StructureTemplatePool> HANDLER = RegistrarHandler.getOrCreate(Registries.TEMPLATE_POOL, Pagamos.MODID).bootstrap(PagamosStructures::bootstrap);

	//@formatter:off
	public static final StructureRegistrar<MossArchStructure> MOSS_ARCH = StructureRegistrar.builder(Pagamos.locate("moss_arch"), () -> () -> MossArchStructure.CODEC)
			.addPiece("main", () -> MossArchPieces.Piece::new)
			.pushStructure(MossArchStructure::new)
				.biomes(PagamosBiomes.TITANIC_WILDS.getKey())
				.generationStep(Decoration.LOCAL_MODIFICATIONS).popStructure()
			.placement(() -> GridStructurePlacement.builder(5, 2, 0.7F).build(PagamosStructures.MOSS_ARCH.getRegistryName()))
			.build();
	//@formatter:on

	// le chicken and an egg situation has arrived
	public static void init()
	{
		/*SkiesStructureProcessors.init();
		Processors.init();*/

		/*FrozenBunkerPools.init();
		GatekeeperHousePools.init();
		SkiesVillagePools.init();*/
	}

	public static void bootstrap(BootstrapContext<StructureTemplatePool> bootstrap)
	{
	}
}
