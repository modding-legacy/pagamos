package com.legacy.pagamos.mixin;

import java.util.Objects;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import com.legacy.pagamos.client.PagamosClient;
import com.legacy.pagamos.registry.PagamosShaders;
import com.legacy.pagamos.registry.PagamosDimensions;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.CompiledShaderProgram;
import net.minecraft.client.renderer.LightTexture;

@SuppressWarnings("resource")
@Mixin(LightTexture.class)
public class LightTextureMixin
{
	@ModifyVariable(at = @At("STORE"), method = "updateLightTexture", index = 16)
	private CompiledShaderProgram modify$compiledshaderprogram(CompiledShaderProgram compiledshaderprogram)
	{
		if (Minecraft.getInstance().level.dimensionType().effectsLocation().equals(PagamosDimensions.PAGAMOS_ID))
		{
			CompiledShaderProgram customLightmap = Objects.requireNonNull(RenderSystem.setShader(PagamosShaders.LIGHTMAP), "Pagamos Lightmap shader not loaded");

			// FIXME: Pass in partialTicks
			customLightmap.safeGetUniform("DimLightModifier").set(PagamosClient.PagamosRenderInfo.getLightmapColors(0));

			return customLightmap;
		}

		return compiledshaderprogram;
	}
}
