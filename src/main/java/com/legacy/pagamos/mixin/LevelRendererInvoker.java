package com.legacy.pagamos.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.client.Camera;
import net.minecraft.client.renderer.LevelRenderer;

@Mixin(LevelRenderer.class)
public interface LevelRendererInvoker
{
	@Invoker("doesMobEffectBlockSky(Lnet/minecraft/client/Camera;)Z")
	abstract boolean glacidus$doesMobEffectBlockSky(Camera camera);
}
