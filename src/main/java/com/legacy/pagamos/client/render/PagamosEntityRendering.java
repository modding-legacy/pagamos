package com.legacy.pagamos.client.render;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.client.PagamosRenderRefs;
import com.legacy.pagamos.client.model.YetiModel;
import com.legacy.pagamos.client.render.entity.IceCreeperRenderer;
import com.legacy.pagamos.client.render.entity.YetiRenderer;
import com.legacy.pagamos.registry.PagamosEntityTypes;

import net.minecraft.client.renderer.entity.NoopRenderer;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;

@EventBusSubscriber(modid = Pagamos.MODID, bus = EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class PagamosEntityRendering
{
	@SubscribeEvent
	public static void initLayers(final EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		event.registerLayerDefinition(PagamosRenderRefs.YETI, YetiModel::createBodyLayer);
	}

	@SubscribeEvent
	public static void initRenders(final EntityRenderersEvent.RegisterRenderers event)
	{
		/*event.registerEntityRenderer(PagamosEntityTypes.YETI, YetiRenderer::new);
		event.registerEntityRenderer(PagamosEntityTypes.PHOENIX, PhoenixRenderer::new);*/

		event.registerEntityRenderer(PagamosEntityTypes.YETI, YetiRenderer::new);
		event.registerEntityRenderer(PagamosEntityTypes.CRYSTAL_PHOENIX, NoopRenderer::new);
		event.registerEntityRenderer(PagamosEntityTypes.ICE_CREEPER, IceCreeperRenderer::new);
	}
}