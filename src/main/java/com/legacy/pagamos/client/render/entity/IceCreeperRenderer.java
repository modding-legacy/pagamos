package com.legacy.pagamos.client.render.entity;

import com.legacy.pagamos.Pagamos;

import net.minecraft.client.renderer.entity.CreeperRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.state.CreeperRenderState;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class IceCreeperRenderer extends CreeperRenderer
{
	private static final ResourceLocation TEXTURE = Pagamos.locate("textures/entity/ice_creeper/ice_creeper.png");;

	public IceCreeperRenderer(EntityRendererProvider.Context context)
	{
		super(context);
	}

	@Override
	public ResourceLocation getTextureLocation(CreeperRenderState entity)
	{
		return TEXTURE;
	}
}