package com.legacy.pagamos.client.render.entity.layer;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.EyesLayer;
import net.minecraft.client.renderer.entity.state.EntityRenderState;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BaseGlowLayer<S extends EntityRenderState, M extends EntityModel<S>> extends EyesLayer<S, M>
{
	private final RenderType type;

	public BaseGlowLayer(RenderLayerParent<S, M> parent, ResourceLocation texture)
	{
		super(parent);
		this.type = RenderType.eyes(texture);
	}

	@Override
	public RenderType renderType()
	{
		return this.type;
	}
}
