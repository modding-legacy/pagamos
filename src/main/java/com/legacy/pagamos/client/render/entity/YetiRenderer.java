package com.legacy.pagamos.client.render.entity;

import com.legacy.pagamos.Pagamos;
import com.legacy.pagamos.client.PagamosRenderRefs;
import com.legacy.pagamos.client.model.YetiModel;
import com.legacy.pagamos.client.render.entity.layer.BaseGlowLayer;
import com.legacy.pagamos.client.render.entity.state.YetiRenderState;
import com.legacy.pagamos.entity.YetiEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;

public class YetiRenderer extends MobRenderer<YetiEntity, YetiRenderState, YetiModel<YetiRenderState>>
{
	private static final ResourceLocation TEXTURE = Pagamos.locate("textures/entity/yeti/yeti.png"), GLOW = Pagamos.locate("textures/entity/yeti/yeti_emissive.png");

	public YetiRenderer(EntityRendererProvider.Context context)
	{
		super(context, new YetiModel<>(context.bakeLayer(PagamosRenderRefs.YETI)), 0.6F);
		this.addLayer(new BaseGlowLayer<>(this, GLOW));
	}

	@Override
	public void extractRenderState(YetiEntity entity, YetiRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
	}

	@Override
	public ResourceLocation getTextureLocation(YetiRenderState entity)
	{
		return TEXTURE;
	}

	@Override
	public YetiRenderState createRenderState()
	{
		return new YetiRenderState();
	}
}