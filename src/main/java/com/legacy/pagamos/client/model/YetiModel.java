package com.legacy.pagamos.client.model;

import com.legacy.pagamos.client.render.entity.state.YetiRenderState;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.model.AnimationUtils;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.HumanoidArm;

public class YetiModel<T extends YetiRenderState> extends EntityModel<T> implements ArmedModel
{
	private final ModelPart root;
	private final ModelPart body;
	private final ModelPart head;
	private final ModelPart beard;
	private final ModelPart arms;
	private final ModelPart left_arm;
	private final ModelPart right_arm;
	private final ModelPart left_leg;
	private final ModelPart right_leg;

	public YetiModel(ModelPart root)
	{
		super(root);

		this.root = root.getChild("root");
		this.body = this.root.getChild("body");
		this.head = this.body.getChild("head");
		this.beard = this.head.getChild("beard");
		this.arms = this.body.getChild("arms");
		this.left_arm = this.arms.getChild("left_arm");
		this.right_arm = this.arms.getChild("right_arm");
		this.left_leg = this.root.getChild("left_leg");
		this.right_leg = this.root.getChild("right_leg");
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();

		PartDefinition root = partdefinition.addOrReplaceChild("root", CubeListBuilder.create(), PartPose.offset(0.0F, -12.0F, -5.0F));

		PartDefinition body = root.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 17).addBox(-10.0F, -20.3333F, -5.0F, 20.0F, 15.0F, 10.0F, new CubeDeformation(0.0F)).texOffs(60, 17).addBox(-10.0F, -20.3333F, -5.0F, 20.0F, 15.0F, 10.0F, new CubeDeformation(0.1F)).texOffs(0, 43).addBox(-9.0F, -5.3333F, -3.0F, 18.0F, 8.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, 20.3333F, 5.0F));

		PartDefinition head = body.addOrReplaceChild("head", CubeListBuilder.create().texOffs(38, 3).addBox(-6.0F, -3.5F, -6.0F, 12.0F, 4.0F, 10.0F, new CubeDeformation(0.0F)).texOffs(0, 0).addBox(-5.0F, -11.5F, -5.0F, 10.0F, 8.0F, 9.0F, new CubeDeformation(0.0F)).texOffs(74, 2).addBox(-6.0F, -6.5F, -6.0F, 12.0F, 3.0F, 3.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -20.8333F, 0.0F));

		PartDefinition beard = head.addOrReplaceChild("beard", CubeListBuilder.create().texOffs(82, 9).addBox(-7.0F, -0.5F, -0.5F, 14.0F, 7.0F, 1.0F, new CubeDeformation(0.0F)), PartPose.offset(0.0F, -1.0F, -5.75F));

		PartDefinition arms = body.addOrReplaceChild("arms", CubeListBuilder.create(), PartPose.offset(0.0F, -20.3333F, -5.0F));

		PartDefinition left_arm = arms.addOrReplaceChild("left_arm", CubeListBuilder.create().texOffs(65, 42).addBox(-6.25F, -3.0F, -3.0F, 6.0F, 7.0F, 6.0F, new CubeDeformation(0.0F)).texOffs(68, 55).addBox(-5.25F, 4.0F, -2.0F, 5.0F, 19.0F, 4.0F, new CubeDeformation(0.0F)), PartPose.offset(-9.75F, 3.0F, 5.0F));

		PartDefinition right_arm = arms.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(95, 55).addBox(0.25F, 4.0F, -2.0F, 5.0F, 19.0F, 4.0F, new CubeDeformation(0.0F)).texOffs(92, 42).addBox(0.25F, -3.0F, -3.0F, 6.0F, 7.0F, 6.0F, new CubeDeformation(0.0F)), PartPose.offset(9.75F, 3.0F, 5.0F));

		PartDefinition left_leg = root.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(0, 58).addBox(-4.0F, -1.25F, -3.5F, 8.0F, 13.0F, 8.0F, new CubeDeformation(0.0F)).texOffs(0, 80).addBox(-5.0F, 11.75F, -6.5F, 10.0F, 2.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(-6.0F, 22.25F, 4.5F));

		PartDefinition right_leg = root.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(32, 58).addBox(-4.0F, -1.25F, -3.5F, 8.0F, 13.0F, 8.0F, new CubeDeformation(0.0F)).texOffs(45, 80).addBox(-5.0F, 11.75F, -6.5F, 10.0F, 2.0F, 12.0F, new CubeDeformation(0.0F)), PartPose.offset(6.0F, 22.25F, 4.5F));

		return LayerDefinition.create(meshdefinition, 128, 128);
	}

	@Override
	public void setupAnim(T state)
	{
		float limbSwing = state.walkAnimationPos, limbSwingAmount = state.walkAnimationSpeed,
				ageInTicks = state.ageInTicks;

		//limbSwing = ageInTicks * 0.5F;
		//limbSwingAmount = 1.0F;

		this.head.resetPose();

		this.head.xRot += state.xRot / Mth.RAD_TO_DEG;
		this.head.yRot += state.yRot / Mth.RAD_TO_DEG;

		this.body.resetPose();
		this.right_arm.resetPose();
		this.left_arm.resetPose();
		this.right_leg.resetPose();
		this.left_leg.resetPose();
		this.arms.resetPose();

		float dumb = 0.6662F, bodyRange = 0.1F;
		float bodyRot = 0;/*Mth.sin(limbSwing * (dumb) + Mth.PI) * (limbSwingAmount * bodyRange / state.speedValue);*/
		
		/*this.body.yRot += Mth.cos(limbSwing * (dumb) + Mth.PI) * (limbSwingAmount * bodyRange / state.speedValue);
		
		bodyRot += (limbSwingAmount * 0.45F);*/
		this.body.xRot += bodyRot;
		
		this.head.xRot -= bodyRot;
		this.head.yRot -= this.body.yRot;

		this.arms.xRot -= bodyRot;

		this.right_arm.xRot += Mth.cos(limbSwing * dumb + Mth.PI) * 2.0F * limbSwingAmount * 0.5F / state.speedValue;
		this.left_arm.xRot += Mth.cos(limbSwing * dumb) * 2.0F * limbSwingAmount * 0.5F / state.speedValue;
		this.right_leg.xRot += Mth.cos(limbSwing * dumb) * 1.4F * limbSwingAmount / state.speedValue;
		this.left_leg.xRot += Mth.cos(limbSwing * dumb+ Mth.PI) * 1.4F * limbSwingAmount / state.speedValue;

		AnimationUtils.bobModelPart(this.left_arm, state.ageInTicks, 1.0F);
		AnimationUtils.bobModelPart(this.right_arm, state.ageInTicks, -1.0F);
		// this.right_arm.zRot -= 0.1F;
		// this.left_arm.zRot += 0.1F;
	}

	@Override
	public void translateToHand(HumanoidArm pSide, PoseStack pPoseStack)
	{
		/*boolean left = pSide == HumanoidArm.LEFT;
		ModelPart arm = left ? this.left_arm : this.right_arm;
		
		this.body.translateAndRotate(pPoseStack);
		arm.translateAndRotate(pPoseStack);
		pPoseStack.translate(0, 0.3F, 0.05F);*/
	}
}