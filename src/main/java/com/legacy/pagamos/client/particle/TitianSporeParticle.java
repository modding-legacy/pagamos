package com.legacy.pagamos.client.particle;

import com.legacy.pagamos.util.MathUtil;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.core.particles.SimpleParticleType;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class TitianSporeParticle extends TextureSheetParticle
{
	private final float maxSize;
	private float rollAddition;

	private TitianSporeParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(level, x, y, z, dx, dy, dz);

		float offset = 0.1F;
		this.xd = this.xd * (double) 0.03F + dx + ((this.random.nextFloat() - this.random.nextFloat()) * offset);
		this.yd = this.yd * (double) 0.01F + dy + 0.03F;
		this.zd = this.zd * (double) 0.03F + dz + ((this.random.nextFloat() - this.random.nextFloat()) * offset);
		this.x += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.y += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.z += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.lifetime = 5 * 20;

		this.oRoll = this.roll;
		this.roll = this.random.nextFloat() * 360F;
		this.rollAddition = MathUtil.plusOrMinus(random, 0.1F, 0.05F);

		this.quadSize = 0;
		this.maxSize = MathUtil.plusOrMinus(random, 0.2F, 0.05F);
		
		//System.out.println(this.brightnessVariance);
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_OPAQUE;
	}

	@Override
	public void move(double x, double y, double z)
	{
		this.setBoundingBox(this.getBoundingBox().move(x, y, z));
		this.setLocationFromBoundingbox();
	}

	@Override
	public float getQuadSize(float partialTicks)
	{
		float max = this.maxSize;
		double progress = this.getProgress(partialTicks);

		double fadeAmount = (progress - 1) * 0.8F;

		return (float) (progress >= 1 ? Math.max(0, max - (max * fadeAmount)) : easeOutElastic(progress) * max);
	}

	private double getProgress(float partialTicks)
	{
		return (float) (this.age + partialTicks) / 40F;
	}

	@Override
	public int getLightColor(float partialTick)
	{
		return super.getLightColor(partialTick);
	}

	@Override
	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;

		if (this.age++ >= this.lifetime)
		{
			this.remove();
		}
		else
		{
			this.move(this.xd, this.yd, this.zd);

			this.xd *= (double) 0.96F;
			this.yd *= (double) 0.96F;
			this.zd *= (double) 0.96F;

			if (this.onGround)
			{
				this.xd *= (double) 0.7F;
				this.zd *= (double) 0.7F;
			}
			this.oRoll = this.roll;

			if (this.getProgress(0) >= 1)
			{
				this.yd = this.yd * 1.1D;
				this.rollAddition += 0.01F;
			}

			this.roll += this.rollAddition;
		}
	}

	private static double easeOutElastic(double x)
	{
		int amount = 5;
		double c4 = (2 * Math.PI) / 2;
		return x == 0 ? 0 : x == 1 ? 1 : Math.pow(2, -amount * x) * Math.sin((x * amount - 0.32) * c4) + 1;
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		@Override
		public Particle createParticle(SimpleParticleType typeIn, ClientLevel level, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			TitianSporeParticle particle = new TitianSporeParticle(level, x, y, z, xSpeed, ySpeed, zSpeed, spriteSet);
			particle.pickSprite(spriteSet);
			return particle;
		}
	}
}