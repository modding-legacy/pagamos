package com.legacy.pagamos.client.particle;

import com.legacy.pagamos.util.MathUtil;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.particle.Particle;
import net.minecraft.client.particle.ParticleProvider;
import net.minecraft.client.particle.ParticleRenderType;
import net.minecraft.client.particle.SpriteSet;
import net.minecraft.client.particle.TextureSheetParticle;
import net.minecraft.core.particles.SimpleParticleType;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

public class CrystalShavingParticle extends TextureSheetParticle
{
	private final float maxSize, brightnessVariance;
	private float rollAddition;

	private CrystalShavingParticle(ClientLevel level, double x, double y, double z, double dx, double dy, double dz, SpriteSet spriteSet)
	{
		super(level, x, y, z, dx, dy, dz);

		this.alpha = 0;

		float offset = 0.05F;
		this.xd = MathUtil.plusOrMinus(random, offset);
		this.yd = MathUtil.plusOrMinus(random, offset);
		this.zd = MathUtil.plusOrMinus(random, offset);
		this.x += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.y += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.z += (double) ((this.random.nextFloat() - this.random.nextFloat()) * 0.05F);
		this.lifetime = 7 * 20;

		this.oRoll = this.roll;
		this.roll = this.random.nextFloat() * 360F;
		this.rollAddition = MathUtil.plusOrMinus(random, 0.1F, 0.05F);

		if (random.nextBoolean())
			this.rollAddition = -this.rollAddition;

		this.alpha = 0.1F;
		this.quadSize = 0;
		this.maxSize = MathUtil.plusOrMinus(random, 0.2F, 0.05F);

		this.brightnessVariance = MathUtil.plusOrMinus(random, 0.2F);
		
	}

	@Override
	public ParticleRenderType getRenderType()
	{
		return ParticleRenderType.PARTICLE_SHEET_TRANSLUCENT;
	}

	@Override
	public void move(double x, double y, double z)
	{
		this.setBoundingBox(this.getBoundingBox().move(x, y, z));
		this.setLocationFromBoundingbox();
	}

	@Override
	public float getQuadSize(float partialTicks)
	{
		return Math.max(0, this.maxSize * ((this.alpha - 0.1F) * 2));
	}

	private double getProgress(float partialTicks)
	{
		return (float) (this.age + partialTicks) / 50F;
	}

	@Override
	public int getLightColor(float partialTick)
	{
		return Math.max((int) (240F * (0.8F + this.brightnessVariance)), super.getLightColor(partialTick));
	}

	@Override
	public void tick()
	{
		this.xo = this.x;
		this.yo = this.y;
		this.zo = this.z;

		if (this.age++ >= this.lifetime || this.age > 20 && this.alpha <= 0)
		{
			this.remove();
		}
		else
		{
			this.move(this.xd, this.yd, this.zd);

			if (this.onGround)
			{
				this.xd *= (double) 0.7F;
				this.zd *= (double) 0.7F;
			}
			this.oRoll = this.roll;

			if (this.getProgress(0) >= 1)
				this.alpha -= 0.01F;
			else
				this.alpha += 0.015F;


			this.alpha = Math.clamp(this.alpha, 0, 1);
			this.roll += this.rollAddition;
		}
	}

	@OnlyIn(Dist.CLIENT)
	public static class Factory implements ParticleProvider<SimpleParticleType>
	{
		private final SpriteSet spriteSet;

		public Factory(SpriteSet spriteSet)
		{
			this.spriteSet = spriteSet;
		}

		@Override
		public Particle createParticle(SimpleParticleType typeIn, ClientLevel level, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed)
		{
			CrystalShavingParticle particle = new CrystalShavingParticle(level, x, y, z, xSpeed, ySpeed, zSpeed, spriteSet);
			particle.pickSprite(spriteSet);
			return particle;
		}
	}
}