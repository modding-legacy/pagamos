package com.legacy.pagamos.client;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import com.legacy.pagamos.client.gui.PagamosPortalScreen;
import com.legacy.pagamos.mixin.LevelRendererInvoker;
import com.legacy.pagamos.registry.PagamosDimensions;
import com.legacy.pagamos.registry.PagamosShaders;

import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.util.CubicSampler;
import net.minecraft.world.level.material.FogType;
import net.minecraft.world.phys.Vec3;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.RegisterDimensionSpecialEffectsEvent;
import net.neoforged.neoforge.client.event.RegisterDimensionTransitionScreenEvent;
import net.neoforged.neoforge.client.event.ViewportEvent;

@SuppressWarnings("resource")
public class PagamosClient
{
	@SubscribeEvent
	public static void modifyFog(ViewportEvent.RenderFog event)
	{
		if (mc().player.level().dimensionType().effectsLocation().equals(PagamosDimensions.PAGAMOS_ID))
		{
			Camera cam = event.getCamera();
			FogType fogtype = cam.getFluidInCamera();
			if (fogtype == FogType.NONE && mc().level.getFluidState(event.getCamera().getBlockPosition()).isEmpty() && !((LevelRendererInvoker) mc().levelRenderer).glacidus$doesMobEffectBlockSky(cam))
			{
				event.setNearPlaneDistance(0.007F);
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public static void modifyFog(ViewportEvent.ComputeFogColor event)
	{
	}

	private static final Minecraft mc()
	{
		return Minecraft.getInstance();
	}

	public static void init(IEventBus modBus)
	{
		modBus.addListener(PagamosClient::initDimensionRenderInfo);
		modBus.addListener(PagamosClient::registerDimensionTransitions);
		modBus.addListener(PagamosShaders::init);
	}

	private static final DimensionSpecialEffects PAGAMOS_RENDER_INFO = new PagamosRenderInfo();

	public static void initDimensionRenderInfo(RegisterDimensionSpecialEffectsEvent event)
	{
		event.register(PagamosDimensions.PAGAMOS_ID, PAGAMOS_RENDER_INFO);
	}

	public static void registerDimensionTransitions(RegisterDimensionTransitionScreenEvent event)
	{
		event.registerIncomingEffect(PagamosDimensions.pagamosKey(), PagamosPortalScreen::new);
		event.registerOutgoingEffect(PagamosDimensions.pagamosKey(), PagamosPortalScreen::new);
	}

	public static class PagamosRenderInfo extends DimensionSpecialEffects
	{
		public PagamosRenderInfo()
		{
			super(Float.NaN, true, DimensionSpecialEffects.SkyType.NONE, false, false);
		}

		@Override
		public boolean tickRain(ClientLevel level, int ticks, Camera camera)
		{
			return true;
		}

		@Override
		public boolean renderClouds(ClientLevel level, int ticks, float partialTick, double camX, double camY, double camZ, Matrix4f modelViewMatrix, Matrix4f projectionMatrix)
		{
			return true;
		}

		@Override
		public Vec3 getBrightnessDependentFogColor(Vec3 fogColor, float brightness)
		{
			return fogColor;
		}

		@Override
		public boolean isFoggyAt(int x, int y)
		{
			return false;
		}

		@SuppressWarnings("resource")
		public static Vector3f getLightmapColors(float partialTicks)
		{
			Vec3 camPos = mc().gameRenderer.getMainCamera().getPosition().subtract(2.0, 2.0, 2.0)/*.scale(0.55)*/;
			Vec3 blendedColor = CubicSampler.gaussianSampleVec3(camPos, (x, y, z) -> getLightmapColorAtY(y));

			return new Vector3f((float) blendedColor.x, (float) blendedColor.y, (float) blendedColor.z);
		}

		private static Vec3 getLightmapColorAtY(float y)
		{
			float light = -0.02F;
			float colorX = light + 0.07F;
			float colorY = light + 0.09F;
			float colorZ = light + 0.15F;
			return new Vec3(colorX, colorY, colorZ);
		}

		private static Minecraft mc()
		{
			return Minecraft.getInstance();
		}
	}
}