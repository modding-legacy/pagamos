package com.legacy.pagamos.client.gui;

import java.util.function.BooleanSupplier;

import com.legacy.pagamos.registry.PagamosBlocks;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.TextureAtlasSprite;

public class PagamosPortalScreen extends ReceivingLevelScreen
{
	protected final ReceivingLevelScreen.Reason reason;

	protected TextureAtlasSprite cachedSprite = null;

	public PagamosPortalScreen(BooleanSupplier levelReceived, Reason reason)
	{
		super(levelReceived, reason);
		this.reason = reason;
	}

	@Override
	public void renderBackground(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTick)
	{
		if (this.reason != ReceivingLevelScreen.Reason.OTHER)
		{
			super.renderBackground(guiGraphics, mouseX, mouseY, partialTick);
			return;
		}

		guiGraphics.blitSprite(RenderType::guiOpaqueTexturedBackground, this.getSprite(), 0, 0, guiGraphics.guiWidth(), guiGraphics.guiHeight());
	}

	@SuppressWarnings("deprecation")
	protected TextureAtlasSprite getSprite()
	{
		if (this.cachedSprite != null)
			return this.cachedSprite;

		this.cachedSprite = this.minecraft.getBlockRenderer().getBlockModelShaper().getParticleIcon(PagamosBlocks.pagamos_portal.defaultBlockState());
		return this.cachedSprite;
	}
}
