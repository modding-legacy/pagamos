package com.legacy.pagamos.client;

import com.legacy.pagamos.Pagamos;

import net.minecraft.client.model.geom.ModelLayerLocation;

// ModelLayers
public class PagamosRenderRefs
{
	public static final ModelLayerLocation YETI = layer("yeti");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(Pagamos.locate(name), layer);
	}
}
