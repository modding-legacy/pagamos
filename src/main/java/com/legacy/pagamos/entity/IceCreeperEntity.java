package com.legacy.pagamos.entity;

import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;

public class IceCreeperEntity extends Creeper
{
	public IceCreeperEntity(EntityType<? extends IceCreeperEntity> type, Level worldIn)
	{
		super(type, worldIn);
	}

	public static boolean canSpawn(EntityType<? extends Monster> typeIn, ServerLevelAccessor worldIn, EntitySpawnReason reason, BlockPos pos, RandomSource randomIn)
	{
		return Monster.checkMonsterSpawnRules(typeIn, worldIn, reason, pos, randomIn);
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSource)
	{
		return PagamosSounds.ENTITY_ICE_CREEPER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return PagamosSounds.ENTITY_ICE_CREEPER_DEATH;
	}

	@Override
	public void playSound(SoundEvent sound, float volume, float pitch)
	{
		super.playSound(sound == SoundEvents.CREEPER_PRIMED ? PagamosSounds.ENTITY_ICE_CREEPER_PRIMED : sound, volume, pitch);
	}
}
