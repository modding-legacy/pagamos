package com.legacy.pagamos.entity;

import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.RandomSource;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.entity.monster.Ghast;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;

public class CrystalPhoenixEntity extends Ghast
{
	public boolean field_753_a;
	public float field_752_b, destPos, field_757_d, field_756_e, field_755_h;

	public CrystalPhoenixEntity(EntityType<? extends CrystalPhoenixEntity> type, Level worldIn)
	{
		super(type, worldIn);

		field_753_a = false;
		field_752_b = 0.0F;
		destPos = 0.0F;
		field_755_h = 1.0F;
	}

	@Override
	public void aiStep()
	{
		super.aiStep();

		if (this.level().isClientSide())
		{
			field_756_e = field_752_b;
			field_757_d = destPos;
			destPos = (float) ((double) destPos + (double) (this.onGround() ? -1 : 4) * 0.29999999999999999D);
			if (destPos < 0.0F)
			{
				destPos = 0.0F;
			}
			if (destPos > 1.0F)
			{
				destPos = 1.0F;
			}
			if (!this.onGround() && field_755_h < 1.0F)
			{
				field_755_h = 1.0F;
			}
			field_755_h = (float) ((double) field_755_h * 0.90000000000000002D);
			field_752_b += field_755_h * 2.0F;
		}
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return PagamosSounds.ENTITY_CRYSTAL_PHOENIX_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return PagamosSounds.ENTITY_CRYSTAL_PHOENIX_IDLE;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return PagamosSounds.ENTITY_CRYSTAL_PHOENIX_DEATH;
	}

	public static boolean canSpawn(EntityType<? extends CrystalPhoenixEntity> typeIn, LevelAccessor worldIn, EntitySpawnReason reason, BlockPos pos, RandomSource randomIn)
	{
		return worldIn.getRandom().nextInt(1000) == 0 && Mob.checkMobSpawnRules(typeIn, worldIn, reason, pos, randomIn);
	}
}
