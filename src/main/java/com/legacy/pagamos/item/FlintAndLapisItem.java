package com.legacy.pagamos.item;

import com.legacy.pagamos.block.PagamosPortalBlock;
import com.legacy.pagamos.registry.PagamosBlocks;
import com.legacy.pagamos.registry.PagamosSounds;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.FlintAndSteelItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BaseFireBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.ItemAbilities;

public class FlintAndLapisItem extends FlintAndSteelItem
{
	public FlintAndLapisItem(Properties builder)
	{
		super(builder);
	}

	@Override
	public InteractionResult useOn(UseOnContext context)
	{
		Player player = context.getPlayer();
		Level level = context.getLevel();
		BlockPos blockpos = context.getClickedPos();
		BlockState blockstate = level.getBlockState(blockpos);
		BlockState blockstate2 = blockstate.getToolModifiedState(context, ItemAbilities.FIRESTARTER_LIGHT, false);

		var clickedPos = blockpos.relative(context.getClickedFace());
		BlockState stateBeforeLight = level.getBlockState(clickedPos);
		PagamosBlocks.pagamos_portal.tryIgnite(level, clickedPos);
		BlockState stateAfterLight = level.getBlockState(clickedPos);

		if (stateAfterLight != stateBeforeLight && stateAfterLight.getBlock() instanceof PagamosPortalBlock)
		{
			if (level.isClientSide)
			{
				RandomSource rand = level.getRandom();
				Vec3 clickVec = context.getClickLocation();

				float scale = 0.5F;
				for (int i = 0; i < 100; ++i)
					level.addParticle(ParticleTypes.SNOWFLAKE, clickVec.x, clickVec.y, clickVec.z, (rand.nextFloat() - rand.nextFloat()) * scale, (rand.nextFloat() - rand.nextFloat()) * scale, (rand.nextFloat() - rand.nextFloat()) * scale);
			}
			level.playSound(null, clickedPos, PagamosSounds.BLOCK_PORTAL_LIGHT, SoundSource.BLOCKS, 1.0F, 1.0F);
			this.playLightSound(level, blockpos);
			return InteractionResult.SUCCESS;
		}

		if (blockstate2 == null)
		{
			BlockPos blockpos1 = blockpos.relative(context.getClickedFace());
			if (BaseFireBlock.canBePlacedAt(level, blockpos1, context.getHorizontalDirection()))
			{
				this.playLightSound(level, blockpos);
				BlockState blockstate1 = PagamosBlocks.blue_fire.defaultBlockState()/*BaseFireBlock.getState(level, blockpos1)*/;
				level.setBlock(blockpos1, blockstate1, 11);
				level.gameEvent(player, GameEvent.BLOCK_PLACE, blockpos);
				ItemStack itemstack = context.getItemInHand();
				if (player instanceof ServerPlayer)
				{
					CriteriaTriggers.PLACED_BLOCK.trigger((ServerPlayer) player, blockpos1, itemstack);
					itemstack.hurtAndBreak(1, player, LivingEntity.getSlotForHand(context.getHand()));
				}

				return InteractionResult.SUCCESS;
			}
			else
			{
				return InteractionResult.FAIL;
			}
		}

		this.playLightSound(level, blockpos);
		level.setBlock(blockpos, blockstate2, 11);
		level.gameEvent(player, GameEvent.BLOCK_CHANGE, blockpos);
		if (player != null)
		{
			context.getItemInHand().hurtAndBreak(1, player, LivingEntity.getSlotForHand(context.getHand()));
		}

		return InteractionResult.SUCCESS;
	}

	public void playLightSound(Level level, BlockPos pos)
	{
		level.playSound(null, pos, SoundEvents.FLINTANDSTEEL_USE, SoundSource.BLOCKS, 1.0F, level.getRandom().nextFloat() * 0.4F + 0.8F);
	}

}
