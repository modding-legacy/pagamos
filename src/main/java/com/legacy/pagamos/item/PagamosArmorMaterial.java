package com.legacy.pagamos.item;

import java.util.EnumMap;

import com.legacy.pagamos.registry.PagamosEquipmentAssets;

import net.minecraft.Util;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.item.equipment.ArmorMaterial;
import net.minecraft.world.item.equipment.ArmorType;

public interface PagamosArmorMaterial
{
	ArmorMaterial TOURMALINE = new ArmorMaterial(33, Util.make(new EnumMap<>(ArmorType.class), instance ->
	{
		instance.put(ArmorType.BOOTS, 3);
		instance.put(ArmorType.LEGGINGS, 6);
		instance.put(ArmorType.CHESTPLATE, 8);
		instance.put(ArmorType.HELMET, 3);
		instance.put(ArmorType.BODY, 11);
	}), 10, SoundEvents.ARMOR_EQUIP_DIAMOND, 2.0F, 0.0F, ItemTags.REPAIRS_DIAMOND_ARMOR, PagamosEquipmentAssets.TOURMALINE);

	ArmorMaterial SAPPHIRE = new ArmorMaterial(33, Util.make(new EnumMap<>(ArmorType.class), instance ->
	{
		instance.put(ArmorType.BOOTS, 3);
		instance.put(ArmorType.LEGGINGS, 6);
		instance.put(ArmorType.CHESTPLATE, 8);
		instance.put(ArmorType.HELMET, 3);
		instance.put(ArmorType.BODY, 11);
	}), 10, SoundEvents.ARMOR_EQUIP_DIAMOND, 2.0F, 0.0F, ItemTags.REPAIRS_DIAMOND_ARMOR, PagamosEquipmentAssets.SAPPHIRE);
}