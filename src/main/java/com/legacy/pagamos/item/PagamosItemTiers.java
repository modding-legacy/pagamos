package com.legacy.pagamos.item;

import com.legacy.pagamos.data.PagamosTags;

import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.ToolMaterial;

public interface PagamosItemTiers
{
	ToolMaterial TOURMALINE = new ToolMaterial(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, 1561, 8.0F, 3.0F, 10, PagamosTags.Items.TOURMALINE_TOOL_MATERIALS);
	ToolMaterial SAPPHIRE = new ToolMaterial(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, 1561, 8.0F, 3.0F, 10, PagamosTags.Items.SAPPHIRE_TOOL_MATERIALS);
}